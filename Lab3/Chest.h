#ifndef CHEST_H
#define CHEST_H

#include <memory>
#include <ctime>
#include <cmath>
#include "Item.h"

class Chest {
private:
	int x;
	int y;
	bool _empty = false;
public:
	virtual bool needKey() const = 0;
	virtual std::unique_ptr<Item> getItem() = 0;
};


class HardChest : public Chest {
public:
	bool needKey() const {
		return true;
	}
	std::unique_ptr<Item> getItem() {

	}

};

class NormalChest : public Chest {
private:
	bool _needKey;
public:
	NormalChest() {
		std::srand(std::clock());
		_needKey = std::rand() % 2;
	}
	bool needKey() const {
		return _needKey;
	}
	std::unique_ptr<Item> getItem() {

	}

};

class EasyChest : public Chest {
public:
	bool needKey() const {
		return false;
	}
	std::unique_ptr<Item> getItem() {

	}

};

#endif
