#ifndef DISPLAY_OBJECT_H
#define DISPLAY_OBJECT_H

#include <SFML/Graphics.hpp>

#include <memory>

class DisplayObject : public sf::Drawable {
protected:
	float _x, _y;
	bool _active = true;
public:

	virtual void updateGlobalPosition(float x = 0.f, float y = 0.f) = 0;
	virtual float getWidth() const = 0;
	virtual float getHeight() const = 0;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

	void setPosition(float x, float y) {
		_x = x;
		_y = y;
	}

	virtual void setActive(bool value) {
		_active = value;
	}

	bool isActive() const {
		return _active;
	}

	float getX() const {
		return _x;
	}

	float getY() const {
		return _y;
	}

	void setX(float x) {
		_x = x;
	}

	void setY(float y) {
		_y = y;
	}

	virtual ~DisplayObject() {}
};

using DisplayObjectPtr = std::unique_ptr<DisplayObject>;

#endif