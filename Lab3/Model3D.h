#ifndef MODEL3D_H
#define MODEL3D_H

#include <vector>

class Model3D {
public:
	struct Object {
		std::vector<float> vCoords;
		std::vector<float> nCoords;
		std::vector<float> texCoords;

		std::string textureName = "";

		size_t size = 0;
	};

	struct Face {
		float x;
		float y;
		float z;
		float w;
		float h;

		float u1 = 0.f;
		float u2 = 1.f;

		float v1 = 0.f;
		float v2 = 1.f;
	};
private:
	std::vector<Object> _objects;
public:
	enum FaceType { BOTTOM, TOP, LEFT, RIGHT, FRONT, BACK };
	const std::vector<Object> &getObjects() const;
	void draw();
	void createNewObject(std::string textureName);
	void addFace(FaceType faceType, Face &face);

	void addXYFace(bool clockwise, Face &face);
	void addXZFace(bool clockwise, Face &face);
	void addYZFace(bool clockwise, Face &face);

	void addBox(float x, float y, float z, float w, float h, float d);

};

#endif