#include "Command.h"
#include "Entity.h"

HealCommand::HealCommand(int amount):
	_amount(amount){
}

bool HealCommand::execute(Entity &receiver){
	return true;
}


KillCommand::KillCommand() {
}

bool KillCommand::execute(Entity &receiver) {
	return true;
}

IncreaseSpeedCommand::IncreaseSpeedCommand(float amount ):
	_amount(amount) {

}
bool IncreaseSpeedCommand::execute(Entity &receiver) {
	return true;
}

MoveCommand::MoveCommand(float xvel, float yvel) {
	_xvel = xvel;
	_yvel = yvel;
}

bool MoveCommand::execute(Entity &receiver) {
	receiver.setX(receiver.getX() + _xvel);
	receiver.setX(receiver.getY() + _yvel);
	return true;
}

bool DamageCommand::execute(Entity &receiver) {
	receiver.damage(_damage);
	return true;
}