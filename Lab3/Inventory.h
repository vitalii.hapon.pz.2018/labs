#ifndef INVENTORY_H
#define INVENTORY_H

#include "ResourceManager.h"

#include "GameItemsSet.h"

#include <vector>
#include <iostream>
#include <memory>

class AbstractInventory {
public:
	virtual void selectItem(size_t id) = 0;
	virtual void fillWithRandomItems() = 0;
	virtual Item & getSelectedItem() = 0;
	virtual Item & getItemByID(size_t id) = 0;
	virtual size_t getSelectedItemID() = 0;
	virtual std::string getSelectedItemName() = 0;
};

class InventoryLogger final : public AbstractInventory {
private:
	std::unique_ptr<AbstractInventory> _invetory;
public:
	InventoryLogger();
	void selectItem(size_t id);
	void fillWithRandomItems();
	Item & getSelectedItem();
	Item & getItemByID(size_t id);
	std::string getSelectedItemName();
	size_t getSelectedItemID();
};

class Inventory final: public AbstractInventory {
private:

	std::vector< std::unique_ptr <Item>> _items;
	size_t _selectedItem;
public:
	Inventory();
	void selectItem(size_t id);
	void fillWithRandomItems();
	Item & getSelectedItem();
	Item & getItemByID(size_t id);
	std::string getSelectedItemName();
	size_t getSelectedItemID();


};

#endif