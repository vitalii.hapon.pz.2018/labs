﻿#include "Application.h"

#include "Game.h"

#include "DisplayContainer.h"
#include "ResourceManager.h"
#include "DisplayObjects.h"
#include "EventManager.h"


#include <glad/glad.h>

#include <ctime>
#include <cmath>

Application::Application(bool isFullscreen):
	_isFullscreen(isFullscreen){
	if (isFullscreen) {
		_window.create(sf::VideoMode::getDesktopMode(), "The Lost Dungeon", sf::Style::Fullscreen, sf::ContextSettings(24));
	}
	else {
		_window.create(sf::VideoMode(1024, 576), "The Lost Dungeon", sf::Style::Titlebar | sf::Style::Close, sf::ContextSettings(24));
	}
	gladLoadGL();
	std::srand(std::time(nullptr));
}

void Application::run() {
	auto &resMan = ResourceManager::getInstance();

	sf::Font font;
	font.loadFromFile("resources/ebd.ttf");

	DisplayContainer mainMenuButtons;
	DisplayContainer sureMessage;
	DisplayContainer slotsButtons;
	DisplayContainer settingsMenuButtons;
	DisplayContainer difficultyButtons;
	DisplayContainer nameInputContainer;

	EventManager eventManager;

	sf::Text volumeValue;
	volumeValue.setFont(font);
	volumeValue.setCharacterSize(36);

	TextInput nameInput = TextInput("menu/big_button_idle", "Enter your name ...", font, 28);
		
	Button btnPlay = Button("menu/big_button", "PLAY", font, 36);
	Button btnSettings = Button("menu/big_button", "SETTINGS", font, 36);
	Button btnExit = Button("menu/big_button", "EXIT", font, 36);

	Button btnSoundInc = Button("menu/small_button", "+", font, 50);
	Button btnSoundDec = Button("menu/small_button", "-", font, 50);
	Button btnWindowMode = Button("menu/big_button", _isFullscreen? "WINDOWED MODE" : "FULLSCREEN", font, 28);
	Button btnBack = Button("menu/button", "BACK", font, 36);

	Button btnLoadSlot1 = Button("menu/big_button", "Vitalik", font, 36);
	Button btnLoadSlot2 = Button("menu/big_button", "NEW GAME", font, 36);
	Button btnLoadSlot3 = Button("menu/big_button", "NEW GAME", font, 36);

	Button btnDelSlot1 = Button("menu/small_button", "x", font, 50);
	Button btnDelSlot2 = Button("menu/small_button", "x", font, 50);
	Button btnDelSlot3 = Button("menu/small_button", "x", font, 50);

	Button btnBack2 = Button("menu/button", "BACK", font, 36);

	Text txtSure = Text("Are you sure?", font);
	Button btnYes = Button("menu/big_button", "YES", font, 36);
	Button btnNo = Button("menu/big_button", "NO", font, 36);

	Text txtEnter = Text("Enter your name", font);
	TextInput itxtName = TextInput("menu/input_background", "your name...", font);
	Button btnCancel2 = Button("menu/big_button", "CANCEL", font, 36);
	Button btnOk = Button("menu/small_button", "OK", font, 30);

	Button btnEasy = Button("menu/big_button", "EASY", font, 36);
	Button btnNormal = Button("menu/big_button", "NORMAL", font, 36);
	Button btnHard = Button("menu/big_button", "HARD", font, 36);
	Button btnCancel = Button("menu/button", "CANCEL", font, 36);


	eventManager.subscribe(btnOk);
	eventManager.subscribe(itxtName);
	eventManager.subscribe(btnCancel2);
	eventManager.subscribe(btnBack2);
	eventManager.subscribe(btnYes);
	eventManager.subscribe(btnNo);
	eventManager.subscribe(btnLoadSlot1);
	eventManager.subscribe(btnLoadSlot2);
	eventManager.subscribe(btnLoadSlot3);
	eventManager.subscribe(btnDelSlot1);
	eventManager.subscribe(btnDelSlot2);
	eventManager.subscribe(btnDelSlot3);
	eventManager.subscribe(btnPlay);
	eventManager.subscribe(btnSettings);
	eventManager.subscribe(btnExit);
	eventManager.subscribe(btnSoundInc);
	eventManager.subscribe(btnSoundDec);
	eventManager.subscribe(btnWindowMode);
	eventManager.subscribe(btnBack);
	eventManager.subscribe(btnEasy);
	eventManager.subscribe(btnNormal);
	eventManager.subscribe(btnHard);
	eventManager.subscribe(btnCancel);

	nameInputContainer.addObject(txtEnter);
	nameInputContainer.addObject(itxtName);
	nameInputContainer.addObject(btnCancel2);
	nameInputContainer.addObject(btnOk);

	slotsButtons.addObject(btnLoadSlot1);
	slotsButtons.addObject(btnLoadSlot2);
	slotsButtons.addObject(btnLoadSlot3);
	slotsButtons.addObject(btnDelSlot1);
	slotsButtons.addObject(btnDelSlot2);
	slotsButtons.addObject(btnDelSlot3);
	slotsButtons.addObject(btnBack2);

	sureMessage.addObject(txtSure);
	sureMessage.addObject(btnYes);
	sureMessage.addObject(btnNo);

	mainMenuButtons.addObject(btnPlay);
	mainMenuButtons.addObject(btnSettings);
	mainMenuButtons.addObject(btnExit);


	settingsMenuButtons.addObject(btnSoundInc);
	settingsMenuButtons.addObject(btnSoundDec);
	settingsMenuButtons.addObject(btnWindowMode);
	settingsMenuButtons.addObject(btnBack);

	difficultyButtons.addObject(btnEasy);
	difficultyButtons.addObject(btnNormal);
	difficultyButtons.addObject(btnHard);
	difficultyButtons.addObject(btnCancel);

	mainMenuButtons.setActive(true);
	settingsMenuButtons.setActive(false);
	difficultyButtons.setActive(false);
	sureMessage.setActive(false);
	slotsButtons.setActive(false);
	nameInputContainer.setActive(false);

	auto updateGrid = [&]() {
		constexpr int spacing = 16;

		auto buttonHeight = btnPlay.getHeight();
		auto buttonWidth = btnPlay.getWidth();
		auto screenWidth = _window.getSize().x;
		auto screenHeight = _window.getSize().y;

		auto btnBlockHeight = 3 * buttonHeight + 2 * spacing;
		auto btnBlockYPos = (screenHeight - btnBlockHeight) / 2;

		mainMenuButtons.setX((screenWidth - buttonWidth) / 2);
		settingsMenuButtons.setX((screenWidth - buttonWidth) / 2);
		difficultyButtons.setX((screenWidth - buttonWidth) / 2);

		btnPlay.setY(btnBlockYPos);
		btnSettings.setY(btnBlockYPos + buttonHeight + spacing);
		btnExit.setY(btnBlockYPos + 2 * buttonHeight + 2 * spacing);

		btnSoundInc.setX(buttonWidth - buttonHeight);

		btnSoundInc.setY(btnBlockYPos);
		btnSoundDec.setY(btnBlockYPos);
		
		btnWindowMode.setY(btnBlockYPos + buttonHeight + spacing);
		btnBack.setY(btnBlockYPos + 2 * buttonHeight + 2 * spacing);

		btnBack.setX((buttonWidth - btnBack.getWidth())/2.0);

		///////////////////////////////////////////////////////////////////////////

		btnBlockHeight = 4 * buttonHeight + 3 * spacing;
		btnBlockYPos = (screenHeight - btnBlockHeight) / 2;
		
		btnEasy.setY(btnBlockYPos);
		btnNormal.setY(btnBlockYPos + buttonHeight + spacing );
		btnHard.setY(btnBlockYPos + 2*(buttonHeight + spacing) );

		buttonWidth = btnCancel.getWidth();
		btnCancel.setX((btnNormal.getWidth() - buttonWidth) / 2);
		btnCancel.setY(btnBlockYPos + 3 * (buttonHeight + spacing) + spacing);



		//////////////////////////////////////////////////////////////////////////////

		nameInputContainer.setX((screenWidth - itxtName.getWidth()) / 2);

		btnBlockHeight = 3 * buttonHeight + 3 * spacing;
		btnBlockYPos = (screenHeight - btnBlockHeight) / 2;


		btnOk.setX(itxtName.getWidth() + 8);
		btnOk.setY(btnBlockYPos + buttonHeight + spacing);

		txtEnter.setY((buttonHeight - txtEnter.getHeight())/2 + btnBlockYPos);
		txtEnter.setX((itxtName.getWidth() - txtEnter.getWidth())/2 );
		itxtName.setY(btnBlockYPos + buttonHeight + spacing);
		btnCancel2.setY(btnBlockYPos + 2 * (buttonHeight + spacing));

		buttonWidth = btnCancel2.getWidth();
		btnCancel2.setX((btnNormal.getWidth() + 8 + btnOk.getWidth() - buttonWidth) / 2);

		/////////////////////////////////////////////////////////////////////////////

		slotsButtons.setX((screenWidth - btnLoadSlot1.getWidth() - 8 - btnDelSlot1.getWidth()) / 2);

		btnBlockHeight = 4 * buttonHeight + 3 * spacing;
		btnBlockYPos = (screenHeight - btnBlockHeight) / 2;

		btnLoadSlot1.setY(btnBlockYPos);
		btnLoadSlot2.setY(btnBlockYPos + buttonHeight + spacing);
		btnLoadSlot3.setY(btnBlockYPos + 2 * (buttonHeight + spacing));

		btnDelSlot1.setY(btnBlockYPos);
		btnDelSlot2.setY(btnBlockYPos + buttonHeight + spacing);
		btnDelSlot3.setY(btnBlockYPos + 2 * (buttonHeight + spacing));
		btnBack2.setY(btnBlockYPos + 3 * (buttonHeight + spacing));

		auto delBtnX = btnLoadSlot1.getWidth() + 8;

		btnDelSlot1.setX(delBtnX);
		btnDelSlot2.setX(delBtnX);
		btnDelSlot3.setX(delBtnX);

		buttonWidth = btnBack2.getWidth();
		btnBack2.setX((btnLoadSlot1.getWidth() + 8 + btnDelSlot2.getWidth() - buttonWidth) / 2);

		/////////////////////////////////////////////////////////////////////////////

		sureMessage.setX((screenWidth - txtSure.getWidth() ) / 2);

		buttonWidth = btnBack2.getWidth();
		btnBack2.setX((btnNormal.getWidth() - buttonWidth) / 2);

		btnBlockHeight = 2 * buttonHeight + 1 * spacing;
		btnBlockYPos = (screenHeight - btnBlockHeight) / 2;


		txtSure.setY(btnBlockYPos);

		btnYes.setY(btnBlockYPos + buttonHeight + spacing);
		btnNo.setY(btnBlockYPos + buttonHeight + spacing);

		buttonWidth = btnNormal.getWidth();
		txtSure.setX((btnNormal.getWidth() - txtSure.getWidth()) / 2);
		btnNo.setX((btnNormal.getWidth() - btnNo.getWidth()));


		/////////////////////////////////////////////////////////////////////////////

		mainMenuButtons.updateGlobalPosition(0.f, 0.f);
		settingsMenuButtons.updateGlobalPosition(0.f, 0.f);
		difficultyButtons.updateGlobalPosition(0.f, 0.f);
		nameInputContainer.updateGlobalPosition(0.f, 0.f);
		sureMessage.updateGlobalPosition(0.f, 0.f);
		slotsButtons.updateGlobalPosition(0.f, 0.f);

	};

	auto updateVolumeValue = [&]() {
		volumeValue.setString("VOL " + std::to_string(int(resMan.getSoundVolume() * 100)) + "%");

		auto screenWidth = _window.getSize().x;
		auto screenHeight = _window.getSize().y;
		volumeValue.setPosition((screenWidth - volumeValue.getGlobalBounds().width) / 2, 
			btnSoundInc.getY() + (btnSoundInc.getHeight() - volumeValue.getGlobalBounds().height) / 2.f);
	};

	updateGrid();


	btnPlay.onClick([&]() {
		mainMenuButtons.setActive(false);
		slotsButtons.setActive(true);

		//GameCaretaker ct = GameCaretaker(*_game, 3);

		
	});

	btnSettings.onClick([&]() {
		mainMenuButtons.setActive(false);
		settingsMenuButtons.setActive(true);
		updateVolumeValue();	
	});

	btnExit.onClick([&]() {
		_window.close();
	});

	auto changeVolume = [&](bool up) {
		static auto lastClock = 0;
		auto diff = std::clock() - lastClock;
		lastClock = std::clock();
		if (diff >= 500)
			diff = 500;

		auto mul = 500.f / diff;
		mul *= mul;
		if(up)
			resMan.setSoundVolume(resMan.getSoundVolume() + mul * 0.01);
		else
			resMan.setSoundVolume(resMan.getSoundVolume() - mul * 0.01);
		updateVolumeValue();
	};

	btnSoundInc.onPress([&]() {
		changeVolume(true);
	});

	btnSoundDec.onPress([&]() {
		changeVolume(false);
	});

	btnWindowMode.onClick([&]() {
	
		_isFullscreen = !_isFullscreen;
		_window.close();

		if (_isFullscreen) {
			_window.create(sf::VideoMode::getDesktopMode(), "The Lost Dungeon", sf::Style::Fullscreen, sf::ContextSettings(24));
			btnWindowMode.setText("WINDOWED MODE");
		}
		else {
			_window.create(sf::VideoMode(1024, 576), "The Lost Dungeon", sf::Style::Titlebar | sf::Style::Close, sf::ContextSettings(24));
			btnWindowMode.setText("FULLSCREEN");
		}

		updateGrid();
		updateVolumeValue();
	});

	btnBack.onClick([&]() {
		mainMenuButtons.setActive(true);
		settingsMenuButtons.setActive(false);

		volumeValue.setString("");
	});

	btnCancel2.onClick([&]() {
		volumeValue.setString("");
		mainMenuButtons.setActive(true);
		settingsMenuButtons.setActive(false);
		difficultyButtons.setActive(false);
		sureMessage.setActive(false);
		slotsButtons.setActive(false);
		nameInputContainer.setActive(false);
	});

	btnBack2.onClick([&]() {

		mainMenuButtons.setActive(true);
		slotsButtons.setActive(false);

	});

	btnLoadSlot1.onClick([&]() {

		_game = std::make_unique<Game>(*this);
		GameCaretaker ct = GameCaretaker(*_game, 3);
		ct.loadFromSloat(1);
		_game->run();
		ct.saveToSlot(1, "alpha");

		mainMenuButtons.setActive(true);
		slotsButtons.setActive(false);

	});



	btnLoadSlot2.onClick([&]() {
		nameInputContainer.setActive(true);
		slotsButtons.setActive(false);
	});

	btnLoadSlot3.onClick([&]() {
		nameInputContainer.setActive(true);
		slotsButtons.setActive(false);
	});

	itxtName.onEnter([&]() {
		_game = std::make_unique<Game>(*this);
		GameCaretaker ct = GameCaretaker(*_game, 3);
		_game->run();
		ct.saveToSlot(1, "alpha");

		nameInputContainer.setActive(false);
		mainMenuButtons.setActive(true);
	});

	btnOk.onClick([&]() {
		_game = std::make_unique<Game>(*this);
		GameCaretaker ct = GameCaretaker(*_game, 3);
		_game->run();
		ct.saveToSlot(1, "alpha");

		nameInputContainer.setActive(false);
		mainMenuButtons.setActive(true);
	});



	btnEasy.onClick([&]() {
		_game = std::make_unique<Game>(*this);	
		GameCaretaker ct = GameCaretaker(*_game, 3);
		_game->run();
		ct.saveToSlot(1, "alpha");

		mainMenuButtons.setActive(true);
		difficultyButtons.setActive(false);

	});

	btnNormal.onClick([&]() {
		_game = std::make_unique<Game>(*this);
		GameCaretaker ct = GameCaretaker(*_game, 3);
		ct.loadFromSloat(1);
		_game->run();

		ct.saveToSlot(1, "alpha");
		mainMenuButtons.setActive(true);
		difficultyButtons.setActive(false);
	});

	btnHard.onClick([&]() {
		_game = std::make_unique<Game>(*this);
		_game->run();
		mainMenuButtons.setActive(true);
		difficultyButtons.setActive(false);
	});

	btnCancel.onClick([&]() {
		mainMenuButtons.setActive(true);
		difficultyButtons.setActive(false);
	});


	while (_window.isOpen()) {
		sf::Event event;
		while (_window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				_window.close();

			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Escape) {
					volumeValue.setString("");
					mainMenuButtons.setActive(true);
					settingsMenuButtons.setActive(false);
					difficultyButtons.setActive(false);
					sureMessage.setActive(false);
					slotsButtons.setActive(false);
					nameInputContainer.setActive(false);
				}
			}

			eventManager.notify(event);
		}
		_window.resetGLStates();
		_window.clear();		
		_window.draw(settingsMenuButtons);
		_window.draw(mainMenuButtons);
		_window.draw(difficultyButtons);
		_window.draw(volumeValue);
		_window.draw(sureMessage);
		_window.draw(slotsButtons);
		_window.draw(nameInputContainer);

		_window.display();
	}
}

sf::RenderWindow &  Application::getWindow() {
	return _window;
}
