#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <functional>
#include <memory>
#include <vector>

#include "Entity.h"

#include "Command.h"


class GameItemsSet;

class Item {
	friend GameItemsSet;

protected:

	std::string _texture;
	std::string _name;

	bool _used = false;

	//std::vector<CommandPtr> _commands;

public:
	Item(std::string texture, std::string name) : _texture(texture), _name(name) {}

	void addCommand(CommandPtr &command) {
		//_commands.push_back(std::move(command));
	}

	bool isUsed() const {
		return _used;
	}

	void use(Entity& entity) {
		/*for (auto &c : _commands) {
			if(c->execute(entity))
				_used = true;
		}*/
	}

	virtual void apply(Entity&) = 0;

	virtual bool hasAttack() const  = 0;
	virtual bool hasUsing() const  = 0;

	virtual std::unique_ptr<Item> clone() const = 0;

	std::string getName() const {
		return _name;
	}
	
	std::string getTextureName() const {
		return _texture;
	}
};

class Weapon: public Item {
	friend GameItemsSet;
private:
	int _damage;

public:
	Weapon(std::string texture, std::string name, int damage):
		Item(texture, name),
		_damage(damage){}

	Weapon(const Weapon&) = default;

	void apply(Entity& entity) {
		entity.damage(_damage);
	}

	bool hasAttack() const override {
		return true;
	}

	bool hasUsing() const override {
		return false;
	}

	std::unique_ptr<Item> clone() const override{
		return std::make_unique<Weapon>(*this);
	}
	
};

class Potion : public Item {
	friend GameItemsSet;
private:
	std::function<void(Entity&)> _effect;

public:
	Potion(std::string texture, std::string name, std::function<void(Entity&)> effect) :
		Item(texture, name),
		_effect(effect) {}

	void apply(Entity& entity) override {
		_effect(entity);
		_used = true;
	}

	bool hasAttack() const override {
		return true;
	}

	bool hasUsing() const override {
		return true;
	}

	std::unique_ptr<Item> clone() const override{
		return std::make_unique<Potion>(*this);
	}
};

class Key : public Item {
	friend GameItemsSet;

public:
	Key(std::string texture, std::string name) :
		Item(texture, name) {}

	void apply(Entity& entity) {	
		//_used = entity.command("use_key");
	}

	bool hasAttack() const override {
		return false;
	}

	bool hasUsing() const override {
		return true;
	}

	std::unique_ptr<Item> clone() const override {
		return std::make_unique<Key>(*this);
	}

};
#endif 
