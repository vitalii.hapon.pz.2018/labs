#ifndef I_MEMENTO_H
#define I_MEMENTO_H

#include <string>
#include <ctime>

#include "SerializedObject.h"

class IMemento: public ISerializable {
public:
	virtual std::string getName() = 0;
	virtual SerializedObject getState() = 0;
	virtual std::time_t getDate() = 0;
};

#endif