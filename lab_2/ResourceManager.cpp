#include "ResourceManager.h"


ResourceManager::ResourceManager() {
	_missingSound.loadFromFile("resources/missing_sound.wav");
	_missingTexure.loadFromFile("resources/missing_texture.png");
}


ResourceManager & ResourceManager::getInstance() {
	if (_instance == nullptr)
		_instance = new ResourceManager();

	return *_instance;
}

sf::SoundBuffer & ResourceManager::loadSound(const std::string &path) {
	auto it = _soundBuffers.find(path);
	if (it != _soundBuffers.end()) {
		return *(it->second);
	}
	else {
		sf::SoundBuffer sb;
		if (sb.loadFromFile("resources/" + path)) {
			auto tmpPtr = new sf::SoundBuffer(sb);
			_soundBuffers[path] = tmpPtr;
			return *tmpPtr;
		}
		else {
			_soundBuffers[path] = &_missingSound;
			return _missingSound;
		}
	}

}

sf::Texture & ResourceManager::loadTexture(const std::string &path) {
	auto it = _textures.find(path);
	if (it != _textures.end()) {
		return *(it->second);
	}
	else {
		sf::Texture texture;
		if (texture.loadFromFile("resources/" + path)) {
			auto tmpPtr = new sf::Texture(texture);
			_textures[path] = tmpPtr;
			return *tmpPtr;
		}
		else {
			_textures[path] = &_missingTexure;
			return _missingTexure;
		}
	}
}

void ResourceManager::setSoundVolume(float val) {
	if (val < 0.f)
		_soundVolume = 0.f;
	else if (val > 1.f)
		_soundVolume = 1.f;
	else
		_soundVolume = val;
}

float ResourceManager::getSoundVolume() const {
	return _soundVolume;
}

sf::Sound ResourceManager::getSound(const std::string &path) {
	sf::Sound sound = sf::Sound(loadSound(path));
	sound.setVolume(_soundVolume * 100.f);
	return sound;

}
ResourceManager* ResourceManager::_instance = nullptr;
