#include "DisplayObjects.h"

#include "ResourceManager.h"

Sprite::Sprite() {
	_sprite.setScale(6.f, 6.f);
}
Sprite::Sprite(const std::string &name){
	auto &resMan = ResourceManager::getInstance();
	_sprite.setTexture(resMan.loadTexture(name));
	_sprite.setScale(6.f, 6.f);
}

void Sprite::updateGlobalPosition(float x, float y) {
	_gx = x;
	_gy = y;
	_sprite.setPosition(_x + x, _y + y);
}
float Sprite::getWidth() const {
	return _sprite.getGlobalBounds().width;
}
float Sprite::getHeight() const {
	return _sprite.getGlobalBounds().height;
}
void Sprite::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	if(_active)
		target.draw(_sprite, states);
}

void Sprite::update() {
	_sprite.setPosition(_x + _gx, _y + _gy);
}

bool Sprite::contains(float x, float y) {
	return _sprite.getGlobalBounds().contains(x, y);
}

void Sprite::setTexture(std::string name) {
	auto &resMan = ResourceManager::getInstance();
	_sprite.setTexture(resMan.loadTexture(name));
}

void Sprite::setTexture(const sf::Texture & texture) {
	_sprite.setTexture(texture);
}

Text::Text(const std::string &text, const sf::Font & font, unsigned int size) {
	_text = sf::Text(text, font, size);
}

void Text::setSize(unsigned int size) {
	_text.setCharacterSize(size);
}
void Text::setText(const std::string &text) {
	_text.setString(text);
}

void Text::setColor(sf::Color color) {
	_text.setFillColor(color);
}


void Text::updateGlobalPosition(float x, float y) {
	_text.setPosition(_x + x, _y + y);
}

float Text::getWidth() const {
	return _text.getGlobalBounds().width;
}

float Text::getHeight() const {
	return _text.getGlobalBounds().height;
}

void Text::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	if(_active)
		target.draw(_text, states);
}


void TextInput::_updatePosition() {
	auto ptextH = _placeHolderText.getHeight();
	auto itextH = _inputText.getHeight();
	auto bH = _background.getHeight();

	_placeHolderText.updateGlobalPosition(_gx + 32, _gy + (bH - ptextH) / 2);
	_inputText.updateGlobalPosition(_gx + 32, _gy + (bH - itextH) / 2);
}

void TextInput::updateGlobalPosition(float x, float y) {
	_gx = _x + x;
	_gy = _y + y;

	_background.updateGlobalPosition(_gx, _gy);
	_updatePosition();
}


TextInput::TextInput(std::string textureName, std::string placeholder, const sf::Font &font, unsigned int fontSize, sf::Color textColor):
	_background(textureName + ".png"),
	_placeHolderText(" " + placeholder, font, fontSize),
	_inputText("", font, fontSize){

	_inputText.setColor(textColor);

	textColor.a *= 0.6;
	_placeHolderText.setColor(textColor);

	setPosition(0, 0);
}

void TextInput::onEnter(std::function<void()> handler) {
	_onEnter = handler;
}

std::string TextInput::getText() const {
	return _inputString;
}

void TextInput::sendEvent(const sf::Event & event) {
	if (_active) {
		if (event.type == sf::Event::KeyPressed) {
			if (event.key.code == sf::Keyboard::Backspace) {
				if (!_inputString.empty()) {
					_inputString.resize(_inputString.size() - 1);
				}
			}
			else if (event.key.code == sf::Keyboard::Enter) {
				if (_onEnter)
					_onEnter();

				_inputString.clear();
			}

		}
		else if (event.type == sf::Event::TextEntered) {
			if (event.text.unicode >= 32 && event.text.unicode < 128) {
				if (_inputString.size() < maxSize) {
					char ch = static_cast<char>(event.text.unicode);
					_inputString += ch;

				}
			}
		}

		_inputText.setText("|");
		_updatePosition();
	}
}

float TextInput::getWidth() const {
	return _background.getWidth();
}
float TextInput::getHeight() const {
	return _background.getHeight();
}

void TextInput::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(_background, states);

	if (_inputString.empty()) {
		target.draw(_placeHolderText, states);
	}

	std::string output = _inputString;

	if (std::clock() % 400 < 200) 
			output += "|";
	
	_inputText.setText(output);
	target.draw(_inputText, states);
}


void Button::_onHover() {
	if (_onHoverHandler)
		_onHoverHandler();
}
void Button::_onClick() {
	if (_onClickHandler)
		_onClickHandler();
}
void Button::_onPress() {
	if (_onPressHandler)
		_onPressHandler();
}
void Button::_onRelease() {
	if (_onReleaseHandler)
		_onReleaseHandler();
}


Button::Button(std::string textureName, std::string text, const sf::Font &font, unsigned int fontSize, sf::Color textColor) :
	_idleBackground(textureName + "_idle.png"),
	_hoverBackground(textureName + "_hover.png"),
	_pressBackground(textureName + "_press.png"),
	_text(text, font, fontSize) {

	setPosition(0, 0);
}


void Button::updateGlobalPosition(float x, float y) {
	_gx = _x + x;
	_gy = _y + y;

	_idleBackground.updateGlobalPosition(_gx, _gy);
	_hoverBackground.updateGlobalPosition(_gx, _gy);
	_pressBackground.updateGlobalPosition(_gx, _gy);
	_updatePosition();

}
void Button::_updatePosition() {
	auto textW = _text.getWidth();
	auto textH = _text.getHeight();
	auto btnW = _idleBackground.getWidth();
	auto btnH = _idleBackground.getHeight();

	_text.updateGlobalPosition(_gx + (btnW - textW) / 2, _gy + (btnH - textH) / 2);
}
void Button::press(int x, int y) {
	if (_idleBackground.contains(x, y)) {
		_onPress();
		_press = true;
	}
}

void Button::release(int x, int y) {
	if (_idleBackground.contains(x, y)) {
		_onRelease();
		if (_press)
			_onClick();
	}
	_press = false;
}
void Button::hover(int x, int y) {
	if (_idleBackground.contains(x, y)) {
		_onHover();
		_hover = true;
	}
	else {
		_hover = false;
	}
}

void Button::setText(const std::string & text) {
	_text.setText(text);
}

void Button::onClick(std::function<void()> handler) {
	_onClickHandler = handler;
}

void Button::onPress(std::function<void()> handler) {
	_onPressHandler = handler;
}

void Button::onRelease(std::function<void()> handler) {
	_onReleaseHandler = handler;
}

void Button::onHover(std::function<void()> handler) {
	_onHoverHandler = handler;
}

float Button::getWidth() const{
	return _idleBackground.getWidth();
}


float Button::getHeight() const{
	return _idleBackground.getHeight();
}

void Button::sendEvent(const sf::Event & event) {
	if (_active) {
		if (event.type == sf::Event::MouseButtonPressed) {
			auto mx = event.mouseButton.x;
			auto my = event.mouseButton.y;
			press(mx, my);
		}
		else if (event.type == sf::Event::MouseButtonReleased) {
			auto mx = event.mouseButton.x;
			auto my = event.mouseButton.y;
			release(mx, my);
		}
		else if (event.type == sf::Event::MouseMoved) {
			auto mx = event.mouseMove.x;
			auto my = event.mouseMove.y;
			hover(mx, my);
		}
	}
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	if (_press) {
		target.draw(_pressBackground, states);
	}
	else if (_hover) {
		target.draw(_hoverBackground, states);
	}
	else {
		target.draw(_idleBackground, states);
	}
	target.draw(_text, states);
}


