#ifndef ROOM_GENERATOR_H
#define ROOM_GENERATOR_H

#include "Room.h"
#include "Enemy.h"

#include <string>
#include <memory>
#include <vector>

class LevelObjectsFactory;

class RoomGenerator {
public:
	struct Position {
		unsigned int x, y;
	};
private:
	Room _resultRoom;
	std::vector<Position> _usedPositions;
public:
	virtual Room addRandomRoom() = 0;
	virtual Room addExit() = 0;
	virtual Room addDecor() = 0;

	Position generateRandomPosition();
	Room addRandomChest(LevelObjectsFactory & factory);
	Room addRoom(const Room& room);
	void connectRooms(std::string style);
	Room getResult();
};

class BrickRoomGenerator: public  RoomGenerator {
public:
	Room addRandomRoom() override;
	Room addExit() override;
	Room addDecor() override;
};

class RockRoomGenerator: public  RoomGenerator {
public:
	Room addRandomRoom() override;
	Room addExit() override;
	Room addDecor() override;
};

class WoodenRoomGenerator: public  RoomGenerator {
public:
	Room addRandomRoom() override;
	Room addExit() override;
	Room addDecor() override;
};

#endif