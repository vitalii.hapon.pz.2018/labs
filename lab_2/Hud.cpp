#include "Hud.h"
#include "Player.h"
#include "Level.h"
#include "Inventory.h"


Hud::Hud(Player & player, Level & level, AbstractInventory & inventory):
	_player(player),
	_level(level),
	_inventory(inventory){

	_levelMapTexture.loadFromImage(createLevelImage());
	_levelMap.setTexture(_levelMapTexture);

	
	_font.loadFromFile("resources/ebd.ttf");

	_text = Text("100 HP", _font, 64);
	_background = Sprite("inventory.png");
	_highLight = Sprite("highlight.png");
	_items.resize(5);

	auto bottom = _levelMap.getHeight();
	auto invetoryX = _levelMap.getWidth() + 48;
	auto invetoryY = bottom - _background.getHeight() - 8;

	_xpos = invetoryX;

	_background.setX(invetoryX);
	_background.setY(invetoryY);
	_levelMap.setY(_levelMap.getY() - 16);

	auto hpX = invetoryX + _background.getWidth() + 48;
	auto hpY = bottom - _text.getHeight() - 32;

	_text.setX(hpX);
	_text.setY(hpY);
	_text.setColor(sf::Color(192,0,14,255));

	_highLight.setY(invetoryY);

	_container.addObject(_levelMap);
	_container.addObject(_background);
	for (auto &item : _items) {
		item.setY(invetoryY + 12);
		_container.addObject(item);
	}
	_container.addObject(_highLight);
	_container.addObject(_text);
	
}
void Hud::update(float x, float y) {
	_gx = x;
	_gy = y;
	update();
	
}
void Hud::update() {
	_text.setText(std::to_string(_player.getHealth()) + " HP");
	_highLight.setX(_inventory.getSelectedItemID() * 18 * 6 + _xpos);

	for (size_t i = 0; i < 5; i++) {
		_items[i].setX(i * 18 * 6 + 12 + _xpos);
		_items[i].setTexture(_inventory.getItemByID(i).getTextureName());
	}
	_container.updateGlobalPosition(_gx, _gy);

}

void Hud::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	 target.draw(_container);
}

float Hud::getWidth() const {
	return _container.getWidth();
}

float Hud::getHeight() const {
	return _container.getHeight();

}


 sf::Image Hud::createLevelImage() {
	 auto &room = _level.getMainRoom();

	 sf::Image img;

	 auto xoff = room.getX();
	 auto yoff = room.getY();
	 img.create(room.getWidth() + xoff + 2, room.getHeight() + yoff + 2, sf::Color(0,0,0,170));

	 for (uint32_t ix = 0; ix < room.getWidth(); ix++) {
		 for (uint32_t iy = 0; iy < room.getHeight(); iy++) {
			 auto type = room._roomMap[ix][iy]._type;
			 auto tmpx = ix + xoff + 1;
			 auto tmpy = iy + yoff + 1;

			 if (type == Room::WALL)
				 img.setPixel(tmpx, tmpy, sf::Color(0, 82, 17, 255));
			 else if (type == Room::FLOOR)
				 img.setPixel(tmpx, tmpy, sf::Color(197, 174, 80, 255));
		 }
	 }
	 return img;
 }


