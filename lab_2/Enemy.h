#ifndef ENEMY_H
#define ENEMY_H

#include "Entity.h"
#include <string>
#include <cmath>

class Enemy: public Entity {
private:
	std::string _texture;
public:
	virtual void attack(Entity &) = 0;

	Enemy(std::string texture, float maxSpeed, int maxHealth):
		_texture(texture){
		_maxSpeed = maxSpeed;
		_health = maxHealth;
		_maxHealth = maxHealth;
	}

	std::string getTextureName() const {
		return _texture;
	}
	

};


class HardEnemy : public Enemy {
public:

	HardEnemy(std::string texture, float maxSpeed, int maxHealth) :
		Enemy(texture, maxSpeed, maxHealth) {

	}

	void attack(Entity & entity) {
		entity.damage(15);
	}

};

class NormalEnemy : public Enemy {
public:
	NormalEnemy(std::string texture, float maxSpeed,  int maxHealth) :
		Enemy(texture, maxSpeed, maxHealth) {

	}

	void attack(Entity & entity) {
		entity.damage(6 + std::rand()%7);
	}
};

class EasyEnemy : public Enemy {
public:
	EasyEnemy(std::string texture, float maxSpeed, int maxHealth) :
		Enemy(texture, maxSpeed,  maxHealth) {

	}

	void attack(Entity & entity) {
		entity.damage(1 + std::rand() % 4);
	}

};

#endif