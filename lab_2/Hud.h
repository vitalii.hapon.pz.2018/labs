#ifndef HUD_H
#define HUD_H

#include "DisplayObjects.h"
#include "DisplayContainer.h"

#include <vector>

class Player;
class Level;
class AbstractInventory;

class Hud: public sf::Drawable{
private:
	DisplayContainer _container;
	Text _text;
	Sprite _background;
	std::vector<Sprite> _items;
	Sprite _highLight;
	Sprite _levelMap;

	Player & _player;
	AbstractInventory & _inventory;
	Level & _level;

	sf::Font _font;

	sf::Texture _levelMapTexture;
	sf::Image createLevelImage();

	float _xpos;
	float _gx = 0.f, _gy = 0.f;

public:
	Hud(Player & player, Level & level, AbstractInventory & inventory);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void update(float x , float y );
	void update();

	float getWidth() const;
	float getHeight() const;
	
};

#endif

