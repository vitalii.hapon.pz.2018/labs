#ifndef GAME_H
#define GAME_H

#include "Player.h"
#include "Level.h"
#include "Inventory.h"

#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>

#include "IMemento.h"

class Application;
class LevelGenerator;
class LevelObjectsFactory;

class Game;

class GameCaretaker {
private:
	Game &_game;
	unsigned int _slotsNumber;
public:
	GameCaretaker(Game &game, unsigned int slotsNumber);
	bool saveToSlot(unsigned int slot, std::string name);
	bool loadFromSloat(unsigned int slot);
};

class GameSnapshot : public IMemento {
private:
	SerializedObject _game;
	std::time_t _date;
	std::string _name;
public:
	GameSnapshot(const SerializedObject &object);
	GameSnapshot(const std::vector<unsigned char> &data);

	void setName(std::string);

	std::string getName() override;
	SerializedObject getState()  override;
	std::time_t getDate()  override;

	SerializedObject serialize() const override;
	void deserialize(const SerializedObject &object) override;
};

class Game{
private:
	Application & _app;

	bool _pause = true;
	bool _running = true;

	std::unique_ptr<Level> _level;
	std::unique_ptr<AbstractInventory> _invetory;

	Room _createTestRoom();

public:
	Game(Application & app);
	void create(LevelGenerator&, LevelObjectsFactory&);
	void run();
	GameSnapshot save();
	void restore(GameSnapshot& snapshot);
};

#endif
