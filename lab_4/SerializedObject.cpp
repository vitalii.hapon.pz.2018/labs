#include "SerializedObject.h"


void SerializedValue::create(const ISerializable &data) {
	_type = OBJECT;
	_ptr.so = new SerializedObject(data.serialize());
}


void SerializedValue::create(const SerializedObject &data) {
	_type = OBJECT;
	_ptr.so = new SerializedObject(data);
}


void SerializedValue::create(const std::vector<unsigned char> &data) {
	_type = BINARY;
	_size = data.size();
	_ptr.bin = new unsigned char[data.size()];
	std::memcpy(_ptr.bin, data.data(), data.size());
}


SerializedValue::SerializedValue(const SerializedValue & other) {
	if (other._type == BINARY) {
		_type = BINARY;
		_size = other._size;
		_ptr.bin = new unsigned char[other._size];
		std::memcpy(_ptr.bin, other._ptr.bin, other._size);
	}
	else {
		_type = OBJECT;
		_ptr.so = new SerializedObject(*(other._ptr.so));
	}
}

SerializedValue SerializedValue::operator=(const SerializedValue & other) {
	if (other._type == BINARY) {
		_type = BINARY;
		_size = other._size;
		_ptr.bin = new unsigned char[other._size];
		std::memcpy(_ptr.bin, other._ptr.bin, other._size);
	}
	else {
		_type = OBJECT;
		_ptr.so = new SerializedObject(*(other._ptr.so));
	}
	return *this;
}
SerializedValue  SerializedValue::operator=(SerializedValue && other) {
	if (other._type == BINARY) {
		_type = BINARY;
		_size = other._size;
		_ptr.bin = other._ptr.bin;
		other._ptr.bin = nullptr;
	}
	else {
		_type = OBJECT;
		_size = other._size;
		_ptr.so = other._ptr.so;
		other._ptr.so = nullptr;
	}
	return *this;
}


SerializedValue::~SerializedValue() {
	if (_type == BINARY)
		delete[] _ptr.bin;
	else
		delete _ptr.so;
}

std::vector<unsigned char> SerializedValue::toBinary() const {
	if (_type == BINARY) {
		std::vector<unsigned char> tmp;
		tmp.reserve(_size);
		for (size_t i = 0; i < _size; i++)
			tmp.push_back(_ptr.bin[i]);
		return tmp;
	}
	return _ptr.so->toBinary();
}

SerializedValue::Type SerializedValue::getType() const {
	return _type;
}



void SerializedValue::loadData(ISerializable &dest) const {
	if (_type == OBJECT)
		dest.deserialize(*(_ptr.so));
}

void SerializedValue::loadData(SerializedObject &value) const {
	value = *(_ptr.so);
}

void SerializedValue::loadData(std::vector<unsigned char> &value) const {
	if (_type == BINARY) {
		value.resize(_size);
		std::memcpy((void*)value.data(), _ptr.bin, _size);
	}
}


void SerializedObject::store(const std::string &key, const ISerializable &value) {
	_map[key] = SerializedValue();
	_map[key].create(value);
}

void SerializedObject::store(const std::string &key, const std::vector<unsigned char> &value) {
	_map[key] = SerializedValue();
	_map[key].create(value);
}


void SerializedObject::load(const std::string &key, ISerializable &value) const {
	auto it = _map.find(key);
	if (it != _map.end()) {
		it->second.loadData(value);
	}
}


void SerializedObject::load(const std::string &key, std::vector<unsigned char> &value) const {
	auto it = _map.find(key);
	if (it != _map.end()) {
		it->second.loadData(value);
	}
}

void SerializedObject::store(const std::string &key, const SerializedObject &value) {
	_map[key] = SerializedValue();
	_map[key].create(value);
}

void SerializedObject::load(const std::string &key, SerializedObject &value) const {
	auto it = _map.find(key);
	if (it != _map.end()) {
		it->second.loadData(value);
	}
}

SerializedObject::SerializedObject(const std::vector<unsigned char> &data) {
	auto ptr = data.data();
	for (size_t i = 0; i < data.size();) {
		unsigned char type = *(reinterpret_cast<const unsigned char*>(ptr + i));
		i++;

		unsigned int keySize = *(reinterpret_cast<const unsigned int*>(ptr + i));
		i += 4;

		unsigned int valueSize = *(reinterpret_cast<const unsigned int*>(ptr + i));
		i += 4;

		std::string key = "";
		for (size_t j = 0; j < keySize; j++, i++) {
			key += *(reinterpret_cast<const unsigned char*>(ptr + i));
		}

		std::vector<unsigned char> value;
		value.reserve(valueSize);

		for (size_t j = 0; j < valueSize; j++, i++) {
			value.push_back(*(reinterpret_cast<const unsigned char*>(ptr + i)));
		}
		_map[key] = SerializedValue();
		if (type == 1) {
			_map[key].create(value);
		}
		else {
			_map[key].create(SerializedObject(value));
		}

	}
}

std::vector<unsigned char> SerializedObject::toBinary() const {
	std::vector<unsigned char> result;
	size_t pos = 0;
	for (auto &it : _map) {
		auto &key = it.first;
		auto keySize = key.size();

		unsigned char type = (it.second.getType() == SerializedValue::BINARY) ? 1 : 0;

		auto value = it.second.toBinary();
		auto valueSize = value.size();

		result.resize(pos + 1 + 4 + 4 + keySize + valueSize);
		auto ptr = result.data();

		*(reinterpret_cast<unsigned char*>(ptr + pos)) = type;
		pos++;

		*(reinterpret_cast<unsigned int*>(ptr + pos)) = keySize;
		pos += 4;

		*(reinterpret_cast<unsigned int*>(ptr + pos)) = valueSize;
		pos += 4;

		for (size_t i = 0; i < keySize; i++, pos++) {
			*(reinterpret_cast<unsigned char*>(ptr + pos)) = key[i];
		}

		for (size_t i = 0; i < valueSize; i++, pos++)
			*(reinterpret_cast<unsigned char*>(ptr + pos)) = value[i];
	}
	return result;
}
