#ifndef RENDER_ENGINE_H
#define RENDER_ENGINE_H

#include <vector>
#include <string>
#include <SFML/Graphics/Shader.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class ResourceManager;
class Level;

#include "Model3D.h"

class RenderEngine{
private:
	bool _showWireFrame = false;

	ResourceManager & _resManager;
	Level & _level;
	int _w, _h;
	
	Model3D _levelModel;

	sf::Shader shader;
	std::string _handItemName = "";

	glm::mat4 _modelMatrix;
	glm::mat4 _viewMatrix;
	glm::mat4 _projectionMatrix;
	

	void _refresh();
	void _setupCamera();
public:
	RenderEngine(int w, int h, Level & level);
		
	void resize(int w, int h);
	void render();

	void toggleWireFrame() ;
	void setHandItemName(const std::string & name);
};



#endif
