#include "State.h"


StandingStillState::StandingStillState(Entity &context) : State(context) {

}

void StandingStillState::kill() {

}
void StandingStillState::move(float direction) {

}
void StandingStillState::stop() {

}


DeadState::DeadState(Entity &context) : State(context) {

}

void DeadState::kill() {

}
void DeadState::move(float direction) {

}
void DeadState::stop() {

}

MovingState::MovingState(Entity &context) : State(context) {

}

void MovingState::kill() {

}

void MovingState::move(float direction) {

}

void MovingState::stop() {

}


State::State(Entity &context) :
	_context(context) {

}