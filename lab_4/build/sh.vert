//[VERTEX SHADER]
#version 430

layout (location = 0) in vec3 vPosition; 
layout (location = 1) in vec2 vTexCoords; 

uniform mat4 mvpMatrix;

out vec2 texCoords;
out float dist;

void main(){
    gl_Position = mvpMatrix * vec4(vPosition, 1.0);

    dist = distance(vec3(0.0, 0.0, 0.0), gl_Position.xyz);
    texCoords = vTexCoords;
}