#ifndef GAME_ITEMS_SET_H
#define GAME_ITEMS_SET_H

#include "Item.h"

#include <vector>
#include <ctime>
#include <cmath>

//Singletone
class GameItemsSet {
protected:
	enum ItemValue { LOW = 120, BELOW_REGULAR = 80, REGULAR = 36, ABOVE_REGULAR = 18, HIGHT = 6, RARE = 2, VERY_RARE = 1 };

	static GameItemsSet* _instance;

	struct ItemPrototype {
		std::unique_ptr<Item> _prototype;
		ItemValue _itemValue;
	};

	std::vector<ItemPrototype> _prototypes;
	std::vector<size_t> _random;


	void addWeapon(std::string texture, std::string name, int damage, ItemValue itemValue) {
		_random.resize(_random.size() + itemValue, _prototypes.size());
		_prototypes.push_back({ std::make_unique<Weapon>(texture, name, damage), itemValue });
	}

	void addPotion(std::string texture, std::string name, std::function<void(Entity&)> effect, ItemValue itemValue) {
		_random.resize(_random.size() + itemValue, _prototypes.size());
		_prototypes.push_back({ std::make_unique<Potion>(texture, name, effect) , itemValue });
	}

	void addKey(std::string texture, std::string name, ItemValue itemValue) {
		_random.resize(_random.size() + itemValue, _prototypes.size());
		_prototypes.push_back({ std::make_unique<Key>(texture, name), itemValue });
	}

	GameItemsSet() {
		addKey("golden_key.png", "Golden key", BELOW_REGULAR);
		addKey("potion_a.png", "Red potion", BELOW_REGULAR);
		addKey("potion_b.png", "Blue potion", BELOW_REGULAR);
		addKey("potion_c.png", "Green potion", REGULAR);
		addKey("sword_a.png", "Green potion", RARE);
		addKey("sword_b.png", "Green potion", BELOW_REGULAR);
		addKey("sword_b.png", "Green potion", BELOW_REGULAR);
		addKey("wand.png", "Green potion", VERY_RARE);
	}
public:
	GameItemsSet(GameItemsSet &other) = delete;
	void operator=(const GameItemsSet &) = delete;

	static GameItemsSet & getInstance() {
		if (_instance == nullptr)
			_instance = new GameItemsSet();

		return *_instance;
	}

	std::unique_ptr<Item> getRandomItem() {
		
		auto i = (std::rand()*std::rand()) % _random.size();
		return _prototypes[_random[i]]._prototype->clone();
	}

};

#endif