#ifndef DISPLAY_CONTAINER_H
#define DISPLAY_CONTAINER_H

#include "DisplayObject.h"

#include <vector>
#include <memory>

class DisplayContainer final : public DisplayObject {
private:
	std::vector<DisplayObject*> _objects;

public:
	enum Layout{NONE, VERTICAL, HORIZONTAL};
	DisplayContainer() = default;


	void sendEvent(const sf::Event & event) override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void updateGlobalPosition(float x, float y) override;

	float getWidth() const override;
	float getHeight() const override;

	void addObject(DisplayObject & object, Layout layout = NONE, float spacing = 0.f);

};

#endif 
