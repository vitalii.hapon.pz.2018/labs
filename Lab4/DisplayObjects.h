#ifndef DISPLAY_OBJECTS_H
#define DISPLAY_OBJECTS_H

#include "DisplayObject.h"

#include <SFML/Graphics.hpp>

#include <string>
#include <functional>

#include "EventManager.h"

class Sprite final : public DisplayObject {
private:
	float _gx = 0.f, _gy = 0.f;
	sf::Sprite _sprite;
	
public:
	Sprite();
	Sprite(const std::string &name);

	void updateGlobalPosition(float x = 0.f, float y = 0.f) override;
	float getWidth() const override;
	float getHeight() const override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	bool contains(float x, float y);

	void setTexture(std::string name);
	void setTexture(const sf::Texture & texture);

	void update();
};

class Text final : public DisplayObject {
private:
	sf::Text _text;

public:
	Text() = default;
	Text(const std::string &text, const sf::Font & font, unsigned int size = 30);

	void setSize(unsigned int size);
	void setText(const std::string &text);
	void setColor(sf::Color color);

	void updateGlobalPosition(float x = 0.f, float y = 0.f) override;
	float getWidth() const override;
	float getHeight() const override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;


};

class TextInput final : public DisplayObject, public IEventListener {
private:
	float _gx, _gy;
	Text _placeHolderText;
	mutable Text _inputText;
	Sprite _background;
	size_t maxSize = 22;

	std::string _inputString;

	std::function<void()> _onEnter;

	void updateGlobalPosition(float x, float y) override;
	void _updatePosition();
public:

	TextInput(std::string textureName, std::string placeholder, const sf::Font &font, unsigned int fontSize = 30, sf::Color textColor = sf::Color(255, 255, 255, 255));
	
	void onEnter(std::function<void()> handler);
	void sendEvent(const sf::Event & event) override;

	float getWidth() const override;
	float getHeight() const override;

	std::string getText() const;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

class Button final: public DisplayObject, public IEventListener {
private:
	float _gx, _gy;

	bool _hover = false;
	bool _press = false;

	Sprite _idleBackground;
	Sprite _hoverBackground;
	Sprite _pressBackground;
	Text _text;

	std::function<void()> _onHoverHandler;
	std::function<void()> _onClickHandler;
	std::function<void()> _onPressHandler;
	std::function<void()> _onReleaseHandler;

	void _onHover();
	void _onClick();
	void _onPress();
	void _onRelease();

	void _updatePosition();

public:
	Button() = default;
	Button(std::string textureName, std::string text, const sf::Font &font, unsigned int fontSize = 30, sf::Color textColor = sf::Color(255, 255, 255, 255));
	
	void updateGlobalPosition(float x, float y) override;
	
	void press(int x, int y);
	void release(int x, int y);
	void hover(int x, int y);
	void setText(const std::string & text);

	void onClick(std::function<void()> handler);
	void onPress(std::function<void()> handler);
	void onRelease(std::function<void()> handler);
	void onHover(std::function<void()> handler);

	void sendEvent(const sf::Event & event) override;


	float getWidth() const override;
	float getHeight() const override;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif

