#include "Inventory.h"

InventoryLogger::InventoryLogger() {
	_invetory = std::make_unique<Inventory>();
}

size_t InventoryLogger::getSelectedItemID() {
	return _invetory->getSelectedItemID();
}

Item & InventoryLogger::getItemByID(size_t id) {
	return _invetory->getItemByID(id);
}

void InventoryLogger::selectItem(size_t id) {
	_invetory->selectItem(id);

	auto iid = _invetory->getSelectedItemID();
	auto name = _invetory->getSelectedItemName();
	auto tex = _invetory->getSelectedItem().getName();

	std::cout << "InventoryLogger :: Item Seleceted with:" << std::endl;
	std::cout << "\t ID: " << iid << "; " << std::endl;
	std::cout << "\t Name: " << name << "; " << std::endl;
	std::cout << "\t Texutre name: " << tex << "; " <<  std::endl;
}
void InventoryLogger::fillWithRandomItems() {
	_invetory->fillWithRandomItems();

	std::cout << "InventoryLogger :: Inventory was filled with random itesm:" << std::endl;
	for (size_t i = 0; i < 5; i++) {
		std::cout << "\t "<< i << " :" << _invetory->getItemByID(i).getName() << "; " << std::endl;
	}
}

Item & InventoryLogger::getSelectedItem() {
	return _invetory->getSelectedItem();
}
std::string InventoryLogger::getSelectedItemName() {
	return _invetory->getSelectedItemName();
}


Inventory::Inventory() {
	_items.resize(5);
}
void Inventory::selectItem(size_t id) {
	if (id > 4)
		_selectedItem = 4;
	else 
		_selectedItem = id;
}

void Inventory::fillWithRandomItems() {
	auto &gis = GameItemsSet::getInstance();

	for (size_t i = 0; i < 5; i++) {
		_items[i] = std::move(gis.getRandomItem());
	}
}

Item & Inventory::getSelectedItem() {
	return *_items[_selectedItem];
}

Item & Inventory::getItemByID(size_t id) {
	if (id > 4)
		id = 4;

	return *_items[id];
}

std::string Inventory::getSelectedItemName() {
	return _items[_selectedItem]->getName();
}

size_t Inventory::getSelectedItemID() {
	return _selectedItem;
}