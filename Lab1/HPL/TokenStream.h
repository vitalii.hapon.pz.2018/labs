#ifndef TOKEN_STREAM
#define TOKEN_STREAM

#include <HPL/lexer/Lexer.h>
#include <HPL/utils/String.h>

#include <stack>

namespace hpl {
	class TokenStream {

	public:
		using Token = hpl::lexer::Token;
		using TokenType = hpl::lexer::TokenType;

		using Tokens = hpl::lexer::Tokens;
		using TokenIterator = hpl::lexer::TokenIterator;
		using String = hpl::utils::String;
	private:
		std::stack<TokenIterator> stack;

		String value;
		TokenType type;
		size_t line = 0;

		TokenIterator it;
		TokenIterator last;

		const TokenIterator begin;
		const TokenIterator end;
	public:
		TokenStream(Tokens &tokens) :
			it(tokens.begin()),
			last(tokens.begin()),
			begin(tokens.begin()),
			end(tokens.end()) {}

		void finish() {
			if(it != end)
				throw InterpretationException(InterpretationException::SYNTAX,
					"Unexpected tokens sequence", last->line);
		}
		TokenIterator advance() {
			if (it == end)
				throw InterpretationException(InterpretationException::SYNTAX,
					"Unexpected tokens sequence at the end of file");
			if(last != end)
				last++;
			return it++;
		}
		TokenIterator getIdentifier() {
			return it;
		}
		void setIdentifier(TokenIterator &i) {
			if (i > last)
				last = i;
			it = i;
		}
		bool available() {
			return it != end;
		}
		String currrentValue() {
			return it->value;
		}
		size_t currrentLine() {
			return it->line;
		}
		bool checkAndAdvance(String val) {
			bool res = check(val);
			if (res)
				++it;
			return res;
		}
		bool checkAndAdvance(TokenType type) {
			bool res = check(type);
			if (res)
				++it;
			return res;
		}
		bool checkAndAdvance(TokenType type, String val) {
			bool res = check(type, val);
			if (res)
				++it;
			return res;
		}
		bool check(String val) {
			if (it == end)
				throw InterpretationException(InterpretationException::SYNTAX,
					"Unexpected tokens sequence at the end of file");
			if (it->value == val) {
				value = it->value;
				type = it->type;
				line = it->line;
				return true;
			}
			return false;
		}
		bool check(TokenType type) {
			if (it == end)
				throw InterpretationException(InterpretationException::SYNTAX,
					"Unexpected tokens sequence at the end of file");
			if (it->type == type) {
				value = it->value;
				type = it->type;
				line = it->line;
				return true;
			}
			return false;
		}
		bool check(TokenType type, String val) {
			if (it == end)
				throw InterpretationException(InterpretationException::SYNTAX,
					"Unexpected tokens sequence at the end of file");
			if (it->type == type && it->value == val) {
				value = it->value;
				type = it->type;
				line = it->line;
				return true;
			}
			return false;
		}
	};
}

#endif
