#include <HPL/HPLProgram.h>

#include <HPL/parser/ProgramNode.h>
#include <HPL/lexer/Tokenizer.h>
#include <HPL/Context.h>
#include <HPL/TokenStream.h>

#include <stdexcept>

using namespace hpl;
using namespace hpl::parser;
using namespace hpl::utils;
using namespace hpl::lexer;


//--------------------------------------------------------------------------------
HPLProgram::HPLProgram(String text) {	
	Tokenizer lexer;
	auto tokens = lexer.tokenize(text);
	
		TokenStream stream(tokens);
		program = ProgramNode::create(stream);
		stream.finish();
	
	
}
//--------------------------------------------------------------------------------
const hpl::Context& HPLProgram::getContext() const {
	return currentContext;
}
//--------------------------------------------------------------------------------
void HPLProgram::optimize() {
	Value ores;
	if (program)
		program->tryOptimize(ores);
}
//--------------------------------------------------------------------------------
void HPLProgram::run() {
	currentContext = defaultContext;
	if (program) {
		program->solve(currentContext);
	}
}
//--------------------------------------------------------------------------------
VisualizationTree HPLProgram::createVisualizationTree() {
	VisualizationTree tmp;
	tmp.root = program->createVisualizationTreeNode();
	return tmp;
}
//--------------------------------------------------------------------------------
void HPLProgram::addUserFunction(String name, std::vector<String> args, std::function<Value(Context &context)> function) {
	defaultContext.addUserFunction(name, args, function);
}
//--------------------------------------------------------------------------------
