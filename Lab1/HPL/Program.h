#ifndef PROGRAM_H
#define PROGRAM_H

#include <HPL/OperatorsList.h>
#include <HPL/parser/ProgramNode.h>
#include <HPL/parser/AbstractNode.h>
#include <HPL/Value.h>
#include <HPL/utils/String.h>
#include <HPL/Context.h>
#include <HPL/VisualizationTree.h>

namespace hpl {
	class Program {
	private:
		static OperatorsList operators;

		Context context;
		AbstractNode::NodeRef program;
	public:
		using String = utils::String;
		Program(String text);
		void optimize();
		void run();
		VisualizationTree createVisualizationTree();
		void addUserFunction(String name, std::function<Value(Context &context)> function);
	};;
}
#endif 
