#ifndef IDENTIFIER_NODE_H
#define IDENTIFIER_NODE_H

#include <HPL/parser/AbstractTerminalNode.h>
#include <HPL/Context.h>

class hpl::parser::IdentifierNode : public AbstractTerminalNode {
private:
	String identifier;
public:
	static Ref<IdentifierNode> create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::IDENTIFIER)) {
			Ref<IdentifierNode> variableNode = std::move(alloc<IdentifierNode>());
			variableNode->identifier = stream.currrentValue();
			variableNode->line = stream.currrentLine();
			stream.advance();
			return variableNode;
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	String getIdentifierName() {
		return identifier;
	}
	Value solve(Context &context) override {
		return context.getVariable(identifier);
	}
	bool tryOptimize(Value & res) override {
		return false;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "ID " + identifier;
		return res;
	}
};
#endif