#ifndef FUNCTION_NODE_H
#define FUNCTION_NODE_H

#include <HPL/parser/AbstractNode.h>
#include <HPL/parser/IdentifierNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/parser/StatementBlockNode.h>

#include <vector>

class hpl::parser::FunctionNode : public AbstractNode {
private:
	Ref<IdentifierNode> functionName;
	std::vector<Ref<IdentifierNode>> args;
	Ref<StatementBlockNode> body;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.checkAndAdvance(TokenType::KEYWORD, "function")) {
			
			if (auto variableNode = IdentifierNode::create(stream)) {
				if (stream.checkAndAdvance(TokenType::LPAREN)) {
					

					Ref<FunctionNode> functionNode = alloc<FunctionNode>();
					if (auto argNode = IdentifierNode::create(stream)) {
						functionNode->args.push_back(std::move(argNode));
						while (true) {
							if (stream.check(TokenType::COMMA)) {

								auto saved_2 = stream.advance();
								if (auto argNode = IdentifierNode::create(stream)) {
									functionNode->args.push_back(std::move(argNode));
								}
								else {
									stream.setIdentifier(saved_2);
									break;
								}
							}
							else
								break;
						}
					}
					if (stream.checkAndAdvance(TokenType::RPAREN)) {
						
						if (Ref<StatementBlockNode> statementBlockNode = StatementBlockNode::create(stream)) {
							functionNode->functionName = std::move(variableNode);
							functionNode->body = std::move(statementBlockNode);
							return functionNode;
						}
					}
				}
			}
		}
	
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		std::vector<String> argsNames;
		for (auto &arg : args) {
			argsNames.push_back(arg->getIdentifierName());
		}

		auto f = [](Ref<StatementBlockNode> &ref, Context &context) {
			 ref->solve(context);
		};
		auto function = std::bind(f, std::ref(body), std::placeholders::_1);

		context.addFunction(functionName, function, argsNames);

		return Value();
	}

	bool tryOptimize(Value & res) override {
		Value ores;
		body->tryOptimize(ores);
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Function: ";

		auto tmp = VisualizationTree::alloc();
		tmp->name = "NAME";
		tmp->nodes.push_back(functionName->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		for (size_t i = 0; i < args.size(); i++) {
			tmp = VisualizationTree::alloc();
			tmp->name = "ARG_" + std::to_string(i + 1);
			tmp->nodes.push_back(args[i]->createVisualizationTreeNode());

			res->nodes.push_back(std::move(tmp));
		}
		tmp = VisualizationTree::alloc();
		tmp->name = "BODY";
		tmp->nodes.push_back(body->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));
	
		return res;
	}

};
#endif