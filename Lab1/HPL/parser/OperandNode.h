#ifndef PROGRAM_NODE_H
#define PROGRAM_NODE_H

#include <HPL/parser/AbstractNode.h>
#include <HPL/parser/AbstractTerminalNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/parser/FunctionCallNode.h>

class hpl::parser::OperandNode : public AbstractNode {
private:
	NodeRef node;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (auto functionCallNode = FunctionCallNode::create(stream))
			return functionCallNode;

		if (stream.checkAndAdvance(TokenType::LPAREN)) {
			
			if (auto expressionNode = ExpressionNode::create(stream)) {
				if (stream.checkAndAdvance(TokenType::RPAREN)) {
					
					return expressionNode;
				}
			}
		}

		stream.setIdentifier(saved);
		if (auto abstractTerminalNode = AbstractTerminalNode::create(stream))
			return abstractTerminalNode;

		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		return node->solve(context);
	}

	bool tryOptimize(Value & res) override {
		Value ores;
		if (node->tryOptimize(ores)) {
			node = AbstractTerminalNode::create(ores);
		}
		return false;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Operand: ";
		res->nodes.push_back(node->createVisualizationTreeNode());
		return res;
	}

};
#endif