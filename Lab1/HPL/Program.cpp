#include <HPL/Program.h>

#include <HPL/parser/ProgramNode.h>
#include <HPL/lexer/Tokenizer.h>
#include <HPL/Context.h>

using namespace hpl;
using namespace hpl::parser;
using namespace hpl::utils;
using namespace hpl::lexer;


//--------------------------------------------------------------------------------
Program::Program(String text) {	
	Tokenizer lexer;
	auto res = lexer.tokenize(text);
	auto beg = res.begin();
	program = ProgramNode::create(beg, res.end());	
}
//--------------------------------------------------------------------------------
void Program::optimize() {

}
//--------------------------------------------------------------------------------
void Program::run() {
	if (program) {
		program->solve(context);
	}
	else {
		throw InterpretationException(InterpretationException::LOGICAL,
			"Cannot create syntax tree", identifierNode->getLine());
	}
}
//--------------------------------------------------------------------------------
VisualizationTree Program::createVisualizationTree() {
	VisualizationTree tmp;
	tmp.root = program->createVisualizationTreeNode();
	return tmp;
}
//--------------------------------------------------------------------------------
void Program::addUserFunction(String name, std::function<Value(Context &context)> function) {
	context.addUserFunction(name, function);
}
//--------------------------------------------------------------------------------
