#ifndef RULE_H
#define RULE_H

#include <HPL/lexer/Lexer.h>
#include <HPL/utils/String.h>

class hpl::lexer::Rule{
public:
	using Char = hpl::utils::Char;
	using String = hpl::utils::String;
	using StringConstIterator = hpl::utils::String::const_iterator;

	virtual bool match(StringConstIterator &beg, const StringConstIterator &end) const = 0;
	virtual Rule* clone() const = 0;
	virtual String toString() const = 0;
	virtual ~Rule() = 0 {};
};

#endif