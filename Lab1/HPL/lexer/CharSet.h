#ifndef CHAR_SET_H
#define CHAR_SET_H

#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Rule.h>
#include <HPL/utils/String.h>

#include <set>


class hpl::lexer::CharSet: public Rule{
private:
	std::set<Char> chars;
	std::set<std::pair<Char, Char>> charRanges;
public:
	enum CharClass{ 
		ANY = 0b0000'0001,
		SPACE = 0b0000'0010,
		ALPHA = 0b0000'0100,
		DIGIT = 0b0000'1000,
		HEX_DIGIT = 0b0001'0000,
		LOWER = 0b0010'0000,
		UPPER = 0b0100'0000,
		PUNCT = 0b1000'0000
	};
	
	CharSet() = default;
	CharSet(const CharSet&) = default;
	CharSet(CharSet&&) = default;
	CharSet& operator=(const CharSet&) = default;

	Rule* clone() const override;

	CharSet(Char from, Char to);
	CharSet(Char ch);
	CharSet(String chars);
	CharSet(CharClass charClass);

	CharSet operator+(const CharSet& v) const;
	CharSet operator/(const std::set<Char>& v) const;

	CharSet& operator+=(const CharSet& v);

	String toString() const override;

	bool match(StringConstIterator &beg, const StringConstIterator &end) const override;
};
#endif