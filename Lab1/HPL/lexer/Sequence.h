#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Rule.h>
#include <HPL/lexer/Group.h>
#include <HPL/utils/String.h>


#include <set>
#include <vector>
#include <initializer_list>

class hpl::lexer::Sequence: public Rule {
private:
	std::vector<Rule*> sequences;
	std::vector<Rule*> cloneSequences() const;

public:
	Sequence() = default;
	Sequence(const Sequence&);
	Sequence(Sequence&&) = default;
	Sequence& operator=(const Sequence&);
	~Sequence();

	template<typename ...Args>
	Sequence(const Rule& seq, const Args& ...args);

	Rule *clone() const;

	Sequence operator+(const Sequence &v) const;
	Sequence operator*(unsigned int v) const;

	Sequence& operator+=(const Sequence &v);
	Sequence& operator+=(const Rule &v);

	String toString() const override;

	bool match(StringConstIterator &begin, const StringConstIterator &end) const;
};

template<typename ...Args>
hpl::lexer::Sequence::Sequence(const Rule& seq, const Args& ...args){
	sequences.push_back(seq.clone());
	(sequences.push_back(args.clone()), ...);
}
#endif 