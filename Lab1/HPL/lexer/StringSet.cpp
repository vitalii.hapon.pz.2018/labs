#include <HPL/lexer/StringSet.h>

using namespace hpl::lexer;
using namespace hpl::utils;

//--------------------------------------------------------------------------------
StringSet::StringSet(std::initializer_list<String> list):
	strings(list){}
//--------------------------------------------------------------------------------
Rule* StringSet::clone() const {
	return new StringSet(*this);
}
//--------------------------------------------------------------------------------
StringSet StringSet::operator+(const StringSet& v) const {
	auto res = *this;
	res.strings.insert(v.strings.begin(), v.strings.end());
	return res;
}
//--------------------------------------------------------------------------------
StringSet& StringSet::operator+=(const StringSet& v) {
	strings.insert(v.strings.begin(), v.strings.end());
	return *this;
}
String StringSet::toString() const{
	String string;
	string += "[ ";
	bool  comma = false;
	for (auto v : strings) {
		if (comma)
			string += ", ";
		else
			comma = true;
		string += v;
	}
	string += " ]";
	return string;
}
//--------------------------------------------------------------------------------
bool StringSet::match(StringConstIterator &beg, const StringConstIterator &end) const {
	if (beg >= end)
		return false;

	if (beg >= end)
		return false;

	// check each string until the matching string is found:
	for (auto &it : strings) {
		auto strSize = it.size();
		if (static_cast<size_t>(end - beg) >= strSize)
			if (it == std::string(beg, beg + strSize)) {
				beg += strSize;
				return true;
			}
	}
	return false;
}
//--------------------------------------------------------------------------------