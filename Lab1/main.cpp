
#include <memory>
#include <iostream>
#include <string>

#include <fstream>
#include <HPL/lexer/Tokenizer.h>
#include <HPL/parser/ProgramNode.h>
#include <HPL/InterpretationException.h>
#include <HPL/Context.h>
#include <HPL/HPLProgram.h>

using namespace hpl::lexer;
using namespace hpl::parser;

int main() {

	std::ifstream file("examples/example_1.txt");

	std::string str;

	file.seekg(0, std::ios::end);
	str.reserve(file.tellg());
	file.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
	try {
		hpl::HPLProgram program{ str };

		program.addUserFunction("printStack", {}, [](auto c) {
			std::cout << c.toString();
			return hpl::Value();
		});

		program.addUserFunction("print", { "a" }, [](hpl::Context &c) {
			std::cout << c.getVariable("a").toString();
			return hpl::Value();
		});
		program.run();
		std::cout << program.createVisualizationTree().toString();

		program.optimize();
		program.optimize();

		program.run();
		std::cout << program.createVisualizationTree().toString();


	}
	catch (hpl::InterpretationException &e) {
		std::cout << e.what();
	}
	//pn->solve(c);
	std::cout << std::endl;
	/*for (auto i : c.global) {
		std::cout << i.first << ": " << i.second.toString() << std::endl;
	}*/

}