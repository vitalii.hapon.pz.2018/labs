#include "Model3D.h"
#include "ResourceManager.h"

#include <glad/glad.h>
#include <SFML/OpenGL.hpp>

const std::vector<Model3D::Object> & Model3D::getObjects() const {
	return _objects;
}

void Model3D::draw() {
	ResourceManager & resMan = ResourceManager::getInstance();
	for (auto &obj : _objects) {

		glEnableVertexAttribArray(0); 
		glEnableVertexAttribArray(1);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), obj.vCoords.data());
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), obj.texCoords.data());

		auto &tex = resMan.loadTexture(obj.textureName);
		glEnable(GL_TEXTURE_2D);
		sf::Texture::bind(&tex);

		glDrawArrays(GL_TRIANGLES, 0, obj.vCoords.size() / 3);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
	}
}

void Model3D::createNewObject(std::string textureName) {
	_objects.push_back({});
	_objects[_objects.size() - 1].textureName = textureName;
}

void Model3D::addFace(FaceType faceType, Face &face) {

}

void Model3D::addXYFace(bool clockwise, Face &face) {
	auto[x, y, z, w, h, u1, u2, v1, v2] = face;
	std::vector<float> v;
	std::vector<float> t;
	std::vector<float> n;

	if (clockwise) {
		v = {
		x		, y		, z ,	// 1
		x + w	, y		, z ,	// 2
		x		, y + h , z ,	// 3
		x		, y + h , z ,	// 3
		x + w	, y		, z ,	// 2
		x + w	, y + h	, z ,   // 4
		};

		n = {
			-1, -1, -1,
			-1,  1, -1,
			 1, -1, -1,
			-1,  1, -1,
			 1, -1, -1,
			 1,  1, -1,
		};

		t = {
			u1, v2,
			u2, v2,
			u1, v1,
			u1, v1,
			u2, v2,
			u2, v1
		};
	}
	else {
		v = {
		x		, y		, z ,	// 1
		x		, y + h , z ,	// 2
		x + w	, y		, z ,	// 3
		x + w	, y		, z ,	// 3
		x		, y + h , z ,	// 2
		x + w	, y + h	, z ,   // 4
		};

		n = {
			-1, -1, 1,
			-1,  1, 1,
			 1, -1, 1,
			 1, -1, 1,
			-1,  1, 1,
			 1,  1, 1,
		};

		t = {			
			u1, v2,
			u1, v1,
			u2, v2,
			u2, v2,			
			u1, v1,
			u2, v1
		};
	}
	if (_objects.end() != _objects.begin()) {
		auto last = _objects.end() - 1;

		last->nCoords.insert(last->nCoords.end(), n.begin(), n.end());
		last->texCoords.insert(last->texCoords.end(), t.begin(), t.end());
		last->vCoords.insert(last->vCoords.end(), v.begin(), v.end());
	}
}
void Model3D::addXZFace(bool clockwise, Face &face) {
	auto[x, y, z, w, h, u1, u2, v1, v2] = face;


	std::vector<float> n;
	std::vector<float> t;
	std::vector<float> v;
	if (clockwise) {
		v = {
		   x		, y , z,		// 1
		   x + w	, y , z,		// 2
		   x		, y	, z + h,	// 3
		   x		, y , z + h,	// 3
		   x + w	, y , z,		// 2
		   x + w	, y , z + h,    // 4
		};
		t = {
			u1, v1,
			u2, v1,
			u1, v2,
			u1, v2,
			u2, v1,
			u2, v2
		};
		n = {
			-1, -1, -1,
			 1, -1, -1,
			-1, -1,  1,
			-1, -1,  1,
			 1, -1, -1,
			 1, -1,  1,
		};
	}
	else {
		v = {
		   x		, y , z,		// 1
		   x		, y	, z + h,	// 3
		   x + w	, y , z,		// 2
		   x + w	, y , z,		// 2
		   x		, y , z + h,	// 3   
		   x + w	, y , z + h,    // 4
		};
		t = {
			u1, v1,
			u1, v2,
			u2, v1,
			u2, v1,
			u1, v2,		
			u2, v2
		};
		n = {
			-1, 1, -1,
			-1, 1,  1,
			 1, 1, -1,
			 1, 1, -1,
			-1, 1,  1,		
			 1, 1,  1,
		};
	}
	if (_objects.end() != _objects.begin()) {
		auto last = _objects.end() - 1;

		last->nCoords.insert(last->nCoords.end(), n.begin(), n.end());
		last->texCoords.insert(last->texCoords.end(), t.begin(), t.end());
		last->vCoords.insert(last->vCoords.end(), v.begin(), v.end());
	}
}
void Model3D::addYZFace(bool clockwise, Face &face) {
	auto[x, y, z, w, h, u1, u2, v1, v2] = face;
	std::vector<float> v;
	std::vector<float> t;
	std::vector<float> n;

	if (clockwise) {
		v = {
			x, y,  z + w,		// 3
			x, y + h, z,		// 2
			x, y, z,			// 1
			x, y + h, z + w,    // 4
			x, y + h, z,		// 2
			x, y,  z + w,		// 3	
		};

		t = {
			u2, v2,
			u1, v1,
			u1, v2,
			u2, v1,
			u1, v1,
			u2, v2
		};

		n = {
			-1, -1, -1,
			-1, 1, -1,
			-1, -1, 1,
			-1, -1, 1,
			-1, 1, -1,
			-1, 1, 1,
		};
	}
	else {
		v = {
			x, y, z,			// 1
			x, y + h, z,		// 2
			x, y,  z + w,		// 3
			x, y,  z + w,		// 3
			x, y + h, z,		// 2
			x, y + h, z + w,    // 4
		};

		t = {
			u2, v2,		
			u2, v1,
			u1, v2,
			u1, v2,
			u2, v1,
			u1, v1
		};

		n = {
			1, -1, -1,
			1, 1, -1,
			1, -1, 1,
			1, -1, 1,
			1, 1, -1,
			1, 1, 1,
		};
	}
	if (_objects.end() != _objects.begin()) {
		auto last = _objects.end() - 1;

		last->nCoords.insert(last->nCoords.end(), n.begin(), n.end());
		last->texCoords.insert(last->texCoords.end(), t.begin(), t.end());
		last->vCoords.insert(last->vCoords.end(), v.begin(), v.end());
	}
}

void Model3D::addBox(float x, float y, float z, float w, float h, float d) {
	std::vector<float> t = {
		0, 0,
		1, 0,
		0, 1,
		0, 1,
		1, 0,
		1, 1
	};
}



