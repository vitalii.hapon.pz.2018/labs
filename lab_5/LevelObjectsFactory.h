#ifndef LEVEL_OBJECT_FACTORY_H
#define LEVEL_OBJECT_FACTORY_H

#include "Chest.h"
#include "Item.h"
#include "Enemy.h"
#include <memory>

class LevelObjectsFactory {
public:
	
	virtual std::unique_ptr<Enemy> createEnemy() = 0;
	virtual std::unique_ptr<Chest>   createChest() = 0;
};

class EeasyLevelObjectsFactory: public LevelObjectsFactory {
public:
	std::unique_ptr<Enemy>  createEnemy() override{
		return nullptr;// std::move(std::make_unique<EasyEnemy>());
	};
	std::unique_ptr<Chest>  createChest() override {
		return nullptr;//std::move(std::make_unique<EasyChest>());
	};
};

class NormalLevelObjectsFactory : public LevelObjectsFactory {
public:
	std::unique_ptr<Enemy> createEnemy() override {
		return nullptr;//std::move(std::make_unique<NormalEnemy>());
	};
	std::unique_ptr<Chest>  createChest() override {
		return nullptr;//std::move(std::make_unique<NormalChest>());
	};
};

class HardLevelObjectsFactory : public LevelObjectsFactory {
public:
	std::unique_ptr<Enemy>  createEnemy() override {
		return nullptr;//std::move(std::make_unique<HardEnemy>());
	};
	std::unique_ptr<Chest>  createChest() override {
		return nullptr;//std::move(std::make_unique<HardChest>());
	};
};

#endif