#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <map>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class ResourceManager {
protected:
	static ResourceManager* _instance;

	sf::SoundBuffer _missingSound;
	sf::Texture _missingTexure;
	
	std::map<std::string, sf::Texture*> _textures;
	std::map<std::string, sf::SoundBuffer*> _soundBuffers;

	float _soundVolume = 1.f;

	ResourceManager();
	
public:
	ResourceManager(ResourceManager &other) = delete;
	void operator=(const ResourceManager &) = delete;

	static ResourceManager & getInstance();

	sf::SoundBuffer & loadSound(const std::string &path);
	sf::Texture & loadTexture(const std::string &path);
	
	sf::Sound getSound(const std::string &path);

	void setSoundVolume(float val);
	float getSoundVolume() const;
};
#endif