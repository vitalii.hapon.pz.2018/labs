#ifndef LEVEL_H
#define LEVEL_H

#include "Room.h"
#include "Enemy.h"
#include "Chest.h"
#include "Player.h"

#include <ctime>
#include <tuple>
#include <iostream>
#include <SFML/Graphics.hpp>

#include "SerializedObject.h"

class LevelGenerator;

class Level: public ISerializable {
	friend LevelGenerator;
protected:
public:
	Room _mainRoom;

	Player _player;

	std::vector<std::unique_ptr<Enemy>> _enemies;
	std::vector<std::unique_ptr<Chest>> _chests;

	const float frameRate = 60.f;

	float _prevClock = 0;
public:

	Level(const SerializedObject & object){
		deserialize(object);

		for (size_t i = 0; i < 10; i++) {
			_enemies.push_back(std::make_unique<NormalEnemy>("ghost.png", 1.0, 100));
			_enemies[i]->setX(std::rand() % 10 + i);
			_enemies[i]->setY(std::rand() % 10 + i);
		}
		_enemies[0]->setX(3);
		_enemies[0]->setY(3);

	}

	Level(const Room & room):
		_mainRoom(room){

		for (size_t i = 0; i < 10; i++) {	
			_enemies.push_back(std::make_unique<NormalEnemy>("ghost.png", 1.0, 100));
			_enemies[i]->setX(std::rand() % 10 + i);
			_enemies[i]->setY(std::rand() % 10 + i);
		}
		_enemies[0]->setX(3);
		_enemies[0]->setY(3);

	}


	void calculatePlayerPhysics() {
		float half = _player.getSize() / 2.f;

	
		auto checkBlock = [&](float x, float y) {
			size_t xi = std::floor(x);
			size_t yi = std::ceil(y);

			if (xi >= 0 && xi < _mainRoom.getWidth()) {
				if (yi >= 0 && yi < _mainRoom.getHeight()) {
					if (_mainRoom.getRawData()[xi][yi]._type != Room::WALL)
						return false;
				}
			}
			return true;
		};

		if (float(std::clock()) - _prevClock > 1.f) {

			auto playerSavedX = _player.getX();
			auto playerSavedY = _player.getY();

			/*std::cout
				<< playerSavedX
				<< " : "
				<< playerSavedY << " :: "
				<< (_mainRoom.getRawData()[std::floor(playerSavedX)][std::floor(playerSavedY)]._type == Room::FLOOR)
				<< std::endl;*/
 
			auto time = std::clock() - _prevClock;
			if(time < 100.f)
				_player.move(time);
			_prevClock = std::clock();
			auto x = _player.getX();
			auto y = _player.getY();

			bool xtest = false, ytest = false;

			if (checkBlock(x + half,  y) || checkBlock(x - half, y)) {
				_player.setX(playerSavedX);
			}

			if (checkBlock(x , y + half) || checkBlock(x , y - half)) {			
				_player.setY(playerSavedY);
			}

			x = _player.getX();
			y = _player.getY();


			if (checkBlock(x + half, y + half)) {
				if (std::fabs(std::floor(x + half) - x) > std::fabs(std::ceil(y + half) - y))
					_player.setX(x - std::fabs(std::floor(x + half) - x));
				else
					_player.setY(y + std::fabs(std::ceil(y + half) - y));
			}
			else if (checkBlock(x - half, y - half)) {
				if (std::fabs(std::floor(x - half) - x) < std::fabs(std::ceil(y - half) -y))
					_player.setX(x + std::fabs(std::floor(x - half) - x));
				else
					_player.setY(y + std::fabs(std::ceil(y - half) - y));
			}
			else if (checkBlock(x - half, y + half)) {
				if (std::fabs( std::ceil(x - half) - x) < std::fabs(std::floor(y + half) - y))
					_player.setX(x + std::fabs(std::ceil(x - half) - x));
				else
					_player.setY(y - std::fabs(std::floor(y + half) - y));
			}
			else if (checkBlock(x + half, y - half)) {
				if (std::fabs(std::floor(x + half) - (x + half)) < std::fabs(std::ceil(y - half) - (y - half)))
					_player.setX(x - std::fabs(std::floor(x + half) - (x + half)));
				else
					_player.setY(y + std::fabs(std::ceil(y - half) - (y - half)));
					
			}
		}
	}

	Player & getPlayer() {
		return _player;
	}

	std::vector<std::unique_ptr<Enemy>> & getEnemies() {
		return _enemies;
	}

	Room & getMainRoom() {
		return _mainRoom;
	}

	std::vector<std::unique_ptr<Chest>> & getChests() {
		return _chests;
	}

	SerializedObject serialize() const override {
		SerializedObject tmp;

		tmp.store("_player", _player);
		tmp.store("_mainRoom", _mainRoom);

		return tmp;
	}

	void deserialize(const SerializedObject &object) override {
		object.load("_player", _player);
		object.load("_mainRoom", _mainRoom);
	}
};


#endif