#ifndef COMMAND_H
#define COMMAND_H

#include <memory>

class Entity;

class Command {
public:
	Command() = default;
	Command(const Command&) = default;
	virtual bool execute(Entity &receiver) = 0;
};

using CommandPtr = std::unique_ptr<Command>;

class HealCommand : public Command {
private:
	int _amount;
public:
	HealCommand(int amount = 15);
	HealCommand(const HealCommand&) = default;
	bool execute(Entity &receiver) override;
};

class KillCommand : public Command {
public:
	KillCommand();
	KillCommand(const KillCommand&) = default;
	bool execute(Entity &receiver) override;
};

class IncreaseSpeedCommand : public Command {
private:
	float _amount;
public:
	IncreaseSpeedCommand(float amount);
	IncreaseSpeedCommand(const IncreaseSpeedCommand&) = default;
	bool execute(Entity &receiver) override;
};

class MoveCommand : public Command {
private:
	float _xvel;
	float _yvel;
public:
	MoveCommand(float xvel, float yvel);
	MoveCommand(const MoveCommand&) = default;
	bool execute(Entity &receiver) override;
};

class DamageCommand : public Command {
private:
	int _damage;
public:
	DamageCommand(int damage) {}
	DamageCommand(const DamageCommand&) = default;
	bool execute(Entity &receiver) override;
};

class IdleCommand : public Command {

public:
	IdleCommand() {}
	IdleCommand(const IdleCommand&) = default;
	bool execute(Entity &receiver) override { return true; }
};

#endif