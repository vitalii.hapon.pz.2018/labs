#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include <SFML/Window/Event.hpp>
#include <vector>
#include <algorithm>

class IEventListener {
public:
	virtual void sendEvent(const sf::Event &event) = 0;
};

class EventManager {
private:
	std::vector<IEventListener*> _listeners;
public:
	void subscribe(IEventListener &listener);
	void unsubscribe(IEventListener &listener);
	void notify(const sf::Event &event);

};

#endif