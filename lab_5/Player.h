#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"
#include "Inventory.h"

class Game;

class Player: public Entity {
	friend Game;
private:
	float _verticalViewAngle;
	
public:
	Player(){

		_maxSpeed = 0.0025f;
		_size = 0.5;
		_health = 100;
	}

	float getVerticalViewAngle() const{
		return _verticalViewAngle;
	}

	float setVerticalViewAngle(float value)  {
		if (value > 70.f)
			value = 70;
		else if (value < -70)
			value = -70;

		_verticalViewAngle = value;
		return value;
	}

};

#endif