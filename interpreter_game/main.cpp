#include <SFML/Graphics.hpp>

#include <HPL/HPLProgram.h>

#include <iostream>
#include <Game.h>

int main(){

	sf::RenderWindow window(sf::VideoMode(400, 400), "Game");
	Control::window = &window;
	Game gm = Game(&window);

	return 0;
}