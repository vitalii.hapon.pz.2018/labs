#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>

#include <vector>
#include <atomic>
#include <string>
#include <mutex>
#include <chrono>
#include <thread>
#include <HPL/HPLProgram.h>

#include "ResourceManager.h"

#include "Control.h"

namespace {
	std::mutex gmutex;
}

struct Player {
	int x;
	int y;
	int steps_left;
	int code_complexity;

};
class GameLevel {
public:
	int _x, _y, _w, _h, _max_code_complexity, _max_steps , _steps_done;


	enum Direction {UP='w', RIGHT='d', LEFT='a', DOWN='s'};
	enum State {OK, GAME_OVER, STUCKED, DONE, TO_COMPLEX};
	enum Block { WALL ='O' , SPACE=' ', SPIKES='X', EXIT='E' };

	State state;

	std::vector<std::string> level;

	GameLevel(const GameLevel&) = default;

	GameLevel(int sx, int sy, int w, int h, int max_code_complexity, int max_steps) {
		_x = sx;
		_y = sy;
		_w = w;
		_h = h;
		_max_code_complexity = max_code_complexity;
		_max_steps = max_steps;
	}

	State movePlayer(Direction dir) {
		auto check = [this](auto block) {
			if (block == WALL) {
				return STUCKED;
			}
			else if (block == SPACE) {
				this->_steps_done++;
				return OK;
			}
			else if (block == EXIT) {
				return DONE;
			}
			else if (block == SPIKES) {
				return GAME_OVER;
			}
		};
		if(dir == UP){
			auto tmp = check(level[_y - 1][_x]);
			if (tmp == OK) {
				_y--;
			}
			return tmp;		
		}
		else if (dir == DOWN) {
			auto tmp = check(level[_y + 1][_x]);
			if (tmp == OK) {
				_y++;
			}
			return tmp;
		}
		else if (dir == RIGHT) {
			auto tmp = check(level[_y][_x + 1]);
			if (tmp == OK) {
				_x++;
			}
			return tmp;
		}
		else {
			auto tmp = check(level[_y][_x - 1]);
			if (tmp == OK) {
				_x--;
			}
			return tmp;
		}
	}

};


class Game {
public:
	struct Instruction {
		enum Type { W, A, S, D, NAME, SAY } type;
		std::string data;
	};
	Game(sf::RenderWindow *rw) {

		std::vector<Instruction> instructions;
		size_t ip = 0;
		std::string name = "Unknow";

		sf::Font font;
		font.loadFromFile("resources/ebd.ttf");

		sf::Text textToSay;
		textToSay.setFont(font);

		int prevX = 0, prevY = 0;
		int curX = 0, curY = 0;
	
		
		GameLevel lvl_1 = { 5, 4, 7, 7, 100, 30 };
		lvl_1.level = { "OOOOOOO",
						"O  X  O",
						"O  X  O",
						"O     O",
						"OE O  O",
						"O  O  O",
						"OOOOOOO" };

		GameLevel lvl_2 = { 1, 1, 11, 11, 100, 30 };
		lvl_2.level = { "OOOOOOOOOOO",
						"O  X      O",
						"O  X  X  XO",
						"O  X  X  XO",
						"O  X      O",
						"O  X  X  XO",
						"O  O  X  EO",
						"O  X  X  XO",
						"O     X  XO",
						"O  X  O   O ",
						"OOOOOOOOOOO" };
		
		GameLevel lvl_3 = { 2, 10, 13, 13, 100, 30 };
		lvl_3.level = { "OOOOOOOOOOOOO",
						"O  O  O     O",
						"O     O  X  O",
						"O  O  O  X  O",
						"O  O  O  X  O",
						"O  O  O  X  O",
						"O  O     X  O",
						"O  OOOOOOO  O",
						"O  O        O",
						"O OOOO XXXXXO",
						"O  X     XXEO",
						"OXXX        O",
						"OOOOOOOOOOOOO" };
		
		auto runInterpreter = [&]() {
			instructions = {};
			hpl::HPLProgram program(Control::code);

			program.addUserFunction("moveUp", {}, [&](hpl::Context &c) {
				Instruction tmp;
				tmp.type = Instruction::W;
				instructions.push_back(tmp);
				return hpl::Value();
			});

			program.addUserFunction("moveDown", { }, [&](hpl::Context &c) {
				Instruction tmp;
				tmp.type = Instruction::S;
				instructions.push_back(tmp);
				return hpl::Value();
			});

			program.addUserFunction("moveLeft", {  }, [&](hpl::Context &c) {
				Instruction tmp;
				tmp.type = Instruction::A;
				instructions.push_back(tmp);
				return hpl::Value();
			});

			program.addUserFunction("moveRight", {}, [&](hpl::Context &c) {
				Instruction tmp;
				tmp.type = Instruction::W;
				instructions.push_back(tmp);
				return hpl::Value();
			});

			program.addUserFunction("say", { "a" }, [&](hpl::Context &c) {
				
				Instruction tmp;
				tmp.type = Instruction::SAY;
				tmp.data = c.getVariable("a").toString();
				instructions.push_back(tmp);
				return hpl::Value();
			});

			program.addUserFunction("changeName", { "a" }, [&](hpl::Context &c) {
				
				Instruction tmp;
				tmp.type = Instruction::NAME;
				tmp.data = c.getVariable("a").toString();
				instructions.push_back(tmp);
				return hpl::Value();
				return hpl::Value();
			});

			program.run();
		};

		while (Control::window == nullptr)
			std::this_thread::yield();
		
		sf::RenderWindow & window = *Control::window;
		sf::Sprite sp;


		auto block_to_name = [](char block) -> std::string {
			switch (block) {
			case GameLevel::WALL:
				return "wall.png";
				break;
			case GameLevel::SPACE:
				return "grass.png";
				break;
			case GameLevel::SPIKES:
				return "fire.png";
				break;
			case GameLevel::EXIT:
				return "finish.png";
				break;
			default:
				break;
			}
		};
		GameLevel tmpLvl = lvl_3;
		GameLevel *cr = &tmpLvl;
		int psize = 10;

		bool end = false;
		int endClock = 0;

		sf::RenderTexture renderTex = sf::RenderTexture();
		auto renderLevel = [&](GameLevel &lvl) {

			renderTex.create(lvl._w * 32, lvl._h * 32);

			auto &resMan = ResourceManager::getInstance();

			sf::Sprite tmp;
			tmp.setScale(2.f, 2.f);
			renderTex.clear();
			for (size_t xi = 0; xi < lvl._w; xi++) {
				for (size_t yi = 0; yi < lvl._h; yi++) {
					tmp.setTexture(resMan.loadTexture(block_to_name(lvl.level[yi][xi])));
					tmp.setPosition(xi * 32, yi * 32);
					renderTex.draw(tmp);
				}
			}

			tmp.setTexture(resMan.loadTexture("player.png"));
			tmp.setPosition(lvl._x * 32, lvl._y * 32);
			renderTex.draw(tmp);

			static auto clock = std::clock();

			if (std::clock() - clock > 250) {
				clock = std::clock();
				if (ip < instructions.size()) {
					GameLevel::State  state = GameLevel::State::OK;
					auto inst = instructions[ip];
					switch (inst.type ){
					case Instruction::W:
						state = lvl.movePlayer((GameLevel::Direction)'w');
						break;
					case Instruction::S:
						state = lvl.movePlayer((GameLevel::Direction)'s');
						break;
					case Instruction::D:
						state = lvl.movePlayer((GameLevel::Direction)'d');
						break;
					case Instruction::A:
						state = lvl.movePlayer((GameLevel::Direction)'a');
						break;
					case Instruction::SAY:
						textToSay.setString(name + ": " + inst.data);
						break;
					case Instruction::NAME:
						name = inst.data;
						break;
					default:
						break;
					}
					if (state == GameLevel::State::GAME_OVER) {
						textToSay.setString("GAME OVER");
						instructions = {};
						
						if (!end)
							endClock = std::clock();
						end = true;
						
					}
					else if (state == GameLevel::State::DONE) {
						textToSay.setString("WIN");
						instructions = {};
						
						if (!end)
							endClock = std::clock();
						end = true;
					}
					ip++;
				}
				else {
					if(!instructions.empty())
						textToSay.setString("GAME OVER");
					if(!end)
						endClock = std::clock();	
					end = true;
				}
			}
			if (end && std::clock() - endClock > 3000) {
				textToSay.setString("");
				name = "Uknown";
				tmpLvl = lvl_3;
				end = false;

				/*Control::run = true;
				Control::code =
					"var res = \"\"; \n"\
					"for(var i = 0; i < 3; i++){\n"\
					"    res += i; \n"\
					"}\n"\
					"say(res);"\
					"moveLeft();"\
					"moveUp();"\
					"moveUp();"\
					"moveUp();"\
					"moveUp();"\
					"moveUp();"\
					"moveUp();"\
					"moveUp();"\
					"moveUp();";*/
			}
			renderTex.display();		
			return renderTex.getTexture();
		};

		sf::Sprite mainSprite;

		prevX = 2;
		prevY = 10;
		curX = 2;
		curY = 10;

		/*Control::run = true;
		Control::code =
			"var res = \"\"; \n"\
			"for(var i = 0; i < 3; i++){\n"\
			"    res += i; \n"\
			"}\n"\
			"say(res);"\
			"moveLeft();"\
			"moveUp();"\
			"moveUp();"\
			"moveUp();"\
			"moveUp();"\
			"moveUp();"\
			"moveUp();"\
			"moveUp();"\
			"moveUp();";*/


		while (window.isOpen()) {
			sf::Event event;
			while (window.pollEvent(event)) {
				if (event.type == sf::Event::Closed)
					window.close();
			}
			if (Control::run) {
				runInterpreter();
				ip = 0;
				Control::run = false;
			}
			//for (auto c : input) {
			renderLevel(*cr);
			mainSprite.setTexture(renderTex.getTexture());
			window.clear();
			window.draw(mainSprite);	
			window.draw(textToSay);
			window.display();
				//move(c);
				//std::this_thread::sleep_for(std::chrono::milliseconds(16));
			//}

		}

	}
};

#endif