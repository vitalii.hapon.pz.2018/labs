#ifndef CONTROL
#define CONTROL

#include <atomic>
#include <string>

#include <SFML/Graphics.hpp>

namespace{
	namespace Control {
		 std::atomic<sf::RenderWindow*> window = nullptr;
		 std::atomic_bool run = false;
		 std::string code = "";
	}
}



#endif 