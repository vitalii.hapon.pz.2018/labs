#ifndef OPERATORS_LIST_H
#define OPERATORS_LIST_H

#include <HPL/utils/String.h>
#include <HPL/Value.h>
#include <functional>
#include <vector>
#include <map>
#include <list>

namespace hpl {
	class OperatorsList {
	public:
		using String = hpl::utils::String;
		using Value = hpl::Value;
		enum OperatorType {
			BINARY_LEFT_RIGHT,
			BINARY_RIGHT_LEFT,
			UNARY_POSTFIX,
			UNARY_PREFIX
		};

		struct PriorityOperators {
			OperatorType type;
			std::map<String, std::function < Value(Value, Value)>> operators;

			bool hasOperator(String op) {
				return operators.find(op) != operators.end();
			}

			std::function < Value(Value)> getUnary(String op) {
				using namespace std::placeholders;

				return std::bind(operators[op], _1, Value());
			}
			std::function < Value(Value, Value)> getBinary(String op) {
				return operators[op];
			}
		};
	private:

		std::vector<PriorityOperators> priorities;
		void fitToPriority(size_t priority) {
			if (priorities.size() <= priority) {
				priorities.resize(priority+1);
			}
		}
	public:
		// priority >= 1
		int getPrioritiesCount() const {
			return priorities.size();
		}
		PriorityOperators& getOperators(size_t priority) {
			return priorities[priority];
		}
		void addBinaryRLOperators(size_t priority, std::map<String, std::function < Value(Value, Value)>> operators) {
			fitToPriority(priority);
			PriorityOperators tmp;
			tmp.type = BINARY_RIGHT_LEFT;
			tmp.operators = operators;
			for (auto &i : operators) {
				tmp.operators[i.first] = i.second;
			}
			priorities[priority] = tmp;
		}
		void addBinaryLROperators(size_t priority, std::map<String, std::function < Value(Value, Value)>> operators) {
			fitToPriority(priority);
			PriorityOperators tmp;
			tmp.type = BINARY_LEFT_RIGHT;
			tmp.operators = operators;
			priorities[priority] = tmp;
		}
		void addPostfixUnaryOperator(size_t priority, std::map<String, std::function < Value(Value)>> operators) {
			using namespace std::placeholders;
			fitToPriority(priority);
			PriorityOperators tmp;
			tmp.type = UNARY_POSTFIX;
			for (auto &i : operators) {
				tmp.operators[i.first] = std::bind(i.second, _1);
			}
			priorities[priority] = tmp;
		}
		void addPrefixUnaryOperator(size_t priority, std::map<String, std::function < Value(Value)>> operators) {
			using namespace std::placeholders;
			fitToPriority(priority);
			PriorityOperators tmp;
			tmp.type = UNARY_PREFIX;
			for (auto &i : operators) {
				tmp.operators[i.first] = std::bind(i.second, _1);
			}
			priorities[priority] = tmp;
		}
	};
}

#endif