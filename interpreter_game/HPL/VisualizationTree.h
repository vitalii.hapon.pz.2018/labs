#ifndef VISUALIZATION_TREE_H
#define VISUALIZATION_TREE_H

#include <HPL/utils/String.h>
#include <memory>
#include <vector>

namespace hpl {
	class VisualizationTree {
	public:
		struct Node;
		using String = hpl::utils::String;
	
		template<class T>

		using Ref = std::unique_ptr<T>;
		using NodeRef = std::unique_ptr<Node>;

	
		static 	NodeRef alloc() {
			return std::move(std::make_unique<Node>());
		}

		template<class T, class... Args>
		static 	Ref<T> alloc(Args&&... args) {
			return std::make_unique<T>(args...);
		}

		struct Node {
			String name;
			std::vector<NodeRef> nodes;
			String toString(size_t recursionDepth = 0) {
				String res = String(recursionDepth * 4, ' ') + name;
				for (auto &n : nodes) {
					res += "\n";
					res += n->toString(recursionDepth + 1);
				}
				return res;
			}
		};

		String toString() {
			if(root)
				return root->toString();
			return "";
		}
		NodeRef root = nullptr;
	};
}

#endif 