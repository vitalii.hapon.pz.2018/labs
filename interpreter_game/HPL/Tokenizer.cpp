#include <HPL/Lexer/Lexer.h>
#include <HPL/Lexer/Group.h>
#include <HPL/Lexer/Sequence.h>
#include <HPL/Lexer/StringSet.h>
#include <HPL/Lexer/RuleSet.h>
#include <HPL/Lexer/CharSet.h>
#include <HPL/Lexer/InvertedRule.h>
#include <HPL/lexer/Tokenizer.h>
#include <HPL/InterpretationException.h>

#include <list>

using namespace hpl::lexer;
using namespace hpl::utils;


const std::list<String> Tokenizer::operators =
{ "++", "--", "!=" , "==", "<=", ">=","+=","-=","*=","/=", "%=", "<", ">", "+", "-", "/", "*", "%" ,"=" , "&&", "||"};
//--------------------------------------------------------------------------------
void Tokenizer::addTokenRule(const Sequence& rule, TokenType type) {
	token_rules.push_back(std::pair<Sequence, TokenType>(rule ,type ));
}
//--------------------------------------------------------------------------------
std::vector<Tokenizer::Token> Tokenizer::tokenize(String text) const {
	std::vector<Token> res;
	auto it = text.begin();
	auto end = text.end();
	auto lineIt = text.begin();

	size_t line = 1;

	bool test;

	do {
		if (it != end)
			test = false;
		else {
			test = true;
			break;
		}
		auto prev = it;

		// check each rule until the matching rule is found:
		for (auto &rule : token_rules) {
			if (rule.first.match(it, end)) {
				if (rule.second == NONE) {
					test = true;
					break;
				}
				Token token;
				token.type = rule.second;
				token.value = String(prev, it);
				token.pos = prev - text.begin();
				while (lineIt != prev) {
					if (*lineIt++ == '\n')
						line++;
				}
				token.line = line;
				res.push_back(token);
				test = true;
				prev = it;
				break;
			}
		}
	} while (test);

	if (it != end) {
		size_t line = 1;
		if (!res.empty()) 
			line = (res.end() - 1)->line;
		throw InterpretationException(InterpretationException::LEXIC, "Unexpected token(s)", line);
	}
	return res;
}
//--------------------------------------------------------------------------------
Tokenizer::Tokenizer() {
	auto max = Group::Quantifier::max;

	addTokenRule({
		Group({1}, StringSet({"//"})),
		Group({0, max}, CharSet(CharSet::ANY) / std::set<Char>{ {'\n'}}),
		});

	addTokenRule({
		Group({1}, StringSet({"/*"})),
		Group({0, max}, Sequence(
			InvertedRule(StringSet({"*/"})),
			CharSet(CharSet::ANY)
		)),
		Group({1}, StringSet({"*/"})),

		});

	addTokenRule({

		Group({1}, CharSet('('))

		}, LPAREN);

	addTokenRule({

		Group({1}, CharSet(')'))

		}, RPAREN);

	addTokenRule({

		Group({1}, CharSet('{'))

		}, LBRACE);

	addTokenRule({

		Group({1}, CharSet('}'))

		}, RBRACE);

	addTokenRule({

		Group({1}, CharSet(','))

		}, COMMA);

	addTokenRule({

		Group({1}, CharSet(';'))

		}, SEMICOLON);

	for (auto &op : operators) {

		addTokenRule({
		Group({1}, StringSet({op}))
			}, OPERATOR);
	}

	addTokenRule({
		Group({1}, StringSet({"if", "for", "else", "string", "break", "continue", "var", "return", "function"}))
		}, KEYWORD);

	addTokenRule({
		Group({1}, CharSet(CharSet::ALPHA) + CharSet('_')),
		Group({0, max}, CharSet(CharSet::ALPHA) + CharSet(CharSet::DIGIT) + CharSet('_')),
		}, IDENTIFIER);

	addTokenRule({

		Group({1}, StringSet({"true", "false"}))

		}, BOOL);

	addTokenRule({

		Group({1, max}, CharSet(CharSet::DIGIT)),
		Group({ 1 }, RuleSet(
				Sequence{
					Group({1}, CharSet({'.'})),
					Group({1, max}, CharSet(CharSet::DIGIT))
				}
			))

		}, FLOAT);

	addTokenRule({

		Group({1, max}, CharSet(CharSet::DIGIT))

		}, INT);



	

	addTokenRule({
	Group({1}, StringSet({"\""})),
	Group({0, max}, RuleSet(
		Group({1},StringSet({"\\\""})),
		CharSet(CharSet::ANY) / std::set<Char>{ {'\"'}}
	)),
	Group({1}, StringSet({"\""})),

		}, STRING);



	addTokenRule({
		Group({1, max}, CharSet(CharSet::SPACE)),
		});

}
