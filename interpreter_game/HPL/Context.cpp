#include <HPL/parser/IdentifierNode.h>
#include <HPL/OperatorsList.h>
#include <HPL/Value.h>
#include <HPL/Context.h>
#include <HPL/utils/String.h>
#include <HPL/InterpretationException.h>

using namespace hpl::parser;
using namespace hpl::utils;
using namespace hpl;

//--------------------------------------------------------------------------------
Value Context::initVariable(IdentifierNodeRef &identifierNode, Value val) {
	auto &top = stackTop();
	auto name = identifierNode->getIdentifierName();
	if (top.find(name) != top.end())
		throw InterpretationException(InterpretationException::LOGICAL,
			"Redefinition of existing varialbe \"" + name + "\"",
			identifierNode->getLine());
	top[name] = val;
	return val;
}
//--------------------------------------------------------------------------------
String Context::toString() const {
	String res;
	int i = 1;
	for (auto &frame : stack) {
		res += "\n Frame" + std::to_string(i++) + ": \n";
		for (auto &var : frame) {
			auto name = var.first;
			auto v = var.second;
			res += "\t" + name + ": " + v.toString();
			res += "\n";
		}
	}
	return res;
}
//--------------------------------------------------------------------------------
void Context::addFunction(
	IdentifierNodeRef &identifierNode,
	std::function<void(Context &context)> function, 
	std::vector<String> &args
) {
	auto name = identifierNode->getIdentifierName();
	if (functions.find(name) != functions.end())
		throw InterpretationException(InterpretationException::LOGICAL,
			"Redefinition of existing function \"" + name + "\"", identifierNode->getLine());
	Function tmp;
	tmp.argsNames = args;
	tmp.function = function;
	functions[name] = tmp;
}
//--------------------------------------------------------------------------------
void Context::push(const Memory &memory) {
	stack.push_back(memory);
}
//--------------------------------------------------------------------------------
void Context::pop() {
	stack.resize(stack.size() - 1);
}
//--------------------------------------------------------------------------------
Context::Context(){
	stack.push_back(Memory());
}
//--------------------------------------------------------------------------------
Value Context::callFunction(IdentifierNodeRef &identifierNode, std::vector<Value> &args) {
	auto name = identifierNode->getIdentifierName();

	auto funcIt =  functions.find(name);
	if(funcIt == functions.end())
		throw InterpretationException(InterpretationException::LOGICAL, 
			"Undefined function \"" + name + "\"", identifierNode->getLine());

	auto &argsNames = funcIt->second.argsNames;
	Memory tmp;

	if (args.size() != argsNames.size())
		throw InterpretationException(InterpretationException::LOGICAL,
			"Incorrect arguments number passed to function \"" + name + "\"", identifierNode->getLine());

	for (size_t i = 0; i < args.size(); i++) {
		tmp[argsNames[i]] = args[i];
	}
	push(tmp);
	funcIt->second.function(*this);
	pop();

	auto res = returnValue;
	returnValue = Value();
	return res;
}
//--------------------------------------------------------------------------------
Value Context::getVariable(String name) {
	for (auto it = stack.rbegin(); it != stack.rend(); it++) {
		if (it->find(name) != it->end())
			return (*it)[name];
	}
	return Value();
}
//--------------------------------------------------------------------------------
Context::Memory& Context::stackTop() {
	if (stack.begin() == stack.end())
		throw InterpretationException(InterpretationException::CONVERSION, "Stack is empty");
	return *(stack.end() - 1);
}
//--------------------------------------------------------------------------------
void Context::loopBreak() {
	flowChanged = true;
	jumpFlag = BREAK;
}
//--------------------------------------------------------------------------------
void Context::loopContinue() {
	flowChanged = true;
	jumpFlag = CONTINUE;
}
//--------------------------------------------------------------------------------
void Context::functionReturn(Value value) {
	returnValue = value;
}
//--------------------------------------------------------------------------------
Context::JumpFlag Context::getJumpFlag() {
	return jumpFlag;
}
//--------------------------------------------------------------------------------
bool Context::isFlowChanged() const {
	return flowChanged;
}
//--------------------------------------------------------------------------------
void Context::restoreFlow() {
	jumpFlag = NONE;
	flowChanged = false;
}
//--------------------------------------------------------------------------------
Value Context::getReturnValue() {
	auto tmp = returnValue;
	returnValue = Value();
	return tmp;
}
//--------------------------------------------------------------------------------
void Context::addUserFunction(String name, std::vector<String> args, std::function<Value(Context &context)> function) {
	Function fn;
	fn.argsNames = args;
	fn.function = [this, function](Context &  c) {
		returnValue = function(c);
	};
	functions[name] = fn;
}
//--------------------------------------------------------------------------------