#ifndef EXPRESSION_NODE_H
#define EXPRESSION_NODE_H

#include <HPL/parser/AbstractNode.h>
#include <HPL/parser/AbstractOperatorNode.h>
#include <HPL/OperatorsList.h>
#include <vector>

class hpl::parser::ExpressionNode : public AbstractNode {
private:
	NodeRef node;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();
		
		auto priorities = AbstractOperatorNode::operators.getPrioritiesCount() - 1;
		if (auto abstractOperatorNode = AbstractOperatorNode::create(priorities, stream)) {
			Ref<ExpressionNode> expr = alloc< ExpressionNode>();
			expr->node = std::move(abstractOperatorNode);
			return expr;
		}
		
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		return node->solve(context);
	}
	bool tryOptimize(Value & res) override {
		Value ores;
		if (node->tryOptimize(ores)) {
			node = AbstractTerminalNode::create(ores);
		}
		return false;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Expression: ";
		res->nodes.push_back(node->createVisualizationTreeNode());
		return res;
	}
	virtual int complexity() {
		return node->complexity();
	}
};
#endif