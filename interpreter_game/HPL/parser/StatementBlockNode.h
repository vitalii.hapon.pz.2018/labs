#ifndef STATEMENT_BLOCK_NODE_H
#define STATEMENT_BLOCK_NODE_H

#include <HPL/parser/AbstractStatementNode.h>
#include <vector>

class hpl::parser::StatementBlockNode : public AbstractStatementNode {
private:
	std::vector<NodeRef> statements;
public:
	static Ref<StatementBlockNode> create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.checkAndAdvance(TokenType::LBRACE)) {
			
			Ref<StatementBlockNode> statementBlock = alloc<StatementBlockNode>();
			while (stream.available()) {
				if (auto statement = AbstractStatementNode::create(stream))
					statementBlock->statements.push_back(std::move(statement));
				else {
					if (stream.checkAndAdvance(TokenType::RBRACE)) 						
						return statementBlock;
					
					break;
				}
			}
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		context.push(Context::Memory());
		for (auto &s : statements) {
			s->solve(context);
			if (context.isFlowChanged())
				break;
		}
		context.pop();
		return Value();
	}
	bool tryOptimize(Value & res) override {
		for (auto &s : statements) {
			Value ores;
			s->tryOptimize(ores);
	
		}
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "StatementBlock: ";
		for (auto &s : statements) {
			res->nodes.push_back(s->createVisualizationTreeNode());
		}
		return res;
	}
	virtual int complexity() {
		int result = 0;
		for (auto &c : statements) {
			result += c->complexity();
		}
		return result;
	}
};
#endif