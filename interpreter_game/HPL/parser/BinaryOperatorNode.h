#ifndef BINARY_OPERATOR_NODE_H
#define BINARY_OPERATOR_NODE_H

#include <HPL/parser/AbstractOperatorNode.h>
#include <vector>

class hpl::parser::BinaryOperatorNode : public AbstractOperatorNode {
private:
	using Operand = std::pair<NodeRef, std::function<Value(Value, Value)>>;

	NodeRef firstOperand;
	std::vector<Operand> operands;
	std::vector<String> opStrings;

	enum Associativity {
		LEFT_RIGHT,
		RIGHT_LEFT
	}associativity;
public:
	static NodeRef create(size_t priority, TokenStream &stream) {
		auto saved = stream.getIdentifier();

		auto op = operators.getOperators(priority);

		NodeRef operandNode;
		if (priority == 0)
			operandNode = OperandNode::create(stream);
		else
			operandNode = AbstractOperatorNode::create(priority - 1, stream);

		if (operandNode) {
			Ref<BinaryOperatorNode> binaryOperatorNode = alloc<BinaryOperatorNode>();
			while (stream.available()) {
				String opStr;
				auto operand = (priority == 0) ? createOperand(op, opStr, stream) : createOperand(priority, opStr, stream);
				if (operand.first) {
					binaryOperatorNode->operands.push_back(std::move(operand));
					binaryOperatorNode->opStrings.push_back(opStr);
				}
				else break;
			}
			if (binaryOperatorNode->operands.empty()) {
				return operandNode;
			}
			else {
				binaryOperatorNode->firstOperand = std::move(operandNode);
				binaryOperatorNode->associativity = op.type == OperatorsList::OperatorType::BINARY_LEFT_RIGHT ? LEFT_RIGHT : RIGHT_LEFT;
				return binaryOperatorNode;
			}

		}

		stream.setIdentifier(saved);
		return nullptr;
	}
	static Operand createOperand(size_t priority, String &opString, TokenStream &stream) {
		auto saved = stream.getIdentifier();

		auto op = operators.getOperators(priority);

		if (stream.check(TokenType::OPERATOR) && op.hasOperator(stream.currrentValue())) {
			auto opStr = stream.advance()->value;
			opString = opStr;
			if (auto abstractOperatorNode = AbstractOperatorNode::create(priority - 1, stream))
				return { std::move(abstractOperatorNode), op.getBinary(opStr) };

		}
		stream.setIdentifier(saved);
		return { nullptr, nullptr };
	}
	static Operand createOperand(Operators &op, String &opString, TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::OPERATOR) && op.hasOperator(stream.currrentValue())) {
			auto opStr = stream.advance()->value;
			opString = opStr;
			if (auto operandNode = OperandNode::create(stream))
				return { std::move(operandNode), op.getBinary(opStr) };

		}
		stream.setIdentifier(saved);
		return { nullptr, nullptr };
	}
	Value solve(Context &context) override {
		if (associativity == RIGHT_LEFT) {
			auto last = (operands.end() - 1);
			Value tmp = last->first->solve(context);
			auto func = last->second;
			if (last != operands.begin()) {
				for (auto it = last - 1; it != operands.begin(); it--) {
					tmp = func(it->first->solve(context), tmp);
					func = it->second;
				}
			}
			return func(firstOperand->solve(context), tmp);
		}
		else {
			Value tmp = firstOperand->solve(context);
			for (auto &ops : operands) {
				tmp = ops.second(tmp, ops.first->solve(context));
			}
			return tmp;
		}
	}
	bool tryOptimize(Value & res) override {
		std::vector<std::pair<NodeRef, std::function<Value(Value, Value)>>> notOptimized;
		if (associativity == RIGHT_LEFT) {
			return false;
		}
		else {
			Value ores;
			if (firstOperand->tryOptimize(ores)) {
				for (auto it = operands.begin(); it != operands.end();) {
					Value sres;
					if (it->first->tryOptimize(sres)) {
						Value tmp = it->second(ores, sres);
						ores = tmp;
						firstOperand = AbstractTerminalNode::create(tmp);
						it = operands.erase(it);
					}
					else {
						break;
					}
				}
			}

			for (auto it = operands.begin(); it != operands.end();) {
				Value ores;
				Value sres;
				if (it->first->tryOptimize(ores)) {

					if ((it + 1) != operands.end() && (it + 1)->first->tryOptimize(sres)) {
						Value tmp = (it + 1)->second(ores, sres);
						(it + 1)->first = AbstractTerminalNode::create(tmp);
						(it + 1)->second = it->second;
						it = operands.erase(it);
					}
					else {
						it->first = AbstractTerminalNode::create(ores);
						it++;
					}
				}
				else
					it++;
			}
			if (operands.empty()) {
				Value ores;
				if (firstOperand->tryOptimize(ores)) {
					res = ores;
					return true;
				}
			}
		}
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "BinaryOperator: ";
		res->nodes.push_back(std::move(firstOperand->createVisualizationTreeNode()));
		for (size_t i = 0; i < operands.size(); i++) {
			auto tmp = VisualizationTree::alloc();
			tmp->name = opStrings[i];
			tmp->nodes.push_back(std::move(operands[i].first->createVisualizationTreeNode()));
			res->nodes.push_back(std::move(tmp));
		}
		return res;
	}
	virtual int complexity() {
		int result = 0;
		for (auto &c : operands) {
			result += c.first->complexity();
		}
		return result;
	}

};
#endif