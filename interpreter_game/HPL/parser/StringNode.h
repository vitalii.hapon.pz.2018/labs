#ifndef STRING_NODE_H
#define STRING_NODE_H

#include <HPL/parser/AbstractTerminalNode.h>

class hpl::parser::StringNode : public AbstractTerminalNode {
private:
	String string;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::STRING)) {
			Ref<StringNode> stringeNode = alloc<StringNode>();
			auto value = stream.currrentValue();

			auto unescape = [](const String& s){
				String res;
				String::const_iterator it = s.begin();
				while (it != s.end()){
					char c = *it++;
					if (c == '\\' && it != s.end()){
						switch (*it++) {
						case '\\': c = '\\'; break;
						case 'n': c = '\n'; break;
						case 't': c = '\t'; break;
						case '\"': c = '\"'; break;
						default:
							continue;
						}
					}
					res += c;
				}
				return res;
			};
			stringeNode->string = unescape(value.substr(1 , value.size()-2));
			stringeNode->line = stream.currrentLine();
			stream.advance();
			return stringeNode;
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	static NodeRef create(Value &val) {
		Ref<StringNode> stringNode = alloc<StringNode>();
		stringNode->string = val.castToString();
		return stringNode;
	}
	Value solve(Context &context) override {
		return Value(string);
	}

	bool tryOptimize(Value & res) override {
		res = Value(string);
		return true;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "String(\"" + string + "\")";
		return res;
	}
	virtual int complexity() {
		return 0;
	}

};
#endif