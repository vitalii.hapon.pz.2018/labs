#include <HPL/parser/AbstractStatementNode.h>
#include <HPL/parser/InitStatementNode.h>
#include <HPL/parser/IfStatementNode.h>
#include <HPL/parser/ForStatementNode.h>
#include <HPL/parser/JumpStatementNode.h>
#include <HPL/parser/StatementBlockNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/parser/AbstractTerminalNode.h>
#include <HPL/parser/IdentifierNode.h>
#include <HPL/parser/FloatNode.h>
#include <HPL/parser/IntNode.h>
#include <HPL/parser/BoolNode.h>
#include <HPL/parser/StringNode.h>
#include <HPL/parser/AbstractOperatorNode.h>
#include <HPL/parser/UnaryOperatorNode.h>
#include <HPL/parser/BinaryOperatorNode.h>
#include <HPL/parser/ReturnStatementNode.h>
#include <HPL/OperatorsList.h>
#include <HPL/Value.h>
#include <HPL/lexer/Tokenizer.h>
#include <HPL/utils/String.h>

using namespace hpl::parser;
using namespace hpl;

//--------------------------------------------------------------------------------
hpl::OperatorsList AbstractOperatorNode::operators = []() {
	OperatorsList operators;

	operators.addPostfixUnaryOperator(
		0, { 
			{"++", [](Value op) {
				Value tmp = op.copy();
				op.increment();
				return tmp;
			}},
			{"--", [](Value op) {
				Value tmp = op.copy();
				op.decrement();
				return tmp;
			}}
		}
	);

	operators.addPrefixUnaryOperator(
		1, { 
			{"!", [](Value op) {
				return op.logicalNot();
			}},
			{"+", [](Value op) {
				return op.promote();
			}},
			{"++", [](Value op) {
				return op.increment();
			}},
			{"-", [](Value op) {
				return op.negate();
			}},
			{"--", [](Value op) {
				return op.decrement();
			}}
		}
	);

	operators.addBinaryLROperators(
		2, {
			{"*", [](Value op_1, Value op_2) {
				return op_1.mul(op_2);
			}},
			{"/", [](Value op_1, Value op_2) {
				return op_1.div(op_2);
			}},
			{"%", [](Value op_1, Value op_2) {
				return op_1.mod(op_2);
			}}

		}
	);

	operators.addBinaryLROperators(
		3, { 
			{"+", [](Value op_1, Value op_2) {
				return op_1.add(op_2);
			}},
			{"-", [](Value op_1, Value op_2) {
				return op_1.sub(op_2);
			}}
		}
	);

	operators.addBinaryLROperators(
		4, {
			{"<", [](Value op_1, Value op_2) {
				return op_1.isLess(op_2);
			}},
			{"<=", [](Value op_1, Value op_2) {
				return op_1.isLessOrEquel(op_2);
			}},
			{">", [](Value op_1, Value op_2) {
				return op_1.isLessOrEquel(op_2).logicalNot();
			}},
			{">=", [](Value op_1, Value op_2) {
				return op_1.isLess(op_2).logicalNot();
			}}
		}
	);

	operators.addBinaryLROperators(
		5, { 
			{"==", [](Value op_1, Value op_2) {
				return op_1.isEquel(op_2);
			}} ,
			{"!=", [](Value op_1, Value op_2) {
				return op_1.isEquel(op_2).logicalNot();
			}},
		}
	);

	operators.addBinaryLROperators(
		6, { {"&&", [](Value op_1, Value op_2) {
				return op_1.logicalAnd(op_2);
			}
		} }
	);

	operators.addBinaryLROperators(
		7, { {"||", [](Value op_1, Value op_2) {
				return op_1.logicalOr(op_2);
			}
		} }
	);

	operators.addBinaryRLOperators(
		8, {
			{"=", [](Value op_1, Value op_2) {
				op_1.set(op_2);
				return op_1;
			}},
			{"*=", [](Value op_1, Value op_2) {
				op_1.set(op_1.mul(op_2));
				return op_1;
			}},
			{"/=", [](Value op_1, Value op_2) {
				op_1.set(op_1.div(op_2));
				return op_1;
			}},
			{"+=", [](Value op_1, Value op_2) {
				op_1.set(op_1.add(op_2));
				return op_1;
			}},
			{"-=", [](Value op_1, Value op_2) {
				op_1.set(op_1.sub(op_2));
				return op_1;
			}},
			{"%=", [](Value op_1, Value op_2) {
				op_1.set(op_1.mod(op_2));
				return op_1;
			}}
		}
	);
	return operators;
}();
//--------------------------------------------------------------------------------
AbstractOperatorNode::NodeRef 
AbstractOperatorNode::create(size_t priority, TokenStream &stream) {
	auto saved = stream.getIdentifier();

	auto op = operators.getOperators(priority);
	if (op.type == OperatorsList::BINARY_LEFT_RIGHT || op.type == OperatorsList::BINARY_RIGHT_LEFT) 
		if (auto binaryOperatorNode = BinaryOperatorNode::create(priority, stream))
			return binaryOperatorNode;
		
	if (op.type == OperatorsList::UNARY_POSTFIX || op.type == OperatorsList::UNARY_PREFIX) 
		if (auto unaryOperatorNode = UnaryOperatorNode::create(priority, stream))
			return unaryOperatorNode;

	stream.setIdentifier(saved);
	return nullptr;
}
//--------------------------------------------------------------------------------
AbstractTerminalNode::NodeRef
AbstractTerminalNode::create(Value &val) {
	NodeRef tmp;
	switch (val.getType()){
	case Value::BOOL:
		return BoolNode::create(val);
		break;
	case Value::STRING:	
		return StringNode::create(val);
		break;
	case Value::FLOAT:
		return FloatNode::create(val);
		break;
	default:
	case Value::INT:
		return IntNode::create(val);
		break;
	}
	return tmp;
}
//--------------------------------------------------------------------------------
AbstractTerminalNode::NodeRef
AbstractTerminalNode::create(TokenStream &stream) {
	auto saved = stream.getIdentifier();

	if (auto variableNode = IdentifierNode::create(stream))
		return variableNode;

	if (auto floatNode = FloatNode::create(stream))
		return floatNode;

	if (auto intNode = IntNode::create(stream))
		return intNode;

	if (auto stringNode = StringNode::create(stream))
		return stringNode;

	if (auto boolNode = BoolNode::create(stream))
		return boolNode;

	stream.setIdentifier(saved);
	return nullptr;
}
//--------------------------------------------------------------------------------
AbstractStatementNode::NodeRef
AbstractStatementNode::create(TokenStream &stream) {
	auto saved = stream.getIdentifier();

	if (auto initStatement = InitStatementNode::create(stream))
		if (stream.checkAndAdvance(TokenType::SEMICOLON))
			return initStatement;
		

	if (auto ifStatement = IfStatementNode::create(stream))
		return ifStatement;

	if (auto forStatement = ForStatementNode::create(stream))
		return forStatement;

	if (auto jumpStatement = JumpStatementNode::create(stream))
		return jumpStatement;

	if (auto returnStatementNode = ReturnStatementNode::create(stream))
		return returnStatementNode;

	if (auto statementBlock = StatementBlockNode::create(stream))
		return statementBlock;

	if (auto expression = ExpressionNode::create(stream))
		if (stream.checkAndAdvance(TokenType::SEMICOLON)) 
			return expression;
		

	stream.setIdentifier(saved);
	return nullptr;
}
//--------------------------------------------------------------------------------