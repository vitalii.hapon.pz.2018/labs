#ifndef BOOL_NODE_H
#define BOOL_NODE_H

#include <HPL/parser/AbstractTerminalNode.h>
#include <vector>

class hpl::parser::BoolNode : public AbstractTerminalNode {
private:
	bool value;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::BOOL)) {
			Ref<BoolNode> boolNode = alloc<BoolNode>();
			boolNode->value = (stream.currrentValue()[0] == 't');
			boolNode->line = stream.currrentLine();

			stream.advance();

			return boolNode;
		}

		stream.setIdentifier(saved);
		return nullptr;
	}
	static NodeRef create(Value &val) {
		Ref<BoolNode> boolNode = alloc<BoolNode>();
		boolNode->value = val.castToBool();
		return boolNode;
	}
	Value solve(Context &context) override {
		return Value(value);
	}
	bool tryOptimize(Value & res) override {
		res = Value(value);
		return true;
	}


	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Bool(";
		res->name += value ? "true" : "false";
		res->name += ")";
		return res;
	}
	virtual int complexity() {
		return 0;
	}

};
#endif