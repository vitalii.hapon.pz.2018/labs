#ifndef ABSTRACT_TERMINAL_NODE_H
#define ABSTRACT_TERMINAL_NODE_H

#include <HPL/parser/AbstractNode.h>

class hpl::parser::AbstractTerminalNode : public AbstractNode {
private:
	String identifier;
public:
	static NodeRef create(TokenStream &stream);
	static NodeRef create(Value &val);
	Value solve(Context &context) override = 0;


	virtual bool tryOptimize(Value & res) = 0;
	virtual int complexity() = 0;
};
#endif