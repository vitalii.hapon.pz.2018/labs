#ifndef JUMP_STATEMENT_NODE_H
#define JUMP_STATEMENT_NODE_H

#include <HPL/parser/AbstractStatementNode.h>

class hpl::parser::JumpStatementNode : public AbstractStatementNode {
private:
	enum JumpType{BREAK, CONTINUE};
	JumpType type;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::KEYWORD)){
			auto tmpLine = stream.currrentLine();
			if (stream.checkAndAdvance("break")) {
				
				if (stream.checkAndAdvance(TokenType::SEMICOLON)) {
					
					Ref<JumpStatementNode> jumpStatemen = alloc<JumpStatementNode>();
					jumpStatemen->type = BREAK;
					jumpStatemen->line = tmpLine;
					return jumpStatemen;
				}
			}
			if (stream.checkAndAdvance("continue")) {
				
				if (stream.checkAndAdvance(TokenType::SEMICOLON)) {
					
					Ref<JumpStatementNode> jumpStatemen = alloc<JumpStatementNode>();
					jumpStatemen->type = CONTINUE;
					jumpStatemen->line = tmpLine;
					return jumpStatemen;
				}
			}
			
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		if (type == BREAK)
			context.loopBreak();
		else
			context.loopContinue();
		return Value();
	}
	bool tryOptimize(Value & res) override {
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = type == BREAK ? "break": "continue";
		return res;
	}

	virtual int complexity() {
		return 0;
	}
};
#endif