#ifndef PARSER_H
#define PARSER_H

namespace hpl {
	namespace parser {
		class AbstractNode;
		class ProgramNode;
		class AbstractStatementNode;
		class InitStatementNode;
		class IfStatementNode;
		class ForStatementNode;
		class ExpressionNode;
		class JumpStatementNode;
		class IntNode;
		class FloatNode;
		class StringNode;
		class BoolNode;
		class IdentifierNode;
		class OperatorNode;
		class StatementBlockNode;
		class AbstractTerminalNode;
		class AbstractOperatorNode;
		class UnaryOperatorNode;
		class BinaryOperatorNode;
		class FunctionCallNode;
		class OperandNode;
		class FunctionNode;
		class ReturnStatementNode;

	};
}
#endif