#ifndef FUNCTION_CALL_NODE_H
#define FUNCTION_CALL_NODE_H

#include <HPL/parser/AbstractNode.h>
#include <HPL/parser/IdentifierNode.h>
#include <HPL/parser/ExpressionNode.h>

#include <vector>

class hpl::parser::FunctionCallNode : public AbstractNode {
private:
	Ref<IdentifierNode> functionName;
	std::vector<NodeRef> expressions;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		auto tmpLine = stream.currrentLine();
		if (Ref<IdentifierNode> variableNode = IdentifierNode::create(stream)) {
			if (stream.checkAndAdvance(TokenType::LPAREN)) {
				
				Ref<FunctionCallNode> functionCallNode = alloc<FunctionCallNode>();
				if (auto expressionNode = ExpressionNode::create(stream)) {
					functionCallNode->expressions.push_back(std::move(expressionNode));
					while (true) {
						if (stream.check(TokenType::COMMA)) {
							auto saved_2 = stream.advance();
							if (auto expressionNode = ExpressionNode::create(stream)) {
								functionCallNode->expressions.push_back(std::move(expressionNode));
							}
							else {
								stream.setIdentifier(saved_2);
								break;
							}
						}
						else
							break;
					}
				}
				if (stream.checkAndAdvance(TokenType::RPAREN)) {
					
					functionCallNode->functionName = std::move(variableNode);
					functionCallNode->line = tmpLine;
					return functionCallNode;
				}
			}
		}

		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		std::vector<Value> args;
		for (auto &exp : expressions) {
			args.push_back(exp->solve(context));
		}
		return context.callFunction(functionName, args);
	}

	bool tryOptimize(Value & res) override {
		for (auto &exp : expressions) {
			Value ores;
			if (exp->tryOptimize(ores)) {
				exp = AbstractTerminalNode::create(ores);
			}
		}
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "FunctionCall: ";

		auto tmp = VisualizationTree::alloc();
		tmp->name = "NAME";
		tmp->nodes.push_back(functionName->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		for (size_t i = 0; i < expressions.size(); i++) {
			tmp = VisualizationTree::alloc();
			tmp->name = "ARG_" + std::to_string(i + 1);
			tmp->nodes.push_back(expressions[i]->createVisualizationTreeNode());

			res->nodes.push_back(std::move(tmp));
		}

		return res;
	}
	virtual int complexity() {
		return 2;
	}

};
#endif