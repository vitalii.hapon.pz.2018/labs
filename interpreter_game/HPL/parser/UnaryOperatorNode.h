#ifndef UNARY_OPERATOR_NODE_H
#define UNARY_OPERATOR_NODE_H

#include <HPL/parser/AbstractOperatorNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/parser/OperandNode.h>

class hpl::parser::UnaryOperatorNode : public AbstractOperatorNode {
private:
	NodeRef node;
	std::function<Value(Value)> func;
	String opString;
	enum UnaryType{POSTFIX, PREFIX} unaryType;
public:
	static NodeRef create(size_t priority, TokenStream &stream) {
		auto saved = stream.getIdentifier();

		auto op = operators.getOperators(priority);

		if (op.type == OperatorsList::OperatorType::UNARY_POSTFIX) {
			if (priority == 0) {
				if (auto operandNode = OperandNode::create(stream)) {
					if (stream.check( TokenType::OPERATOR )&& op.hasOperator(stream.currrentValue())) {
						Ref<UnaryOperatorNode> unaryOperatorNode = alloc<UnaryOperatorNode>();
						unaryOperatorNode->node = std::move(operandNode);
						unaryOperatorNode->func = op.getUnary(stream.currrentValue());
						unaryOperatorNode->opString = stream.currrentValue();
						unaryOperatorNode->unaryType = POSTFIX;

						stream.advance();
						return unaryOperatorNode;
					}
					else 
						return operandNode;				
				}
			}
			else if (auto abstractOperatorNode = AbstractOperatorNode::create(priority - 1, stream)) {
				if (stream.check(TokenType::OPERATOR) && op.hasOperator(stream.currrentValue())) {
					Ref<UnaryOperatorNode> unaryOperatorNode = alloc<UnaryOperatorNode>();
					unaryOperatorNode->node = std::move(abstractOperatorNode);
					unaryOperatorNode->func = op.getUnary(stream.currrentValue());
					unaryOperatorNode->opString = stream.currrentValue();
					unaryOperatorNode->unaryType = POSTFIX;
					stream.advance();
					return unaryOperatorNode;
				}
				else
					return abstractOperatorNode;
			}
		}
		else if (op.type == OperatorsList::OperatorType::UNARY_PREFIX) {
			if (priority == 0) {
				if (stream.check(TokenType::OPERATOR) && op.hasOperator(stream.currrentValue())) {
					auto opStr = stream.advance()->value;
					if (auto operandNode = OperandNode::create(stream)) {
						Ref<UnaryOperatorNode> unaryOperatorNode = alloc<UnaryOperatorNode>();
						unaryOperatorNode->node = std::move(operandNode);
						unaryOperatorNode->func = op.getUnary(opStr);
						unaryOperatorNode->opString = stream.currrentValue();
						unaryOperatorNode->unaryType = PREFIX;

						return unaryOperatorNode;
					}
					else {
						stream.setIdentifier(saved);
						return nullptr;
					}
				}
				else 
					if (auto operandNode = OperandNode::create(stream))
						return operandNode;
			}
			else {
				if (stream.check(TokenType::OPERATOR) && op.hasOperator(stream.currrentValue())) {
					auto opStr = stream.advance()->value;
					if (auto abstractOperatorNode = AbstractOperatorNode::create(priority - 1, stream)) {
						Ref<UnaryOperatorNode> unaryOperatorNode = alloc<UnaryOperatorNode>();
						unaryOperatorNode->node = std::move(abstractOperatorNode);
						unaryOperatorNode->func = op.getUnary(opStr);
						unaryOperatorNode->opString = stream.currrentValue();
						unaryOperatorNode->unaryType = PREFIX;

						return unaryOperatorNode;
					}
					else {
						stream.setIdentifier(saved);
						return nullptr;
					}
				}
				else
					if (auto abstractOperatorNode = AbstractOperatorNode::create(priority - 1, stream))
						return abstractOperatorNode;
			}
		}
	
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		auto b = node->solve(context);
		return func(b);
	}
	bool tryOptimize(Value & res) override {
		Value ores;
		if (node->tryOptimize(ores)) {
			if (unaryType == POSTFIX) 
				res = ores;
			else 
				res = func(ores);
			
			return true;
		}
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "UnaryOperator: ";

		auto tmp = VisualizationTree::alloc();
		
		tmp->name = (unaryType == POSTFIX) ? "POSTFIX " : "PREFIX ";
		tmp->name += opString;
		tmp->nodes.push_back(node->createVisualizationTreeNode());


		return res;
	}

	virtual int complexity() {
		return node->complexity();
	}

};
#endif