#ifndef FLOAT_NODE_H
#define FLOAT_NODE_H

#include <HPL/parser/AbstractTerminalNode.h>
#include <vector>

class hpl::parser::FloatNode : public AbstractTerminalNode {
private:
	long double value;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::FLOAT)) {
			Ref<FloatNode> floatNode = alloc<FloatNode>();
			floatNode->value = std::stold(stream.currrentValue());
			floatNode->line = stream.currrentLine();
			stream.advance();
			return floatNode;
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	static NodeRef create(Value &val) {
		Ref<FloatNode> floatNode = alloc<FloatNode>();
		floatNode->value = val.castToFloat();
		return floatNode;
	}
	Value solve(Context &context) override {
		return Value(value);
	}
	bool tryOptimize(Value & res) override {
		res = Value(value);
		return true;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Float(" + std::to_string(value) + ")";
		return res;
	}
	virtual int complexity() {
		return 0;
	}

};
#endif