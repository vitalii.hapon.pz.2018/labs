#ifndef STRING_H
#define STRING_H

#include <string>

namespace hpl {
	namespace utils {
		using String = std::string;
		using Char = unsigned char;
		namespace {
			static Char charMaxValue = ~String::value_type();
		}
	}
}

#endif