#ifndef  INTERPRETAION_EXCEPTION_H
#define  INTERPRETAION_EXCEPTION_H

#include <HPL/utils/String.h>

namespace hpl {
	class InterpretationException {
	public:
		enum Type {
			CONVERSION, 
			SYNTAX,
			LEXIC,
			LOGICAL
		};
		using String = hpl::utils::String;
	private:
		String _msg;
		Type _type;
		size_t _line;
	public:
		InterpretationException(Type type, String msg, size_t line = 0):
			_msg(msg),
			_type(type),
			_line(line){}

		String what() {
			String res;
			switch (_type){
			case hpl::InterpretationException::CONVERSION:
				res = String("Conversion Error");
				break;
			case hpl::InterpretationException::SYNTAX:
				res = String("Syntax Error");
				break;
			case hpl::InterpretationException::LEXIC:
				res = String("Lexic Error");
				break;
			case hpl::InterpretationException::LOGICAL:
				res = String("Logical Error");
				break;
			default:
				break;
			}
			if(_line)
				res += " at line " + std::to_string(_line) + ": " + _msg;
			else
				res += " : " + _msg;
			return res;
		}
	};
}
#endif 
