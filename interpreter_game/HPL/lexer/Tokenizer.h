#ifndef TOKENIZER_H
#define TOKENIZER_H


#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Sequence.h>
#include <HPL/utils/String.h>


#include <vector>
#include <typeinfo>
#include <string>
#include <list>
#include <utility>


class hpl::lexer::Tokenizer {
public:
	using Char = hpl::utils::Char;
	using String = hpl::utils::String;
	using Sequence = hpl::lexer::Sequence;

	using Token = hpl::lexer::Token;
	using TokenType = hpl::lexer::TokenType;

	using Tokens = hpl::lexer::Tokens;
	using TokenIterator = hpl::lexer::TokenIterator;

	static const std::list<String> operators;

protected:
	std::vector<std::pair<Sequence, TokenType>> token_rules;
	void addTokenRule(const  Sequence& rule, TokenType type = NONE);
public:
	Tokenizer();
	Tokens tokenize(String text) const;
};

#endif