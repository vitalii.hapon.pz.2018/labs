#ifndef STRING_SET_H
#define STRING_SET_H

#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Rule.h>
#include <HPL/utils/String.h>

#include <set>
#include <initializer_list>

class hpl::lexer::StringSet: public Rule{
private:
	std::set<String> strings;
public:
	StringSet() = default;
	StringSet(const StringSet&) = default;
	StringSet(StringSet&&) = default;
	StringSet& operator=(const StringSet&) = default;

	Rule* clone() const override;

	StringSet(std::initializer_list<String> list);

	StringSet operator+(const StringSet& v) const;
	StringSet& operator+=(const StringSet& v);

	String toString() const override;

	bool match(StringConstIterator &beg, const StringConstIterator &end) const override;
};

#endif

