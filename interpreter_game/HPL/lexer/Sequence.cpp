#include <HPL/lexer/Sequence.h>

using namespace hpl::lexer;
using namespace hpl::utils;

//--------------------------------------------------------------------------------
Sequence::Sequence(const Sequence& other):
	sequences(other.cloneSequences()){}
//--------------------------------------------------------------------------------
Sequence& Sequence::operator=(const Sequence& other) {
	sequences = { other.cloneSequences() };
	return *this;
}
//--------------------------------------------------------------------------------
Sequence::~Sequence() {
	for (auto i : sequences)
		delete i;
}
//--------------------------------------------------------------------------------
Rule *Sequence::clone() const {
	return new Sequence(*this);
}
//--------------------------------------------------------------------------------
std::vector<Rule*> Sequence::cloneSequences() const {
	std::vector<Rule*> res;
	res.reserve(sequences.size());
	for (auto &set : sequences) {
		res.push_back(set->clone());
	}
	return res;
}
//--------------------------------------------------------------------------------
Sequence Sequence::operator+(const Sequence &v) const {
	auto res = *this;
	auto seq = v.cloneSequences();
	res.sequences.insert(res.sequences.end(), seq.begin(), seq.end());
	return res;
}
//--------------------------------------------------------------------------------
Sequence& Sequence::operator+=(const Sequence &v) {
	auto seq = v.cloneSequences();
	sequences.insert(sequences.end(), seq.begin(), seq.end());
	return *this;
}
Sequence& Sequence::operator+=(const Rule &v) {
	sequences.push_back(v.clone());
	return *this;
}
String Sequence::toString() const{
	String string;
	string += "( ";
	bool  comma = false;
	for (auto v : sequences) {
		if (comma)
			string += ", ";
		else
			comma = true;
		string += v->toString();
	}
	string += " )";
	return string;
}
//--------------------------------------------------------------------------------
Sequence Sequence::operator*(unsigned int v) const {
	auto res = Sequence();
	for (size_t i = 0; i < v; i++) {
		auto seq = cloneSequences();
		res.sequences.insert(res.sequences.end(), seq.begin(), seq.end());
	}
	return res;
}
//--------------------------------------------------------------------------------
bool Sequence::match(StringConstIterator &begin, const StringConstIterator &end) const {
	bool test = true;
	auto tmpBeg = begin;
	for (auto &it: sequences) 
		if (!(test = it->match(tmpBeg, end))) 
			break;

	if (test) begin = tmpBeg;
	return test;
}
//--------------------------------------------------------------------------------