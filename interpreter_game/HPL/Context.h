#ifndef CONTEXT_H
#define CONTEXT_H

#include <HPL/Value.h>

#include <HPL/utils/String.h>
#include <HPL/parser/parser.h>

#include <map>
#include <stack>
#include <vector>
#include <functional>
#include <iostream>

namespace hpl {
	namespace parser {
		class IdentifierNode;
	}
	class Context {
	public:
		using String = hpl::utils::String;
		struct Function {
			std::vector<String> argsNames;
			std::function<void(Context &context)> function;
		};	
		using Memory = std::map<String, Value>;
		using Stack = std::vector<Memory>;
		using Functions = std::map<String, Function>;	
		using IdentifierNodeRef = std::unique_ptr<parser::IdentifierNode>;

		enum JumpFlag { NONE, BREAK, CONTINUE };		
		
	private:
		Stack stack;
		JumpFlag jumpFlag;
		bool flowChanged = false;
		Value returnValue;
		Functions functions;
	public:
		Context();

		void push(const Memory &memory);
		void pop();

		Memory& stackTop();

		Value getVariable(String name);
		Value initVariable(IdentifierNodeRef &identifierNode, Value val);

		void addFunction(IdentifierNodeRef &identifierNode, std::function<void(Context &context)> function, std::vector<String> &args);
		Value callFunction(IdentifierNodeRef &identifierNode, std::vector<Value> &args);

		void addUserFunction(String name, std::vector<String> args, std::function<Value(Context &context)> function);

		JumpFlag getJumpFlag();
		void loopBreak();
		void loopContinue();
		void restoreFlow();
		void functionReturn(Value value);	
		bool isFlowChanged() const;
		
		Value getReturnValue();

		String toString() const;

	};
}
#endif