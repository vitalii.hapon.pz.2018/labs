#ifndef APPLICATION_H
#define APPLICATION_H


#include <memory>
#include <SFML/Graphics.hpp>

#include "Game.h"

class Application{
private:
	std::unique_ptr<Game> _game;
	sf::RenderWindow _window;
	bool _isFullscreen;
public:
	Application(bool isFullscreen = true);
	void run();
	sf::RenderWindow & getWindow();
};

#endif

