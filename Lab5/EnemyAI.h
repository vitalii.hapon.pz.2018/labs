#ifndef ENEMY_AI_H
#define ENEMY_AI_H

#include "Command.h"
#include <memory>

class Level;
class Enemy;

class EnemyAI {
public:
	virtual CommandPtr calculate(Level &player, Enemy &enemy) = 0;

};

class FindPlayerAI: public EnemyAI {
public:
	CommandPtr calculate(Level &level, Enemy &enemy);
};

class IdleAI : public EnemyAI {
public:
	CommandPtr calculate(Level &level, Enemy &enemy);
};

class AttackAI : public EnemyAI {
public:
	CommandPtr calculate(Level &level, Enemy &enemy);
};

class PassTheObstacleAI : public EnemyAI {
public:
	CommandPtr calculate(Level &level, Enemy &enemy);
};

#endif