#include "EventManager.h"

void EventManager::subscribe(IEventListener &listener) {
	_listeners.push_back(&listener);
}

void EventManager::unsubscribe(IEventListener &listener) {
	std::remove(_listeners.begin(), _listeners.end(), &listener);
}

void EventManager::notify(const sf::Event &event) {
	for (auto listener : _listeners) {
		listener->sendEvent(event);
	}
}