#ifndef LEVEL_GENERATOR_H
#define LEVEL_GENERATOR_H

#include "Room.h"
#include "Enemy.h"
#include "Level.h"
#include "LevelObjectsFactory.h"
#include <memory>

class LevelGenerator {
public:
	virtual void addEnemy(LevelObjectsFactory &) = 0;
	virtual void addChest(LevelObjectsFactory &) = 0;
	virtual void addRoom(int size) = 0;

	virtual std::unique_ptr<Level> getLevel() = 0;
	virtual void reset() = 0;
};

class NormalLevelGenerator {
public:
	void addEnemy(LevelObjectsFactory &);
	void addChest(LevelObjectsFactory &);
	void addRoom(int size);

	std::unique_ptr<Level> getLevel();
	void reset();
};


class LargeLevelGenerator {
public:
	virtual void addEnemy(LevelObjectsFactory &);
	virtual void addChest(LevelObjectsFactory &);
	virtual void addRoom(int size);

	virtual std::unique_ptr<Level> getLevel();
	void reset();
};


#endif