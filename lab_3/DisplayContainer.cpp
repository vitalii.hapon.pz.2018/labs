#include "DisplayContainer.h"


void DisplayContainer::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	if (_active) {
		for (auto &ptr : _objects) {
			target.draw(*ptr, states);
		}
	}
}

void DisplayContainer::addObject(DisplayObject & object, Layout layout, float spacing ) {
	
	if (layout == VERTICAL) {
		float tmpOffest = 0.f;

		if (!_objects.empty()) {
			auto & tmp = (*(_objects.end() - 1));
			tmpOffest = object.getY() + object.getHeight();
		}
		
		object.setY(object.getY() + tmpOffest + spacing);
	}
	else if (layout == HORIZONTAL) {
		float tmpOffest = 0.f;

		if (!_objects.empty()) {
			auto & tmp = (*(_objects.end() - 1));
			tmpOffest = tmp->getX() + tmp->getWidth();
		}

		object.setX(object.getX() + tmpOffest + spacing);
	}
	_objects.push_back(&object);
}

void DisplayContainer::updateGlobalPosition(float x, float y) {
	for (auto &ptr : _objects) {
		ptr->updateGlobalPosition(_x + x, _y + y);
	}
}

void DisplayContainer::setActive(bool value) {
	_active = value;
	for (auto &ptr : _objects) {
		ptr->setActive(value);
	}
}


float DisplayContainer::getWidth() const {
	float minX;
	float maxXW;

	if (!_objects.empty()) {
		minX = (*_objects.begin())->getX();
		maxXW = (*_objects.begin())->getWidth() + minX;
		for (auto &ptr : _objects) {
			auto tmpX = ptr->getX();
			auto tmpXW = tmpX + ptr->getWidth();

			if (tmpX < minX)
				minX = tmpX;

			if (tmpXW > maxXW)
				maxXW = tmpXW;
		}
		return maxXW - minX;
	}
	return 0.f;
}

float DisplayContainer::getHeight() const {
	float minY;
	float maxYH;

	if (!_objects.empty()) {
		minY = (*_objects.begin())->getY();
		maxYH = (*_objects.begin())->getHeight() + minY;
		for (auto &ptr : _objects) {
			auto tmpY = ptr->getY();
			auto tmpYH = tmpY + ptr->getHeight();

			if (tmpY < minY)
				minY = tmpY;

			if (tmpYH > maxYH)
				maxYH = tmpYH;
		}
		return maxYH - minY;
	}
	return 0.f;
}


