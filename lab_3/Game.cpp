#include "Game.h"

#include "Player.h"
#include "Level.h"
#include "Application.h"
#include "RenderEngine.h"

#include "EventManager.h"
#include "DisplayContainer.h"
#include "DisplayObjects.h"
#include "Hud.h"

#include <fstream>

Room Game::_createTestRoom() {

	auto rn = [](int min, int max) {
		return std::rand() % (max - min + 1) + min;
	};
	Room room3 = Room(rn(0,3) , rn(7, 9), rn(5, 13), rn(5, 18));
	room3 = room3.fill({ Room::FLOOR, "style_a" });
	room3 = room3.makeWallFrame("style_a");

	Room room = Room(rn(0, 6), rn(3, 9), rn(9, 16), rn(9, 13));;
	room = room.fill({ Room::FLOOR, "style_a" });
	room = room.makeWallFrame("style_a");

	Room room2 = Room(rn(1, 7), rn(1, 7), rn(6, 9), rn(3, 6));
	room2 = room2.fill({ Room::FLOOR, "style_a" });
	room2 = room2.makeWallFrame("style_a");

	Room room4 = Room(rn(5, 7), rn(7, 12), rn(9, 13), rn(5, 23));
	room4 = room4.fill({ Room::FLOOR, "style_a" });
	room4 = room4.makeWallFrame("style_b");

	Room room5 = Room(rn(0, 3), rn(1, 13), rn(7, 13), rn(7, 11));
	room5 = room5.fill({ Room::FLOOR, "style_a" });
	room5 = room5.makeWallFrame("style_b");

	Room room6 = Room(0, 0, 6, 6);;
	room6 = room6.fill({ Room::FLOOR, "style_a" });
	room6 = room6.makeWallFrame("style_a");

	room = room.merge(room2, "style_a");
	room = room.merge(room3, "style_a");
	room = room.merge(room4, "style_a");
	room = room.merge(room5, "style_a");
	room = room.merge(room6, "style_a");

	return room;
}

Game::Game(Application & app):
	_level(std::make_unique<Level>(_createTestRoom())),
	_invetory(std::make_unique<InventoryLogger>()),
	_app(app){

}

void Game::run() {
	auto & window = _app.getWindow();

	sf::Font font;
	font.loadFromFile("resources/ebd.ttf");

	Hud hud = Hud(_level->getPlayer(), *_level, *_invetory);

	DisplayContainer menuButtons;
	EventManager eventManager;


	Button btnResume = Button("menu/big_button", "RESUME", font, 36);
	Button btnMainMenu = Button("menu/big_button", "MAIN MENU", font, 36);

	eventManager.subscribe(btnResume);
	eventManager.subscribe(btnMainMenu);

	_invetory->fillWithRandomItems();
	hud.update();


	hud.update((window.getSize().x - hud.getWidth()) / 1.8, (window.getSize().y - hud.getHeight()) + 4.f);

	sf::RectangleShape menuBackground = sf::RectangleShape(sf::Vector2f(window.getSize().x, window.getSize().y));
	{
		menuBackground.setFillColor(sf::Color(0, 0, 0, 150));
		menuButtons.addObject(btnResume);
		menuButtons.addObject(btnMainMenu, DisplayContainer::VERTICAL, 16);
		menuButtons.setActive(false);

		auto blockHeight = menuButtons.getHeight();
		auto blockWidth = menuButtons.getWidth();

		menuButtons.setX((window.getSize().x - blockWidth) / 2);
		menuButtons.setY((window.getSize().y - blockHeight) / 2);

		menuButtons.updateGlobalPosition(0.f, 0.f);

	}

	enum class LRDirection { NONE, LEFT, RIGHT } lrDir = LRDirection::NONE;
	enum class FBDirection { NONE, BACKWARD, FORWARD } fbDir = FBDirection::NONE;

	auto movePlayer = [&]() {
		auto & player = _level->getPlayer();
		if (lrDir != LRDirection::NONE && fbDir != FBDirection::NONE) {
			if (lrDir == LRDirection::RIGHT && fbDir == FBDirection::BACKWARD) {
				player.setMotionDirection(45.f);
			}
			else if (lrDir == LRDirection::LEFT && fbDir == FBDirection::FORWARD) {
				player.setMotionDirection(225.f);
			}
			else if (lrDir == LRDirection::LEFT && fbDir == FBDirection::BACKWARD) {
				player.setMotionDirection(135.f);
			}
			else if (lrDir == LRDirection::RIGHT && fbDir == FBDirection::FORWARD) {
				player.setMotionDirection(315.f);
			}
		}
		else if (lrDir != LRDirection::NONE) {
			if (lrDir == LRDirection::LEFT) {
				player.setMotionDirection(180.f);
			}
			else {
				player.setMotionDirection(0.f);
			}
		}
		else if (fbDir != FBDirection::NONE) {
			if (fbDir == FBDirection::FORWARD) {
				player.setMotionDirection(270.f);
			}
			else {
				player.setMotionDirection(90.f);
			}
		}
		else {
			player.setMotionDirection(0.f, false);
		}
	};

	auto togglePause = [&]() {
		_pause = !_pause;

		static int savedX = sf::Mouse::getPosition(window).x;
		static int savedY = sf::Mouse::getPosition(window).y;

		lrDir = LRDirection::NONE;
		fbDir = FBDirection::NONE;

		movePlayer();

		menuButtons.setActive(_pause);
		window.setMouseCursorVisible(_pause);
		if (_pause) {
			sf::Mouse::setPosition(sf::Vector2i(savedX, savedY), window);
		}
		else {
			savedX = sf::Mouse::getPosition(window).x;
			savedY = sf::Mouse::getPosition(window).y;
			sf::Mouse::setPosition(sf::Vector2i(200, 200), window);
		}
	};

	btnResume.onClick([&]() {
		togglePause();
	});

	btnMainMenu.onClick([&]() {
		_running = false;
	});

	RenderEngine renderEngine = RenderEngine(window.getSize().x, window.getSize().y, *_level);


	int sx = 0 , sy = 0;
	float rdx = 0.0f, rdy = _level->getPlayer().getDirection();

	togglePause();

	while (window.isOpen() && _running) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();

			eventManager.notify(event);

			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Escape) {
					togglePause();
				}
				if (!_pause) {
					if (event.key.code == sf::Keyboard::W)
						fbDir = FBDirection::FORWARD;
					else if (event.key.code == sf::Keyboard::S)
						fbDir = FBDirection::BACKWARD;
					else if (event.key.code == sf::Keyboard::A)
						lrDir = LRDirection::LEFT;
					else if (event.key.code == sf::Keyboard::D)
						lrDir = LRDirection::RIGHT;

					else if (event.key.code == sf::Keyboard::X)
						renderEngine.toggleWireFrame();
					movePlayer();

					if (event.key.code == sf::Keyboard::Num1)
						_invetory->selectItem(0);
					else if (event.key.code == sf::Keyboard::Num2)
						_invetory->selectItem(1);
					else if (event.key.code == sf::Keyboard::Num3)
						_invetory->selectItem(2);
					else if (event.key.code == sf::Keyboard::Num4)
						_invetory->selectItem(3);
					else if (event.key.code == sf::Keyboard::Num5)
						_invetory->selectItem(4);

					if (event.key.code == sf::Keyboard::K) {
						/*sf::RenderTexture render;
						
						render.create(1920, 1080, sf::ContextSettings(24));
						render.setActive(true);
	
						renderEngine.resize(1920, 1080);
						render.clear();	
						renderEngine.render();
						render.display();

						window.setActive(true);
						renderEngine.resize(window.getSize().x, window.getSize().y);*/
						
						sf::Texture text;
						text.create(window.getSize().x, window.getSize().y);
						text.update(window);
						text.copyToImage().saveToFile("screenshot.png");


					}

					
				}
			}
			else if (event.type == sf::Event::KeyReleased) {
				if (!_pause) {
					if (event.key.code == sf::Keyboard::W) {

						if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
							fbDir = FBDirection::NONE;
					}
					else if (event.key.code == sf::Keyboard::S) {
						if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W))
							fbDir = FBDirection::NONE;
					}
					else if (event.key.code == sf::Keyboard::A) {
						if (!sf::Keyboard::isKeyPressed(sf::Keyboard::D))
							lrDir = LRDirection::NONE;
					}
					else if (event.key.code == sf::Keyboard::D) {
						if (!sf::Keyboard::isKeyPressed(sf::Keyboard::A))
							lrDir = LRDirection::NONE;
					}
					movePlayer();
				}
			}
			
			if (!_pause) {
				sf::Vector2i pos = sf::Mouse::getPosition(window);

				int x = pos.x;
				int y = pos.y;
				sx = x - 200;
				sy = y - 200;

				rdx += static_cast<float>(sy)*0.5;
				rdy += static_cast<float>(sx)*0.5;	

		
				sf::Mouse::setPosition(sf::Vector2i(200, 200), window);

				rdx = _level->getPlayer().setVerticalViewAngle(rdx);
				_level->getPlayer().setDirection(rdy);
				
				_level->calculatePlayerPhysics();
			}
		}
		hud.update();
		window.clear();
		renderEngine.render();

		window.pushGLStates();
		window.draw(hud);
		
		if (_pause) {
			
			window.draw(menuBackground);
			window.draw(menuButtons);
			
		}
		window.popGLStates();
		window.display();
	}
}

GameSnapshot Game::save() {
	SerializedObject tmp;
	tmp.store("_level", *_level);
	return GameSnapshot(tmp);
}

void Game::restore(GameSnapshot& snapshot) {
	SerializedObject tmp;
	auto state = snapshot.getState();
	state.load("_level", tmp);
	_level = std::make_unique<Level>(tmp);
}

std::string GameSnapshot::getName(){
	return _name;
}

SerializedObject GameSnapshot::getState() {
	return _game;
}

std::time_t GameSnapshot::getDate()  {
	return _date;
}

SerializedObject GameSnapshot::serialize() const {
	SerializedObject tmp;
	tmp.storeValue("_date", _date);
	tmp.store("_game", _game);
	
	std::vector<unsigned char> txt;
	txt.insert(txt.begin(), _name.begin(), _name.end());
	tmp.store("_name", txt);
	return tmp;
}

void GameSnapshot::deserialize(const SerializedObject &object) {
	object.loadValue("_date", _date);
	object.load("_game", _game);

	std::vector<unsigned char> txt;
	object.load("_name", txt);
	_name.insert(_name.begin(), txt.begin(), txt.end());
}

GameSnapshot::GameSnapshot(const std::vector<unsigned char> &data) {
	deserialize(SerializedObject(data));
}

GameSnapshot::GameSnapshot(const SerializedObject &object) {
	_date = std::time(nullptr);
	_game = object;
}

GameCaretaker::GameCaretaker(Game &game, unsigned int slotsNumber) :
	_game(game),
	_slotsNumber(slotsNumber) {

}

bool GameCaretaker::saveToSlot(unsigned int slot, std::string name) {
	GameSnapshot snapshot = _game.save();
	snapshot.setName(name);

	if (slot <= _slotsNumber) {
		std::ofstream file("savings/" + std::to_string(slot) + ".sav" , std::ios::out | std::ios::binary);

		if (file.is_open()) {
			auto data = snapshot.serialize().toBinary();
			file.write((const char*)data.data(), data.size());
			return true;
		}		
	}
	return false;
}

void GameSnapshot::setName(std::string name) {
	_name = name;
}

bool GameCaretaker::loadFromSloat(unsigned int slot) {
	if (slot <= _slotsNumber) {
		std::ifstream file("savings/" + std::to_string(slot) + ".sav", std::ios::ate | std::ios::binary);

		if (file.is_open()) {
			std::streamsize size = file.tellg();
			file.seekg(0, std::ios::beg);

			auto data = std::vector<unsigned char>(size);
			file.read((char*)data.data(), size);

			GameSnapshot tmp = GameSnapshot(data);
			_game.restore(tmp);
			
			return true;
		}
	}
	return false;
}


