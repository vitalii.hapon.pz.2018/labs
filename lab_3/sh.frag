//[FRAGMENT SHADER]
#version 430

layout(binding = 0) uniform sampler2D texture;
layout(location = 0) out vec4 color;

uniform float time;

in vec2 texCoords;
in float dist;

const vec3 PLAYER_LIGHT_COLOR =  vec3(0.97, 0.67, 0.0);
const vec3 ENV_LIGHT_COLOR = vec3(177./256.,213./256.,198./256.);
const float AMBIENT_STRENGTH = 0.6;
const vec3 AMBIENT_LIGHT = AMBIENT_STRENGTH * ENV_LIGHT_COLOR;
const vec4 FOG_COLOR = vec4(0.30625,0.375,0.7375,1);

float rand(vec2 co){
    co.x += time/100000.0;
    co.y += (time+0.45360)/100000.0;
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453 );
}

float noiseInFog(vec2 co){
    return  (rand(co) * 0.03 - 0.08);
}

void main(){
    vec4 pixel = texture2D(texture,  texCoords);

    float fogFactor = 1.0 - exp(-0.03 * dist* dist);
    float playerLightFactor = exp(-0.6 * dist* dist* dist* dist)*1.5 ;

    color = pixel;

    color *= vec4(AMBIENT_LIGHT * ENV_LIGHT_COLOR * 0.3 ,1.0);
    color *=  1.0 - fogFactor + 0.5;

    color += 0.4 * FOG_COLOR * fogFactor;
    color += noiseInFog(texCoords) * ( fogFactor);
   
    color += pixel * vec4(PLAYER_LIGHT_COLOR * playerLightFactor, 1.0);

    if(pixel.x == 0.0 &&
       pixel.y == 1.0 &&
       pixel.z == 0.0)
	    color = vec4(1.0, 0.0, 0.0, 1.0);
   
    color.w = pixel.w;

}