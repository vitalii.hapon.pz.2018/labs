#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include <algorithm>
#include <string>

#include <cstdint>

#include "SerializedObject.h"

class Room : public ISerializable {
private:
	uint32_t _x, _y, _w, _h;

public:
	enum BlockType { EMPTY, SPIKES, WALL, FLOOR, CHEST, EXIT, DOOR, DECOR };

	struct Block {
		BlockType _type;
		std::string _style;
	};

	Room(uint32_t x = 0, uint32_t y = 0, uint32_t width = 3, uint32_t height = 3) :
		_x(x),
		_y(y),
		_w(width < 3 ? 3 : width),
		_h(height < 3 ? 3 : height),
		_roomMap(_w, std::vector<Block>(_h, { EMPTY,"" })) {

	}

	Room merge(const Room &other, const std::string &intersection_style) {
		uint32_t x1 = std::min(_x, other._x);
		uint32_t y1 = std::min(_y, other._y);

		uint32_t x2 = std::max(_x + _w, other._x + other._w);
		uint32_t y2 = std::max(_y + _h, other._y + other._h);

		Room tmp = Room(x1, y1, x2 - x1, y2 - y1);

		uint32_t x_offset_this = _x - x1;
		uint32_t y_offset_this = _y - y1;

		uint32_t x_offset_other = other._x - x1;
		uint32_t y_offset_other = other._y - y1;

		for (uint32_t ix = 0; ix < _w; ix++) {
			for (uint32_t iy = 0; iy < _h; iy++) {
				tmp._roomMap[ix + x_offset_this][iy + y_offset_this] = _roomMap[ix][iy];
			}
		}

		for (uint32_t ix = 0; ix < other._w; ix++) {
			for (uint32_t iy = 0; iy < other._h; iy++) {
				auto oldblock = tmp._roomMap[ix + x_offset_other][iy + y_offset_other];
				auto newblock = other._roomMap[ix][iy];

				if (oldblock._type == EMPTY && newblock._type == EMPTY) {
					tmp._roomMap[ix + x_offset_other][iy + y_offset_other] = { EMPTY,"" };
				}
				else if (oldblock._type == WALL && newblock._type == WALL) {
					tmp._roomMap[ix + x_offset_other][iy + y_offset_other] = { WALL ,intersection_style };
				}
				else if (oldblock._type == newblock._type || oldblock._type == EMPTY) {
					tmp._roomMap[ix + x_offset_other][iy + y_offset_other] = { newblock._type ,intersection_style };
				}
				else {
					tmp._roomMap[ix + x_offset_other][iy + y_offset_other] = { FLOOR ,intersection_style };
				}
			}
		}
		return tmp;
	}
	Room makeWallFrame(std::string style) {
		Room tmp = *this;
		for (uint32_t ix = 0; ix < _w; ix++) {
			tmp._roomMap[ix][0] = { WALL, style };
			tmp._roomMap[ix][_h - 1] = { WALL, style };
		}

		for (uint32_t iy = 1; iy < _h - 1; iy++) {
			tmp._roomMap[0][iy] = { WALL, style };
			tmp._roomMap[_w - 1][iy] = { WALL, style };
		}
		return tmp;
	}
	Room fill(const Block & block) {
		Room tmp = *this;
		for (uint32_t ix = 0; ix < _w; ix++) {
			for (uint32_t iy = 0; iy < _h; iy++) {
				tmp._roomMap[ix][iy] = block;
			}
		}
		return tmp;
	}
	uint32_t getX() const {
		return _x;
	}
	uint32_t getY() const {
		return _y;
	}
	uint32_t getWidth() const {
		return _w;
	}
	uint32_t getHeight() const {
		return _h;
	}
	void setBlock(uint32_t x, uint32_t y, const Block &block) {
		_roomMap[x][y] = block;
	}

	std::vector< std::vector <Block>> & getRawData() {
		return _roomMap;
	}


	SerializedObject serialize() const override {
		SerializedObject tmp;
		tmp.storeValue("_x", _x);
		tmp.storeValue("_y", _y);
		tmp.storeValue("_w", _w);
		tmp.storeValue("_h", _h);

		for (uint32_t ix = 0; ix < _w; ix++) {
			for (uint32_t iy = 0; iy < _h; iy++) {
				auto block = _roomMap[ix][iy];
				std::string id = std::to_string(ix) + ":" + std::to_string(iy);
				tmp.storeValue("t:" + id, block._type);

				std::vector<unsigned char> txt;
				txt.insert(txt.begin(), block._style.begin(), block._style.end());
				tmp.store("s:" + id, txt);
			}
		}
		return tmp;
	}
	void deserialize(const SerializedObject &object) override {
		object.loadValue("_x", _x);
		object.loadValue("_y", _y);
		object.loadValue("_w", _w);
		object.loadValue("_h", _h);
		_roomMap.resize(_w, std::vector<Block>(_h, { EMPTY,"" }));

		for (uint32_t ix = 0; ix < _w; ix++) {
			_roomMap[ix].resize(_h);
			for (uint32_t iy = 0; iy < _h; iy++) {
				Block block;
				std::string id = std::to_string(ix) + ":" + std::to_string(iy);

				object.loadValue("t:" + id, block._type);
				
				std::vector<unsigned char> txt;
				object.load("s:" + id, txt);
				block._style.insert(block._style.begin(), txt.begin(), txt.end());

				_roomMap[ix][iy] = block;
			}
		}
	}


private:
public:
	std::vector< std::vector <Block>> _roomMap;
};

#endif