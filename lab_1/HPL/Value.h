#ifndef VALUE_H
#define VALUE_H

#include <HPL/utils/String.h>
#include <HPL/InterpretationException.h>

namespace hpl {
	class Value {
	public:
		using Char = hpl::utils::Char;
		using String = hpl::utils::String;
		enum Type {
			BOOL,
			STRING,
			FLOAT,
			INT,
			UNDEFINED
		};
	private:
		union _ValuePtr {
			String *_string;
			long long *_int;
			long double *_float;
			bool *_bool;
		} _valuePtr;
	
		Type _type;
		size_t *_counter;
	public:
		size_t line = 0;
		String name = "";
		Value() {
			_type = UNDEFINED;
		}
		Value(bool val) {
			_valuePtr._bool = new bool(val);
			_type = BOOL;
			_counter = new size_t(1);
		}
		Value(long long val) {
			_valuePtr._int = new long long(val);
			_type = INT;
			_counter = new size_t(1);
		}
		Value(String val) {
			_valuePtr._string = new String(val);
			_type = STRING;
			_counter = new size_t(1);
		}
		Value(long double val) {
			_valuePtr._float = new long double(val);
			_type = FLOAT;
			_counter = new size_t(1);
		}
		Value(const Value &other):
			_valuePtr(other._valuePtr),
			_type(other._type),
			_counter(other._counter){
			if(_type != UNDEFINED)
				++(*_counter);
		}
		Value & operator=(const Value &other) {
			if (other._valuePtr._bool != _valuePtr._bool) {
				_valuePtr = other._valuePtr;
				_type = other._type;
				_counter = other._counter;
				if (_type != UNDEFINED)
					++(*_counter);
			}
			return *this;
		}
		Type getType() const {
			return _type;
		}
		bool isDefined() {
			return _type != UNDEFINED;
		}

		String castToString() {
			switch (_type) {
			case BOOL:
				return String((*(_valuePtr._bool)) ? "true" : "false");
				break;
			case STRING:
				return *(_valuePtr._string);
				break;
			case FLOAT:
				try {
					return std::to_string(*(_valuePtr._float));
				}
				catch (...) {
					throw InterpretationException(InterpretationException::CONVERSION, "Cannot convert float to string", line);
				}
				break;
			case INT:
				try {
					return std::to_string(*(_valuePtr._int));
				}
				catch (...) {
					throw InterpretationException(InterpretationException::CONVERSION, "Cannot convert integer to string", line);
				}
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		String toString() {
			switch (_type) {
			case BOOL:
				return String((*(_valuePtr._bool)) ? "true" : "false");
				break;
			case STRING:
				return *(_valuePtr._string);
				break;
			case FLOAT:
				try {
					return std::to_string(*(_valuePtr._float));
				}
				catch (...) {
					throw InterpretationException(InterpretationException::CONVERSION, "Cannot convert float to string", line);
				}
				break;
			case INT:
				try {
					return std::to_string(*(_valuePtr._int));
				}
				catch (...) {
					throw InterpretationException(InterpretationException::CONVERSION, "Cannot convert integer to string", line);
				}
				break;
			default:
				return "Unefined";
				break;
			}
		}
		long long castToInt() {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::CONVERSION, "Invalid conversion bool to integer", line);
				break;
			case STRING:
				try {
					return std::stoll(*(_valuePtr._string));
				}
				catch (...) {
					throw InterpretationException(InterpretationException::CONVERSION,
						"Cannot convert string \"" + *(_valuePtr._string) + "\" to int", line);
				}
				break;
			case FLOAT:
				
				return static_cast<long long>(*(_valuePtr._float));			
				break;
			case INT:
				return *(_valuePtr._int);
				break;
			default:
				if(name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
					"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		bool castToBool() {
			switch (_type) {
			case BOOL:
				return *(_valuePtr._bool);
				break;
			case STRING:
				return (*(_valuePtr._string)).empty();
				break;
			case FLOAT:
				return *(_valuePtr._float) == 0.0;
				break;
			case INT:
				return *(_valuePtr._int) == 0;
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		long double castToFloat() {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::CONVERSION, "Invalid conversion bool to float", line);
				break;
			case STRING:
				try {
					return std::stold(*(_valuePtr._string));
				}
				catch (...) {
					throw InterpretationException(InterpretationException::CONVERSION,
						"Cannot convert string \"" + *(_valuePtr._string) + "\" to float", line);
				}
				break;
			case FLOAT:
				return *(_valuePtr._float);
				break;
			case INT:
				return static_cast<long double>(*(_valuePtr._int));
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value add(Value &op_2) {
			switch (_type){
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot add bool values", line);
				break;
			case STRING:
				return Value(*(_valuePtr._string) + op_2.castToString());
				break;
			case FLOAT:
				return Value(*(_valuePtr._float) + op_2.castToFloat());
				break;
			case INT:
				return Value(*(_valuePtr._int) + op_2.castToInt());
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}

		Value sub(Value &op_2) {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot subtract bool values", line);
				break;
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot subtract string values", line);
				break;
			case FLOAT:
				return Value(*(_valuePtr._float) - op_2.castToFloat());
				break;
			case INT:
				return Value(*(_valuePtr._int) - op_2.castToInt());
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}

		Value mul(Value &op_2) {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot subtract multiply values", line);
				break;
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot subtract multiply values", line);
				break;
			case FLOAT:
				return Value(*(_valuePtr._float) * op_2.castToFloat());
				break;
			case INT:
				return Value(*(_valuePtr._int) * op_2.castToInt());
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		
		Value div(Value &op_2) {
			long long op_2_val;
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot subtract multiply values", line);
				break;
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot subtract multiply values", line);
				break;
			case FLOAT:
				return Value(*(_valuePtr._float) / op_2.castToFloat());
				break;
			case INT:
				op_2_val = op_2.castToInt();
				if (op_2_val == 0) {
					throw InterpretationException(InterpretationException::LOGICAL,
						"Division by zero", line);
				}
				return Value(*(_valuePtr._int) / op_2_val);
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}

		Value mod(Value &op_2) {
			long long op_2_val;
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot get modulo from bool values", line);
				break;
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot get modulo from string values", line);
				break;
			case FLOAT:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot get modulo from float values", line);
				break;
			case INT:
				op_2_val = op_2.castToInt();
				if (op_2_val == 0) {
					throw InterpretationException(InterpretationException::LOGICAL,
						"Division by zero", line);
				}
				return Value(*(_valuePtr._int) % op_2_val);
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value set(Value op_2) {
			if (!name.empty()) {
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot assign to rvalue", line);
			}
			switch (_type) {
			case BOOL:
				*(_valuePtr._bool) = op_2.castToBool();
				return *this;
				break;
			case STRING:
				*(_valuePtr._string) = op_2.castToString();
				return *this;
				break;
			case FLOAT:
				*(_valuePtr._float) = op_2.castToFloat();
				return *this;
				break;
			case INT:
				*(_valuePtr._int) = op_2.castToInt();
				return *this;
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}

		Value logicalAnd(Value &op_2) {
			switch (_type) {
			case BOOL:
			case STRING:
			case FLOAT:
			case INT:
				return castToBool() && op_2.castToBool();
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		
		Value logicalOr(Value &op_2) {
			switch (_type) {
			case BOOL:
			case STRING:
			case FLOAT:
			case INT:
				return castToBool() || op_2.castToBool();
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value logicalNot() {
			switch (_type) {
			case BOOL:
			case STRING:
			case FLOAT:
			case INT:
				return !castToBool();
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}

		Value isEquel(Value &op_2) {
			switch (_type) {
			case BOOL:
				return castToBool() == op_2.castToBool();
			case STRING:
				return castToString() == op_2.castToString();
			case FLOAT:
				return castToFloat() == op_2.castToFloat();
			case INT:
				return castToInt() == op_2.castToInt();
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value isLess(Value &op_2) {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot compare bool values", line);
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot compare string values", line);
			case FLOAT:
				return castToFloat() < op_2.castToFloat();
			case INT:
				return castToInt() < op_2.castToInt();
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value isLessOrEquel(Value &op_2) {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot compare bool values", line);
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot compare string values", line);
			case FLOAT:
				return castToFloat() <= op_2.castToFloat();
			case INT:
				return castToInt() <= op_2.castToInt();
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value increment() {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot increment bool values", line);
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot increment string values", line);
			case FLOAT:
				return ++(*(_valuePtr._float));
			case INT:
				return ++(*(_valuePtr._int));
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value decrement() {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot dencrement bool values", line);
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot dencrement string values", line);
			case FLOAT:
				return --(*(_valuePtr._float));
			case INT:
				return --(*(_valuePtr._int));
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value copy() {
			switch (_type) {
			case BOOL:
				return Value(*_valuePtr._bool);
			case STRING:
				return Value(*_valuePtr._string);
			case FLOAT:
				return Value(*_valuePtr._float);
			case INT:
				return Value(*_valuePtr._int);
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value negate() {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot negate bool values", line);
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot negate string values", line);
			case FLOAT:
				return Value(-(*_valuePtr._float));
			case INT:
				return Value(-(*_valuePtr._int));
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		Value promote() {
			switch (_type) {
			case BOOL:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot promote bool values", line);
			case STRING:
				throw InterpretationException(InterpretationException::LOGICAL,
					"Cannot promote string values", line);
			case FLOAT:
				return Value(+(*_valuePtr._float));
			case INT:
				return Value(+(*_valuePtr._int));
				break;
			default:
				if (name.empty())
					throw InterpretationException(InterpretationException::LOGICAL,
						"Value is undefined", line);
				else
					throw InterpretationException(InterpretationException::LOGICAL,
						"Variable " + name + " is undefined", line);
				break;
			}
		}
		~Value() {
			if (_type != UNDEFINED && --(*_counter) == 0) {
				delete _counter;
				switch (_type){
				case BOOL:
					delete _valuePtr._bool;
					break;
				case STRING:
					delete _valuePtr._string;
					break;
				case FLOAT:
					delete _valuePtr._float;
					break;
				case INT:
					delete _valuePtr._int;
					break;

				}
			}
		}

	};

}
#endif