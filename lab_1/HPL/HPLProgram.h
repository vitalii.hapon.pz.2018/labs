#ifndef PROGRAM_H
#define PROGRAM_H

#include <HPL/OperatorsList.h>
#include <HPL/parser/ProgramNode.h>
#include <HPL/parser/ProgramNode.h>
#include <HPL/Value.h>
#include <HPL/utils/String.h>
#include <HPL/Context.h>
#include <HPL/VisualizationTree.h>

namespace hpl {
	class HPLProgram {
	private:
		static OperatorsList operators;

		Context currentContext;
		Context defaultContext;
		std::unique_ptr<parser::AbstractNode> program;
	public:
		using String = utils::String;
		HPLProgram(String text);
		void optimize();
		void run();
		VisualizationTree createVisualizationTree();
		void addUserFunction(String name, std::vector<String> args, std::function<Value(Context &context)> function);

		const hpl::Context& getContext() const;
	};;
}
#endif 
