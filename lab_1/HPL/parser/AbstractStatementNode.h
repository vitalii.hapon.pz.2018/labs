#ifndef ABSTRACT_STATEMENT_NODE_H
#define ABSTRACT_STATEMENT_NODE_H

#include <HPL/parser/AbstractNode.h>

class hpl::parser::AbstractStatementNode : public AbstractNode {
public:
	using StatementRef = Ref<AbstractStatementNode>;
	static NodeRef create(TokenStream &stream);

	Value solve(Context &context) override  = 0;
	virtual bool tryOptimize(Value & res) = 0;

};
#endif