#ifndef INIT_STATEMENT_NODE_H
#define INIT_STATEMENT_NODE_H

#include <HPL/parser/AbstractStatementNode.h>
#include <HPL/parser/IdentifierNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/Context.h>

class hpl::parser::InitStatementNode : public AbstractStatementNode {
private:
	Ref<IdentifierNode> variable;
	NodeRef expression;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.checkAndAdvance(TokenType::KEYWORD, "var")) {
				
			if (Ref<IdentifierNode> variableNode = IdentifierNode::create(stream)) {
				if (stream.checkAndAdvance("=")) {
									
					if (auto expressionNode = ExpressionNode::create(stream)) {
					
							Ref<InitStatementNode> initStatement = alloc<InitStatementNode>();
							initStatement->variable		= std::move(variableNode);
							initStatement->expression	= std::move(expressionNode);

							return initStatement;
						
					}
				}
			}
		}
	
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		return context.initVariable(variable, expression->solve(context));
	}

	bool tryOptimize(Value & res) override {
		Value ores;
		if (expression->tryOptimize(ores)) {
			expression = AbstractTerminalNode::create(ores);
		}
		return false;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "InitStatement: ";

		auto tmp = VisualizationTree::alloc();
		tmp->name = "VAR";
		tmp->nodes.push_back(variable->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		tmp = std::move(VisualizationTree::alloc());
		tmp->name = "VALUE";
		tmp->nodes.push_back(expression->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		return res;
	}

};
#endif