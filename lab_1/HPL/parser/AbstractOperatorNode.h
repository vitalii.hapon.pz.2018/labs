#ifndef ABSTRACT_OPERATOR_NODE_H
#define ABSTRACT_OPERATOR_NODE_H

#include <HPL/parser/AbstractNode.h>
#include <HPL/OperatorsList.h>

class hpl::parser::AbstractOperatorNode : public AbstractNode {
	friend class Program;
	friend class ExpressionNode;
protected:
	static hpl::OperatorsList operators;
public:
	using Operators = hpl::OperatorsList::PriorityOperators;

	static NodeRef create(size_t priority, TokenStream &stream);

	Value solve(Context &context) override = 0;
	virtual bool tryOptimize(Value & res) = 0;

};
#endif