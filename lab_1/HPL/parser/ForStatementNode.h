#ifndef FOR_STATEMENT_NODE_H
#define FOR_STATEMENT_NODE_H

#include <HPL/parser/AbstractStatementNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/parser/InitStatementNode.h>
#include <HPL/parser/StatementBlockNode.h>


class hpl::parser::ForStatementNode : public AbstractStatementNode {
private:
	NodeRef expression_1;
	NodeRef expression_2;
	NodeRef expression_3;
	NodeRef statement;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.checkAndAdvance(TokenType::KEYWORD,"for")) {
			
			if (stream.checkAndAdvance(TokenType::LPAREN)) {
				
				NodeRef expressionNode_1 = InitStatementNode::create(stream);
				if (!expressionNode_1) 
					expressionNode_1 = ExpressionNode::create(stream);

				if (expressionNode_1) {
					if (stream.checkAndAdvance(TokenType::SEMICOLON)) {
						
						if (auto expressionNode_2 = ExpressionNode::create(stream)) {
							if (stream.checkAndAdvance(TokenType::SEMICOLON)) {
								
								if (auto expressionNode_3 = ExpressionNode::create(stream)) {
									if (stream.checkAndAdvance(TokenType::RPAREN)) {
										
										if (auto statementNode = AbstractStatementNode::create(stream)) {
											Ref<ForStatementNode> forStatement = alloc<ForStatementNode>();

											forStatement->expression_1 = std::move(expressionNode_1);
											forStatement->expression_2 = std::move(expressionNode_2);
											forStatement->expression_3 = std::move(expressionNode_3);

											forStatement->statement = std::move(statementNode);

											return forStatement;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		context.push(Context::Memory());
		expression_1->solve(context);
		while (expression_2->solve(context).castToBool()) {
			context.push(Context::Memory());
			statement->solve(context);
			context.pop();
			if (context.isFlowChanged()) {
				if (context.getJumpFlag() == Context::BREAK) {
					context.restoreFlow();
					break;
				}
				else if (context.getJumpFlag() == Context::CONTINUE){
					context.restoreFlow();
					expression_3->solve(context);
					continue;
				}
				break;
			}
			expression_3->solve(context);
		}
		context.pop();
		//variable->solve(context);
		//expression->solve(context);
		return Value();
	}

	bool tryOptimize(Value & res) override {
		Value ores;
		expression_1->tryOptimize(ores);
		expression_2->tryOptimize(ores);
		expression_3->tryOptimize(ores);
		statement->tryOptimize(ores);
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "ForStatement: ";

		auto tmp = VisualizationTree::alloc();
		tmp->name = "INIT";
		tmp->nodes.push_back(expression_1->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));


		tmp = VisualizationTree::alloc();
		tmp->name = "CONDITION";
		tmp->nodes.push_back(expression_2->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		tmp = VisualizationTree::alloc();
		tmp->name = "ACTION";
		tmp->nodes.push_back(expression_3->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		tmp = VisualizationTree::alloc();
		tmp->name = "THEN";
		tmp->nodes.push_back(statement->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		return res;
	}
};
#endif