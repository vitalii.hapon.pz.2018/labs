#ifndef PROGRAM_NODE_H
#define PROGRAM_NODE_H

#include <HPL/parser/AbstractNode.h>
#include <HPL/parser/AbstractStatementNode.h>
#include <HPL/parser/FunctionNode.h>
#include <vector>

class hpl::parser::ProgramNode : public AbstractNode {
private:
	std::vector<NodeRef> statements;
public:
	static NodeRef create(TokenStream &stream) {
		Ref<ProgramNode> program = alloc<ProgramNode>();
		while (stream.available()) {
			if (auto statement = AbstractStatementNode::create(stream))
				program->statements.push_back(std::move(statement));
			else if (auto functionNode = FunctionNode::create(stream))
				program->statements.push_back(std::move(functionNode));
			else
				return std::move(program);
		}
		return program;
	}
	Value solve(Context &context) override {
		for (auto &s : statements) {
			s->solve(context);
			if (context.isFlowChanged())
			
				if(context.getJumpFlag() == Context::BREAK)
					throw InterpretationException(InterpretationException::LOGICAL, "Break statement outside of loop", line);
				else if (context.getJumpFlag() == Context::CONTINUE)
					throw InterpretationException(InterpretationException::LOGICAL, "Continue statement outside of loop", line);				
				else 
					throw InterpretationException(InterpretationException::LOGICAL, "Return statement outside of function", line);
				
		}
		return Value();
	}
	bool tryOptimize(Value & res) override {
		for (auto &s : statements) {
			Value ores;
			s->tryOptimize(ores);
		}
		return false;
	}
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Program: ";
		for (auto &s : statements) {
			res->nodes.push_back(s->createVisualizationTreeNode());
		}
		return res;
	}

};
#endif