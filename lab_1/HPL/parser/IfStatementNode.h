#ifndef if_STATEMENT_NODE_H
#define if_STATEMENT_NODE_H

#include <HPL/parser/AbstractStatementNode.h>
#include <vector>

class hpl::parser::IfStatementNode : public AbstractStatementNode {
private:
	NodeRef expression;
	NodeRef statement;
	NodeRef elseStatement;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.checkAndAdvance(TokenType::KEYWORD, "if")) {
			
			if (stream.checkAndAdvance(TokenType::LPAREN)) {
				

				NodeRef expressionNode = InitStatementNode::create(stream);
				if (!expressionNode)
					expressionNode = ExpressionNode::create(stream);
				if (stream.checkAndAdvance(TokenType::RPAREN)) {
					
					if (auto statementNode = AbstractStatementNode::create(stream)) {
						Ref<IfStatementNode> ifStatement = alloc<IfStatementNode>();

						ifStatement->expression = std::move(expressionNode);
						ifStatement->statement = std::move(statementNode);
						ifStatement->elseStatement = nullptr;

						auto saved_2 = stream.getIdentifier();
						if (stream.available() && stream.checkAndAdvance(TokenType::KEYWORD, "else")) {
							
							if (auto elseStatementNode = AbstractStatementNode::create(stream)) {
								ifStatement->elseStatement = std::move(elseStatementNode);
								return ifStatement;
							}
						}

						stream.setIdentifier(saved_2);
						return ifStatement;
					}
				}
				
			}
		}

		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		context.push(Context::Memory());
		if (expression->solve(context).castToBool()) 
			statement->solve(context);
		else if (elseStatement)
			elseStatement->solve(context);
		context.pop();
		return Value();
	}

	bool tryOptimize(Value & res) override {
		Value ores;
		expression->tryOptimize(ores);
		if(elseStatement)
			elseStatement->tryOptimize(ores);
		statement->tryOptimize(ores);
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const override {
		auto res = VisualizationTree::alloc();
		res->name = "IfStatement: ";

		auto tmp = VisualizationTree::alloc();
		tmp->name = "IF";
		tmp->nodes.push_back(expression->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));


		tmp = VisualizationTree::alloc();
		tmp->name = "THEN";
		tmp->nodes.push_back(statement->createVisualizationTreeNode());

		res->nodes.push_back(std::move(tmp));

		if (elseStatement) {
			tmp = VisualizationTree::alloc();
			tmp->name = "ELSE";
			tmp->nodes.push_back(elseStatement->createVisualizationTreeNode());

			res->nodes.push_back(std::move(tmp));
		}
		
		return res;
	}

};
#endif