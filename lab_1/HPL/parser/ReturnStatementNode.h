#ifndef RETURN_STATEMENT_NODE_H
#define RETURN_STATEMENT_NODE_H

#include <HPL/parser/AbstractStatementNode.h>
#include <HPL/parser/ExpressionNode.h>
#include <HPL/TokenStream.h>

class hpl::parser::ReturnStatementNode : public AbstractStatementNode {
private:
	NodeRef expression;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::KEYWORD,"return")){
			auto tmpLine = stream.currrentLine();
			stream.advance();
			if (auto expressionNode = ExpressionNode::create(stream)) {
				if (stream.checkAndAdvance(TokenType::SEMICOLON)) {
					
					Ref<ReturnStatementNode> returnStatementNode = alloc<ReturnStatementNode>();
					returnStatementNode->expression = std::move(expressionNode);
					returnStatementNode->line = tmpLine;

					return returnStatementNode;
				}
			}
			if (stream.checkAndAdvance(TokenType::SEMICOLON)) {
				
				Ref<ReturnStatementNode> returnStatementNode = alloc<ReturnStatementNode>();
				returnStatementNode->expression = nullptr;
				returnStatementNode->line = tmpLine;

				return returnStatementNode;
			}
			
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	Value solve(Context &context) override {
		if (expression)
			context.functionReturn(expression ? expression->solve(context) : Value());

		return Value();
	}
	bool tryOptimize(Value & res) override {
		Value ores;
		if (expression->tryOptimize(ores)) {
			expression = AbstractTerminalNode::create(ores);
		}
		return false;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		
		if (expression) {
			res->name = "Return: ";
			res->nodes.push_back(expression->createVisualizationTreeNode());
		}
		else {
			res->name = "Return; ";
		}
		return res;
	}

};
#endif