#ifndef ABSRACT_NODE_H
#define ABSRACT_NODE_H

#include <HPL/utils/String.h>
#include <HPL/Value.h>
#include <HPL/Context.h>
#include <HPL/parser/Parser.h>
#include <HPL/lexer/Tokenizer.h>
#include <HPL/VisualizationTree.h>

#include <HPL/TokenStream.h>

#include <memory>

class hpl::parser::AbstractNode {
protected:
	size_t line;
public:
	template<class T>
	using Ref = std::unique_ptr<T>;
	using NodeRef = std::unique_ptr<AbstractNode>;

	template<class T, class... Args>
	static 	Ref<T> alloc(Args&&... args) {
		return std::make_unique<T>(args...);
	}

	size_t getLine() const {
		return line;
	}

	using String	= hpl::utils::String;	
	using Value		= hpl::Value;
	using Context	= hpl::Context;
	using Token		= hpl::lexer::Tokenizer::Token;
	using TokenType = hpl::lexer::Tokenizer::TokenType;

	using TokenIterator = hpl::lexer::Tokenizer::TokenIterator;

	virtual Value solve(Context &context) = 0;
	virtual bool tryOptimize(Value &res) = 0;
	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const = 0;


	virtual ~AbstractNode() {}

};
#endif