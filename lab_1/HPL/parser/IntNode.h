#ifndef INT_NODE_H
#define INT_NODE_H

#include <HPL/parser/AbstractTerminalNode.h>

class hpl::parser::IntNode : public AbstractTerminalNode {
private:
	long long value;
public:
	static NodeRef create(TokenStream &stream) {
		auto saved = stream.getIdentifier();

		if (stream.check(TokenType::INT)) {
			Ref<IntNode> intNode = alloc<IntNode>();
			intNode->value = std::stoll(stream.currrentValue());
			intNode->line = stream.currrentLine();
			stream.advance();
			return intNode;
		}
		stream.setIdentifier(saved);
		return nullptr;
	}
	static NodeRef create(Value &val) {
		Ref<IntNode> intNode = alloc<IntNode>();
		intNode->value = val.castToInt();
		return intNode;
	}
	Value solve(Context &context) override {
		return Value(value);
	}

	bool tryOptimize(Value & res) override {
		res = Value(value);
		return true;
	}

	virtual VisualizationTree::NodeRef createVisualizationTreeNode() const {
		auto res = VisualizationTree::alloc();
		res->name = "Int(" + std::to_string(value) + ")";
		return res;
	}

};
#endif