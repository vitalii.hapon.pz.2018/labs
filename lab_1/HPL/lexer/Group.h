#ifndef GROUP_H
#define GROUP_H

#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Rule.h>
#include <HPL/utils/String.h>


#include <vector>
#include <memory>

class hpl::lexer::Group: public Rule {
private:
	std::vector<Rule*> sequences;
	std::vector<Rule*> cloneSequences() const;
public:
	class Quantifier {
		friend Group;
	private:
		size_t minCount, maxCount;
	public:
		static const size_t max = ~size_t();

		Quantifier(const Quantifier&) = default;
		Quantifier(Quantifier&&) = default;
		Quantifier& operator=(const Quantifier&) = default;
		Quantifier& operator=(Quantifier&&) = default;

		Quantifier(size_t count = 1);
		Quantifier(size_t minCount, size_t maxCount);
	};
	Group(const Group&);
	Group(Group&&);
	Group& operator=(const Group&);
	Group& operator=(Group&&);
	~Group();

	template<typename ...Args>
	Group(Quantifier q, const Rule& seq, const Args& ...args);

	Rule *clone() const;

	Group operator+(const Rule& v) const;
	Group& operator+=(const Rule& v);	

	String toString() const override;

	bool match(StringConstIterator &beg, const StringConstIterator &end) const;
private:
	Quantifier quantifier;
};

template<typename ...Args>
hpl::lexer::Group::Group(Quantifier q, const Rule& seq, const Args& ...args):
	quantifier(q){
	sequences.push_back(seq.clone());
	(sequences.push_back(args.clone()), ...);
}

#endif