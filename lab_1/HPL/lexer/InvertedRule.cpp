#include <HPL/lexer/InvertedRule.h>

using namespace hpl::lexer;
using namespace hpl::utils;


//--------------------------------------------------------------------------------
Rule* InvertedRule::clone() const {
	return new InvertedRule(*this);
}
//--------------------------------------------------------------------------------
InvertedRule::InvertedRule(const Rule& other) :
	rule(other.clone()) {}
//--------------------------------------------------------------------------------
String InvertedRule::toString() const{
	String string;
	string += "!";
	string += rule->toString();
	return string;
}
//--------------------------------------------------------------------------------
bool InvertedRule::match(StringConstIterator &beg, const StringConstIterator &end) const {
	if (beg >= end)
		return false;

	auto tmpBeg = beg;
	if (!rule->match(tmpBeg, end)) {

		return true;
	}
	//beg = tmpBeg;
	return false;
}
//--------------------------------------------------------------------------------
