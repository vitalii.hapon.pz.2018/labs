#include <HPL/lexer/RuleSet.h>


using namespace hpl::lexer;
using namespace hpl::utils;


//--------------------------------------------------------------------------------
RuleSet::RuleSet(const RuleSet& other):
	set(other.cloneSet()){}
//--------------------------------------------------------------------------------
RuleSet& RuleSet::operator=(const RuleSet& other) {
	set = { other.cloneSet() };
	return *this;
}
//--------------------------------------------------------------------------------
RuleSet::~RuleSet() {
	for (auto &i : set)
		delete i;
}
//--------------------------------------------------------------------------------
Rule* RuleSet::clone() const {
	return new RuleSet(*this);
}
//--------------------------------------------------------------------------------
std::vector<Rule*> RuleSet::cloneSet() const {
	std::vector<Rule*> res;
	res.reserve(set.size());
	for (auto &set : set) {
		res.push_back(set->clone());
	}
	return res;
}
//--------------------------------------------------------------------------------
RuleSet RuleSet::operator+(const RuleSet& v) const {
	auto res = *this;
	res.set.insert(res.set.end(), v.set.begin(), v.set.end());
	return res;
}
//--------------------------------------------------------------------------------
RuleSet& RuleSet::operator+=(const RuleSet& v) {
	set.insert(set.end(), v.set.begin(), v.set.end());
	return *this;
}
//--------------------------------------------------------------------------------
String RuleSet::toString() const{
	String string;
	string += "[ ";
	bool  comma = false;
	for (auto v : set) {
		if (comma)
			string += ", ";
		else
			comma = true;

		string += v->toString();
	}
	string += " ]";
	return string;
}
//--------------------------------------------------------------------------------
bool RuleSet::match(StringConstIterator &beg, const StringConstIterator &end) const {
	if (beg >= end)
		return false;

	// check each rule  until the matching rule is found:
	for (auto &it : set) 
		if (it->match(beg, end)) 		
				return true;
		
	return false;
}
//--------------------------------------------------------------------------------