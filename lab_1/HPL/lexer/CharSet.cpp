#include <HPL/lexer/CharSet.h>

#include <algorithm>

using namespace hpl::lexer;
using namespace hpl::utils;

//--------------------------------------------------------------------------------
CharSet::CharSet(Char from, Char to) :
	charRanges({std::pair{from,to}}) {}
//--------------------------------------------------------------------------------
CharSet::CharSet(Char ch):
	chars({ ch }) {}
//--------------------------------------------------------------------------------
CharSet::CharSet(String chars):
	chars(chars.begin(), chars.end()){}
//--------------------------------------------------------------------------------
Rule* CharSet::clone() const {
	return new CharSet(*this);
}
//--------------------------------------------------------------------------------
CharSet::CharSet(CharClass charClass){
	switch (charClass){
	case lexer::CharSet::ANY:
		charRanges.insert({ {Char(), charMaxValue} });
		break;
	case lexer::CharSet::SPACE:
		chars.insert({ ' ', '\f', '\n', '\r', '\t', '\v' });
		break;
	case lexer::CharSet::ALPHA:
		charRanges.insert({ {'a','z'}, {'A', 'Z'} });
		break;
	case lexer::CharSet::DIGIT:
		charRanges.insert({ {'0','9'} });
		break;
	case lexer::CharSet::HEX_DIGIT:
		charRanges.insert({ {'0','9'} , {'a', 'f'}, {'A', 'F' } });
		chars.insert({ 'x','X' });
		break;
	case lexer::CharSet::LOWER:
		charRanges.insert({ {'a','z'} });
		break;
	case lexer::CharSet::UPPER:
		charRanges.insert({ {'A', 'Z'} });
		break;
	case lexer::CharSet::PUNCT:
		charRanges.insert({ {'!','/'}, {':', '@'}, {'[', '\''},{'{', '~'} });
		break;
	}
}
//--------------------------------------------------------------------------------
CharSet CharSet::operator+(const CharSet& v) const {
	CharSet res = *this;
	res.chars.insert(v.chars.begin(), v.chars.end());
	res.charRanges.insert(v.charRanges.begin(), v.charRanges.end());
	return res;
}
//--------------------------------------------------------------------------------
CharSet &CharSet::operator+=(const CharSet& v) {
	chars.insert(v.chars.begin(), v.chars.end());
	charRanges.insert(v.charRanges.begin(), v.charRanges.end());
	return *this;
}
//--------------------------------------------------------------------------------
String CharSet::toString() const{
	String string;
	string += "[ ";
	bool  comma = false;
	for (auto ch : chars) {
		if (comma) 
			string += ", ";
		else
			comma = true;
		
			string += "( ";
			string += String(1, ch);
			string += " )";
	}
	for (auto ch : charRanges) {
		if (comma)
			string += ", ";
		else
			comma = true;

		string += "( ";
		string += String(1, ch.first) + ":" + String(1, ch.second);
		string += " )";
	}
	string += " ]";
	return string;
}
//--------------------------------------------------------------------------------
bool CharSet::match(StringConstIterator &beg, const StringConstIterator &end) const {
	if (beg >= end)
		return false;

	Char ch = *beg;
	auto it = chars.find(ch);
	if (it != chars.end()) {
		++beg;
		return true;
	}

	// check each char range until the matching char is found:
	for (auto &it : charRanges)
		if (it.first <= ch && ch <= it.second) {
			++beg;
			return true;
		}
	return false;
}
//--------------------------------------------------------------------------------
CharSet CharSet::operator/(const std::set<Char>& v) const {
	auto res = *this;

	// excluding of all banned character from char set:
	for (auto it = res.chars.begin(); it != res.chars.end(); ) {
		if (v.find(*it) != v.end())
			it = res.chars.erase(it);
		else
			it++;
	}

	// excluding of all banned character from char ranges:
	for (auto it = res.charRanges.begin(); it != res.charRanges.end(); it++) {
		for (auto &ch : v) {
			if (it->first <= ch && ch <= it->second) {
				Char min = it->first;
				Char mid = ch;
				Char max = it->second;
				res.charRanges.erase(it);
				res.charRanges.insert({ { min, mid - 1 }, {mid + 1, max} });
				
				it = res.charRanges.begin();
			}				
		}
	}

	return res;
}
//--------------------------------------------------------------------------------
