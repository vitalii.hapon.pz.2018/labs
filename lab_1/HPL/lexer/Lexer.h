#ifndef LEXER_H
#define LEXER_H

#include <HPL/utils/String.h>
#include <vector>

namespace hpl {
	namespace lexer {
		class Rule;
		class CharSet;
		class StringSet;
		class RuleSet;
		class Group;
		class Sequence;
		class Tokenizer;
		class InvertedRule;
		enum TokenType {
			NONE,

			LPAREN,
			RPAREN,
			LBRACE,
			RBRACE,
			COMMA,
			SEMICOLON,
			OPERATOR,
			IDENTIFIER,

			INT,
			STRING,
			FLOAT,
			BOOL,

			KEYWORD,
		};

		struct Token {
			hpl::utils::String value;

			TokenType type;
			size_t pos;
			size_t line;
		};

		using Tokens = std::vector<Token>;
		using TokenIterator = std::vector<Token>::const_iterator;

	}
}
#endif