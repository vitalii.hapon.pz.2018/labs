#ifndef RULE_SET_H
#define RULE_SET_H

#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Rule.h>
#include <HPL/utils/String.h>


#include <vector>
#include <initializer_list>

class hpl::lexer::RuleSet: public Rule {
private:
	std::vector<Rule*> set;
	std::vector<Rule*> cloneSet() const;
public:
	RuleSet() = default;
	RuleSet(const RuleSet&);
	RuleSet(RuleSet&&) = default;
	RuleSet& operator=(const RuleSet&);
	~RuleSet();

	template<typename ...Args>
	RuleSet(const Rule& set, const Args& ...args);

	Rule* clone() const override;

	RuleSet operator+(const RuleSet& v) const;
	RuleSet& operator+=(const RuleSet& v);

	String toString() const override;

	bool match(StringConstIterator &beg, const StringConstIterator &end) const override;
};

template<typename ...Args>
hpl::lexer::RuleSet::RuleSet(const Rule& seq, const Args& ...args){
	set.push_back(seq.clone());
	(set.push_back(args.clone()), ...);
}

#endif