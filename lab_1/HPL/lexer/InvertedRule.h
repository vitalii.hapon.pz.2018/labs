#ifndef INVERTED_RULE_H
#define INVERTED_RULE_H

#include <HPL/lexer/Lexer.h>
#include <HPL/lexer/Rule.h>
#include <HPL/utils/String.h>

#include <set>
#include <initializer_list>

class hpl::lexer::InvertedRule: public Rule{
private:
	Rule *rule;
public:
	InvertedRule(const Rule& rule);

	Rule* clone() const override;

	String toString() const override;

	bool match(StringConstIterator &beg, const StringConstIterator &end) const override;
};

#endif

