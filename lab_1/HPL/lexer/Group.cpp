#include <HPL/lexer/Group.h>

using namespace hpl::lexer;
using namespace hpl::utils;

//--------------------------------------------------------------------------------
Group::Group(const Group& other):
	quantifier(other.quantifier),
	sequences(other.cloneSequences()) {}
//------------------ --------------------------------------------------------------
Group::Group(Group&& other):
	quantifier(other.quantifier),
	sequences(other.cloneSequences()) {}
//--------------------------------------------------------------------------------
Group& Group::operator=(const Group& other) {
	if (&other == this)
		return *this;
	this->quantifier = other.quantifier;
	this->sequences = other.cloneSequences();
	return *this;
}
//--------------------------------------------------------------------------------
Group& Group::operator=(Group&& other) {
	if (&other == this)
		return *this;
	this->quantifier = other.quantifier;
	this->sequences = other.cloneSequences();
	return *this;
}
//--------------------------------------------------------------------------------
Group::~Group() {
	for (auto &set : sequences) 
		delete set;
}
//--------------------------------------------------------------------------------
Rule *Group::clone() const {
	return new Group(*this);
}
//--------------------------------------------------------------------------------
std::vector<Rule*> Group::cloneSequences() const {
	std::vector<Rule*> res;
	res.reserve(sequences.size());
	for (auto &set : sequences) {
		res.push_back(set->clone());
	}
	return res;
}
//--------------------------------------------------------------------------------
Group::Quantifier::Quantifier(size_t count): 
	minCount(count), 
	maxCount(count) {}
//--------------------------------------------------------------------------------
Group::Quantifier::Quantifier(size_t minCount, size_t maxCount): 
	minCount(minCount), 
	maxCount(maxCount) {}
//--------------------------------------------------------------------------------
Group Group::operator+(const Rule& v) const {
	return *this;
}
//--------------------------------------------------------------------------------
Group& Group::operator+=(const Rule& v) {
	return *this;
}
String Group::toString() const{
	String string;

	string += "{ ";
	bool  comma = false;
	for (auto v : sequences) {
		if (comma)
			string += ", ";
		else
			comma = true;

		
		string += v->toString();
	}
	string += " }";
	return string;
}
//--------------------------------------------------------------------------------
bool Group::match(StringConstIterator &beg, const StringConstIterator &end) const {
	// check each rule  until the matching group of rule is found:
	for (auto &set : sequences) {
		auto tmpBeg = beg;
		size_t max = quantifier.maxCount;
		size_t count = 0;

		// check rule until number of matches is less than quantifier.maxCount:
		while (count < max && tmpBeg != end) {
			if (set->match(tmpBeg, end))
				count++;
			else
				break;
		}
		// if number of mathcer is greater or equel to quantifier.minCount the text matches the group:
		if (count >= quantifier.minCount) {
			beg = tmpBeg;
			return true;
		}
	}
	return false;
}
//--------------------------------------------------------------------------------
