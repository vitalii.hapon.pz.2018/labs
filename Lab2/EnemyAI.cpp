#include "EnemyAI.h"
#include "Command.h"
#include "Level.h"

#include <cmath>

CommandPtr FindPlayerAI::calculate(Level &level, Enemy &enemy){

	auto playerX = level.getPlayer().getX();
	auto playerY = level.getPlayer().getY();

	auto enemyX = enemy.getX();
	auto enemyY = enemy.getY();

	auto enemyMaxSpeed = enemy.getMaxSpeed();

	auto vecx = playerX - enemyX;
	auto vecy = playerY - enemyY;

	auto len = std::sqrt(vecx*vecx + vecy * vecy);

	auto velx = enemyMaxSpeed * (vecx / len);
	auto vely = enemyMaxSpeed * (vecy / len);

	return  std::make_unique<MoveCommand>(-velx, -vely);
}

CommandPtr IdleAI::calculate(Level &level, Enemy &enemy) {
	return std::make_unique<IdleCommand>();
}

CommandPtr AttackAI::calculate(Level &level, Enemy &enemy) {
	return std::make_unique<DamageCommand>(1);
}

CommandPtr PassTheObstacleAI::calculate(Level &level, Enemy &enemy) {
	return std::make_unique<IdleCommand>();
}