#include "RenderEngine.h"
#include "Level.h"
#include "ResourceManager.h"

#include <glad/glad.h>
#include <SFML/Graphics.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

RenderEngine::RenderEngine(int w, int h, Level & level) :
	_resManager(ResourceManager::getInstance()),
	_level(level),
	_w(w),
	_h(h){

	_refresh();

	shader.loadFromFile("sh.vert", "sh.frag");
	

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glClearDepth(1.f);

	glDisable(GL_LIGHTING);
	glViewport(0, 0, w, h);


	const float ratio = static_cast<float>(w) / h;
	const float zNear = 0.001f;
	const float zFar = 500.f;
	const float fov = 90.f;

	_projectionMatrix = glm::perspective(glm::radians(fov), ratio, zNear, zFar);

	glEnable(GL_CULL_FACE);
	

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glAlphaFunc(GL_GREATER, 0.5);
	glEnable(GL_ALPHA_TEST);
}
void RenderEngine::setHandItemName(const std::string & name) {
	_handItemName = name;
}
void RenderEngine::toggleWireFrame() {
	_showWireFrame = !_showWireFrame;
	if (_showWireFrame) {
		glLineWidth(5.f);
		glDisable(GL_CULL_FACE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else {
		glEnable(GL_CULL_FACE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void RenderEngine::_refresh() {
	auto &room = _level.getMainRoom();
	auto &roomData = _level.getMainRoom().getRawData();

	auto testNeighborDrawOptimisation = [&](int xoff, int yoff) {

		if (xoff < 0 || xoff >= room.getWidth())
			return false;


		if (yoff < 0 || yoff >= room.getHeight())
			return false;
		
		auto &res = roomData[xoff][yoff];

		if (res._type == Room::EMPTY || res._type == Room::WALL)
			return false;

		return true;
	};
		

	for (size_t xi = 0; xi < room.getWidth(); xi++) {
		for (size_t yi = 0; yi < room.getHeight(); yi++) {

			auto block = roomData[xi][yi];

			Model3D::Face face;

			switch (block._type) {
			case Room::WALL:
				_levelModel.createNewObject(block._style + "/" + "wall.png");

			
				face.y = 0.f;
				face.w = 1.f;
				face.h = 1.5f;
				
				if (testNeighborDrawOptimisation(xi, yi - 1)) {
					face.x = xi;
					face.z = yi - 1.f;
					_levelModel.addXYFace(false, face);
				}

				if (testNeighborDrawOptimisation(xi , yi + 1)) {
					face.x = xi;
					face.z = yi;
					_levelModel.addXYFace(true, face);
				}

				if (testNeighborDrawOptimisation(xi + 1, yi)) {
					face.x = xi + 1.f;
					face.z = yi - 1.f;
					_levelModel.addYZFace(false, face);
				}
				
				if (testNeighborDrawOptimisation(xi - 1, yi)) {
					face.x = xi;
					face.z = yi - 1.f;
					_levelModel.addYZFace(true, face);
				}

				break;
			case Room::FLOOR:

				face.x = xi;
				face.z = yi - 1.f;

				face.w = 1.f;
				face.h = 1.f;

				_levelModel.createNewObject(block._style + "/" + "floor.png");
				face.y = 0.f;
				_levelModel.addXZFace(false, face);

				_levelModel.createNewObject(block._style + "/" + "ceil.png");
				face.y = 1.5f;
				_levelModel.addXZFace(true, face);

				break;

			default:
				break;
			}
		}
	}
}

void RenderEngine::resize(int w, int h) {
	glViewport(0, 0, w, h);

	const float ratio = static_cast<float>(w) / h;
	const float zNear = 0.001f;
	const float zFar = 500.f;
	const float fov = 90.f;

	_projectionMatrix = glm::perspective(glm::radians(fov), ratio, zNear, zFar);
}

void RenderEngine::_setupCamera() {
	static float time = 0.f;

	auto & player = _level.getPlayer();

	if (player.isMoving()) {
		time = std::clock() / 90.f;
	}


	_viewMatrix = glm::mat4(1.f);

	float xr = glm::radians(player.getVerticalViewAngle());
	float xy = glm::radians(player.getDirection());

	_viewMatrix = glm::rotate(_viewMatrix, xr, { 1.f, 0.f, 0.f });
	_viewMatrix = glm::rotate(_viewMatrix, xy, { 0.f, 1.f, 0.f });
	_viewMatrix = glm::translate(_viewMatrix, { -player.getX(), -0.75f + std::sin(time) * 0.03f, -player.getY() });
}

void RenderEngine::render() {
	ResourceManager & resMan = ResourceManager::getInstance(); 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	sf::Shader::bind(&shader);

	_setupCamera();

	_modelMatrix = glm::mat4(1.f);
	glm::mat4 mvpMatrix = _projectionMatrix * _viewMatrix * _modelMatrix;
	shader.setUniform("mvpMatrix", *reinterpret_cast<sf::Glsl::Mat4*>(&mvpMatrix[0][0]));
	shader.setUniform("time", static_cast<float>(std::clock()));

	_levelModel.draw();


	static const GLfloat face[] = {

		0, 0,  -0.5,	0, 1,
		0, 1.5, -0.5,	0, 0,
		0, 0, 0.5,		1, 1,
		0, 0, 0.5,		1, 1,
		0, 1.5, -0.5,	0, 0,
		0, 1.5, 0.5,	1, 0,
	};

	glEnableVertexAttribArray(0);   
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), face);      
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), face + 3);    
	
	static float time = 0.f;
	time = std::clock() / 290.f;
	
	sf::Vector2f xv;
	sf::Vector2f yv;

	yv.x = 1;
	yv.y = 0;
	
	for (auto &c : _level.getEnemies()) {
		
		static float time2 = 0.f;

		auto & player = _level.getPlayer();

		if (player.isMoving()) {
			time2 = std::clock() / 90.f;
		}

	

		xv.x = c->getX() - player.getX();
		xv.y = c->getY() - player.getY();

		auto enemyMaxSpeed = 0.01f;

		auto vecx = xv.x;
		auto vecy = xv.y;

		auto len = std::sqrt(vecx*vecx + vecy * vecy);

		auto velx = enemyMaxSpeed *(vecx / len);
		auto vely = enemyMaxSpeed * (vecy / len);
		
		if (len < 6) {
			if (len > 0.5) {
				c->setX(c->getX() - velx);
				c->setY(c->getY() - vely);
			}
			else {
				if (std::clock() % 100 == 0)
					player.damage(1);
			}
		}


		float dot = xv.x * yv.x + xv.y * yv.y;
		float norm = std::sqrt(xv.x * xv.x + xv.y * xv.y) * std::sqrt(yv.x * yv.x + yv.y * yv.y);
		float cos = dot / norm;
		float angle = 0 ;
		if (xv.y > 0) {
			angle = 180.f + std::acos(dot / norm) / 3.1416 * 180.f;
		}
		else {
			angle = 180.f - std::acos(dot / norm) / 3.1416 * 180.f;;
		}

		_modelMatrix = glm::mat4(1.f);
		_modelMatrix = glm::translate(_modelMatrix, { c->getX(), 0.05f - std::sin(time) * 0.07f, c->getY() });
		_modelMatrix = glm::rotate(_modelMatrix, glm::radians(angle), {0.f, -1.f, 0.f});

		glm::mat4 mvpMatrix = _projectionMatrix * _viewMatrix * _modelMatrix;
		shader.setUniform("mvpMatrix", *reinterpret_cast<sf::Glsl::Mat4*>(&mvpMatrix[0][0]));
		
		auto &tex = resMan.loadTexture(c->getTextureName());
		glEnable(GL_TEXTURE_2D);
		sf::Texture::bind(&tex);

		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	
	
}

