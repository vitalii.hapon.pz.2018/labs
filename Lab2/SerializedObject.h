#ifndef SERIALIZED_OBJECT_H
#define SERIALIZED_OBJECT_H

#include <string>
#include <vector>
#include <map>

class SerializedObject;
class ISerializable;

class SerializedValue {
private:
	size_t _size;
	union Ptr{
		SerializedObject *so;
		unsigned char *bin;
	} _ptr;
public:
	enum Type { BINARY, OBJECT };


	SerializedValue() = default;
	SerializedValue(const SerializedValue & other);
	SerializedValue(SerializedValue && other) = default;

	SerializedValue operator=(const SerializedValue & other);
	SerializedValue operator=(SerializedValue && other);


	template<class T>
	void createFromBinary(const T *data);

	void create(const ISerializable &data);
	void create(const SerializedObject &data);
	void create(const std::vector<unsigned char> &data);


	~SerializedValue();

	std::vector<unsigned char> toBinary() const;

	template<class T>
	void loadRawData(T &dest) const;

	void loadData(ISerializable &dest) const;
	void loadData(SerializedObject &value) const;
	void loadData(std::vector<unsigned char> &value) const;


	Type getType() const;
private:
	Type _type;
};


class SerializedObject {
private:
	std::map<std::string, SerializedValue> _map;
public:

	SerializedObject() = default;
	SerializedObject(const std::vector<unsigned char> &data);

	template<class T>
	void storeValue(const std::string &key, const T &value);

	template<class T>
	void loadValue(const std::string &key, T &value) const;

	void store(const std::string &key, const ISerializable &value);
	void load(const std::string &key, ISerializable &value) const;

	void store(const std::string &key, const std::vector<unsigned char> &value);
	void load(const std::string &key, std::vector<unsigned char> &value) const;

	void store(const std::string &key, const SerializedObject &value);
	void load(const std::string &key, SerializedObject &value) const;

	std::vector<unsigned char> toBinary() const;

};

class ISerializable {
public:
	virtual SerializedObject serialize() const = 0;
	virtual void deserialize(const SerializedObject &object) = 0;
};

template<class T>
void SerializedValue::createFromBinary(const T *data) {
	_type = BINARY;
	_size = sizeof(T);
	_ptr.bin = new unsigned char[sizeof(T)];
	std::memcpy(_ptr.bin, data, sizeof(T));
}

template<class T>
void SerializedValue::loadRawData(T &dest) const {
	if (_type == BINARY)
		std::memcpy((void*)&dest, _ptr.bin, _size);
}

template<class T>
void SerializedObject::storeValue(const std::string &key, const T &value) {
	_map[key] = SerializedValue();
	_map[key].createFromBinary(&value);
}


template<class T>
void SerializedObject::loadValue(const std::string &key, T &value) const {
	auto it = _map.find(key);
	if (it != _map.end()) {
		it->second.loadRawData(value);
	}
}

#endif