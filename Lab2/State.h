#ifndef STATE_H
#define STATE_H

#include <memory>

class Entity;

class State{
protected:
	Entity& _context;
public:
	State(Entity& context);
	
	virtual void kill() = 0;
	virtual void move(float direction) = 0;
	virtual void stop() = 0;
};

using StatePtr = std::unique_ptr<State>;


class MovingState : public State {
public:
	MovingState(Entity &context);

	void kill() override;
	void move(float direction) override;
	void stop() override;
};

class StandingStillState : public State {
public:
	StandingStillState(Entity &context);

	void kill() override;
	void move(float direction) override;
	void stop() override;
};

class DeadState : public State {
public:
	DeadState(Entity &context);

	void kill() override;
	void move(float direction) override;
	void stop() override;
};




#endif