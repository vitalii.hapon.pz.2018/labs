#ifndef ENTITY_H
#define ENTITY_H

#include <string>

#include "SerializedObject.h"

#include "State.h"


class Game;

class Entity: public ISerializable {
	friend Game;

protected:
	float _x = 3.f;
	float _y = 3.f;

	bool _isMoving;

	float _direction = 0.f;

	float _maxSpeed;

	int _health;
	int _maxHealth;

	float _size;
	float _motionDirection;

	StatePtr _state;

public:

	Entity() {
		_state = std::make_unique<StandingStillState>(*this);
	}

	void changeState(StatePtr &state) {
		state = std::move(state);
	}

	float getMaxSpeed() const {
		return _maxSpeed;
	}
	bool isAlive() {
		return _health != 0;
	}

	void damage(int value) {
		if (value >= _health)
			_health = 0;
		else
			_health -= value;
	}

	float getX() const {
		return _x;
	}

	float getY() const {
		return _y;
	}

	float getSize() const {
		return _size;
	}

	int getHealth() const {
		return _health;
	}

	void setX(float x)  {
		_x = x;
	}

	void setY(float y) {
		_y = y;
	}

	float getDirection() const {
		return _direction;
	}

	void setDirection(float value) {
		_direction = std::fmod(value, 360.f);
	}

	void move(float time) {
		if (_isMoving) {
			float  angle = (_direction + _motionDirection) / 180.f * 3.1416;

			_x += _maxSpeed * std::cos(angle) * time;
			_y += _maxSpeed * std::sin(angle) * time;
		}
	}
	
	void setMotionDirection(float dir, bool moving = true) {

		_isMoving = moving;
		_motionDirection = dir;
		
	}

	bool isMoving() const {
		return _isMoving;
	}

	SerializedObject serialize() const override {
		SerializedObject tmp;

		tmp.storeValue("_x", _x);
		tmp.storeValue("_y", _y);

		tmp.storeValue("_isMoving", _isMoving);
		tmp.storeValue("_direction", _direction);
		tmp.storeValue("_maxSpeed", _maxSpeed);
		tmp.storeValue("_health", _health);
		tmp.storeValue("_maxHealth", _maxHealth);
		tmp.storeValue("_size", _size);
		tmp.storeValue("_motionDirection", _motionDirection);
		return tmp;
	}

	void deserialize(const SerializedObject &object) override {
		object.loadValue("_x", _x);
		object.loadValue("_y", _y);

		object.loadValue("_isMoving", _isMoving);
		object.loadValue("_direction", _direction);
		object.loadValue("_maxSpeed", _maxSpeed);
		object.loadValue("_health", _health);
		object.loadValue("_maxHealth", _maxHealth);
		object.loadValue("_size", _size);
		object.loadValue("_motionDirection", _motionDirection);
	}
};

#endif
